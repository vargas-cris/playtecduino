using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArduinoGenerator : MonoBehaviour
{
    public Text code;
    public string mensajeAutomatizado = "/* Beep boop mensaje creado por un robot.*/";
    public List<string> definitions = new List<string>();
    public List<string> setup = new List<string>();
    public List<string> loop = new List<string>();
    public void Clear()
    {
        definitions.Clear();
        setup.Clear();
        loop.Clear();
    }
    public void AddToDefinitions(string input)
    {
        definitions.Add(input);
    }
    public void AddToSetup(string input)
    {
        setup.Add(input);
    }
    public void AddToLoop(string input)
    {
        loop.Add(input);
    }
    public void AddFunctionDefinition(string type, string name, string content)
    {
        string result = "\n"+type + " " + name + "() {\n" + content + "\n}";
        AddToDefinitions(result);
    }
    // Start is called before the first frame update
    public void Generate()
    {
        code.text = mensajeAutomatizado + "\n\n";
        foreach(var item in definitions)
        {
            code.text = code.text + item + "\n";
        }
        code.text = code.text + "\nsetup()\n{\n";
        foreach (var item in setup)
        {
            code.text = code.text + item + "\n";
        }
        code.text = code.text + "}\n\nloop()\n{\n";
        foreach (var item in loop)
        {
            code.text = code.text + item + "\n";
        }
        code.text = code.text + "}";

    }
    public void CopyToClipboard()
    {
        GUIUtility.systemCopyBuffer = code.text;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
