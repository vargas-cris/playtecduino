using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement2 : MonoBehaviour
{
    [SerializeField] GirarRueda RuedaDerecha;
    [SerializeField] GirarRueda RuedaIzquierda;
    public Rigidbody rb;
    public Transform startPos;
    public Text commandText; 
    public bool advance, back, turnLeft, turnRight, stop, startMove;
    public List<string> comandos = new List<string>();
    public float turnSpeed = 160f;
    private float baseTurnSpeed = 160f;
    public float moveSpeed = 6f;
    private float baseMoveSpeed = 6f;
    public float waitTime = 0.5f;
    public char separation = ' ';

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void CheckMovementBools()
    {
        if(advance)
        {
            RuedaDerecha.ChangeSpeed(1);
            RuedaIzquierda.ChangeSpeed(1);
        }
        if(back)
        {
            RuedaDerecha.ChangeSpeed(-1);
            RuedaIzquierda.ChangeSpeed(-1);
        }
        if(turnLeft)
        {
            RuedaDerecha.ChangeSpeed(1);
            RuedaIzquierda.ChangeSpeed(-1);
        }
        if(turnRight)
        {
            RuedaDerecha.ChangeSpeed(1);
            RuedaIzquierda.ChangeSpeed(-1);
        }
        if(stop) 
        {
            RuedaDerecha.ChangeSpeed(0);
            RuedaIzquierda.ChangeSpeed(0);
        }
    }
    // Update is called once per frame
    void Update()
    {
        Debug.Log(moveSpeed);
        CheckMovementBools();
        if(advance)
        {
            rb.velocity = transform.forward * moveSpeed;
            
        }
        if(back)
        {
            rb.velocity = transform.forward * -moveSpeed;
            
        }
        if(turnLeft)
        {
            rb.angularVelocity = new Vector3(0, (turnSpeed * Mathf.PI *-1)/ 180, 0);
            
        }
        if(turnRight)
        {
            rb.angularVelocity = new Vector3(0, (turnSpeed * Mathf.PI) / 180, 0);
           
        }
    }
    IEnumerator StartCoroutine(List<string> comandos)
    {
        foreach (var item in comandos)
        {

            if (item.StartsWith("forward"))
            {
                back = false;
                turnLeft = false;
                turnRight = false;
                //yield return new WaitForSeconds(waitTime);
                stop = false;
                advance = true;
                
                yield return null;
            }
            if (item.StartsWith("back"))
            {
                advance = false;
                turnLeft = false;
                turnRight = false;
                stop = false;
                //yield return new WaitForSeconds(waitTime);
                back = true;
                
                yield return null;
            }
            if (item.StartsWith("turnLeft"))
            {
                advance = false;
                back = false;
                turnRight = false;
                stop = false;
                //yield return new WaitForSeconds(waitTime);
                turnLeft = true;
                
                yield return null;
            }
            if (item.StartsWith("turnRight"))
            {
                advance = false;
                back = false;
                turnLeft = false;
                stop = false;
                //yield return new WaitForSeconds(waitTime);
                turnRight = true;
                /*RuedaDerecha.ChangeSpeed(-1);
                RuedaIzquierda.ChangeSpeed(1);*/
                yield return null;
            }
            if (item.StartsWith("stop"))
            {
                advance = false;
                back = false;
                turnLeft = false;
                turnRight = false;
                stop = true;

                yield return null;
            }
            if (item.StartsWith("wait"))
            {
                string[] values = item.Split(separation);
                float duration = float.Parse(values[1]);
                while (duration >= 0.0f)
                {
                    duration -= Time.deltaTime;
                    yield return null;
                }
            }
            if(item.StartsWith("moveSpeed"))
            {
                string[] values = item.Split(separation);
                moveSpeed = float.Parse(values[1])*baseMoveSpeed;
                turnSpeed = float.Parse(values[1]) * baseTurnSpeed;
                RuedaDerecha.ChangeBaseSpeed(float.Parse(values[1]));
                RuedaIzquierda.ChangeBaseSpeed(float.Parse(values[1]));
            }
        }
        startMove = false;

    }
    public void StartMove()
    {
        if (startMove == false)
        {
            startMove = true;
            StartCoroutine(StartCoroutine(comandos));
        }
    }
    public void RellenarComando(string comando)
    {
        comandos.Add(comando);
    }
    public void RellenarComando()
    {
        comandos.Add(commandText.text);
    }
    public void VaciarLista()
    {
        comandos = new List<string>();
    }
    public void ResetPlayer()
    {
        //comandos = new List<string>();
        advance = false;
        back = false;
        turnLeft = false;
        turnRight = false;
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
        rb.MovePosition(startPos.position);
        transform.rotation = startPos.transform.rotation;
        moveSpeed = baseMoveSpeed;
        RuedaDerecha.ChangeSpeed(0);
        RuedaIzquierda.ChangeSpeed(0);

        startMove = false;
        StopAllCoroutines();
    }
}
