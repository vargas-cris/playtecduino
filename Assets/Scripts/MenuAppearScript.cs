using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuAppearScript : MonoBehaviour
{
    public Sprite first;
    public Sprite second;
    public GameObject menu; // Assign in inspector

    private bool isShowing;

    public void MenuAppear()
    {
        isShowing = !isShowing;
        SetImage(isShowing);
        menu.SetActive(isShowing);
    }
    private void SetImage(bool state)
    {
        if(state)
        {
            GetComponent<Image>().sprite = first;
        }
        else
        {
            GetComponent<Image>().sprite = second;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isShowing = !isShowing;
            menu.SetActive(isShowing);
        }
    }
}