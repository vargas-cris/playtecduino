﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssociateImage : MonoBehaviour
{
    public float scaleFactorX = 2.5f;
    public float scaleFactorY = 2.5f;
    public float scaleFactorZ = 2.5f;
    public SpriteRenderer image;
    public Material mat;
    public GameObject display;
    // Start is called before the first frame update
    private void Awake()
    {
        Vector3 scale = new Vector3((image.size.x / image.size.y) * scaleFactorX, scaleFactorY, scaleFactorZ);
        this.transform.localScale = scale;
        var x = display.GetComponent<MeshRenderer>();
        x.material = mat;
    }
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {

    }
}
