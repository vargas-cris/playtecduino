using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    public Rigidbody rb;
    public float turnSpeed = 180f;
    public float moveSpeed = 6f;
    public List<string> comandos = new List<string>();
    public Transform startPos;
    public Text commandText;
    bool startMove = false;
    public bool stopped = false;
    public char separation = ' ';
    // Update is called once per frame
    void Update()
    {
    }
    // Set your parameters in the Inspector.
    public void StartMove()
    {
        if (startMove == false)
        {
            startMove = true;
            stopped = false;
            StartCoroutine(StartCoroutine(comandos));
        }
    }
    public void RellenarComando(string comando)
    {
        comandos.Add(comando);
    }
    public void RellenarComando()
    {
        comandos.Add(commandText.text);
    }
    public void ResetPlayer()
    {
        comandos = new List<string>();
        rb.MovePosition(startPos.position);
        rb.velocity = new Vector3(0, 0, 0);
        stopped = true;
        startMove = false;
        StopAllCoroutines();
    }
    public float distance = 10f;
    public float waitSeconds = 5f;
    public Vector3 targetOffset = Vector3.forward * 10f;
    public Quaternion targetRotation;

    // Make Start a coroutine that begins 
    // as soon as our object is enabled.
    IEnumerator Wait(int seconds)
    {
        float duration = (float)seconds;
        while (duration >= 0.0f)
        {
            duration -= Time.deltaTime;
            Debug.Log(duration);
            yield return null;
        }
    }
    IEnumerator StartCoroutine(List<string> comandos)
    {
        stopped = false;
        foreach(var item in comandos)
        {
            if(item.StartsWith("move"))
            {
                string[] values = item.Split(separation);
                distance = (float)int.Parse(values[1]);
                targetOffset = transform.forward * distance;
                waitSeconds = distance / moveSpeed;
                // First, wait our defined duration.
                //yield return new WaitForSeconds(0);

                // Then, pick our destination point offset from our current location.
                Vector3 targetPosition = transform.position + targetOffset;

                // Loop until we're within Unity's vector tolerance of our target.
                while (!stopped&&Vector3.Distance(transform.position,targetPosition)>0.2f)
                {
                    //Movimiento que no tiene en cuenta las f�sicas
                    // Move one step toward the target at our given speed.
                    /*transform.position = Vector3.MoveTowards(
                          transform.position,
                          targetPosition,
                          speed * Time.deltaTime
                     );*/
                    //Movimiento que s� tiene en cuenta las f�sicas
                    rb.velocity = transform.forward*moveSpeed;

                    // Wait one frame then resume the loop.
                    yield return null;
                }
                rb.velocity = new Vector3(0,0,0);
                // We have arrived. Ensure we hit it exactly.
                //transform.position = targetPosition;

            }
            if(item.StartsWith("rotate"))
            {
                string[] values = item.Split(separation);
                int direction = int.Parse(values[1]);
                float angles = (float)int.Parse(values[2]);
                Vector3 targetRotationVec = transform.rotation.eulerAngles+new Vector3(0,angles*direction,0);
                Vector3 transformRotationVec = transform.rotation.eulerAngles;
                Debug.Log(Vector3.Distance(targetRotationVec, transformRotationVec));
                
                while (!stopped&&Vector3.Distance(targetRotationVec,transformRotationVec)>2f)
                {
                    rb.angularVelocity = new Vector3(0, turnSpeed*Mathf.PI/180*direction, 0);
                    transformRotationVec = transform.rotation.eulerAngles;
                    yield return null;
                }
                rb.angularVelocity = new Vector3(0, 0, 0);
            }
            if(item.StartsWith("wait"))
            {
                string[] values = item.Split(separation);
                float duration = (float)int.Parse(values[1]);
                while(duration>=0.0f)
                {
                    duration -= Time.deltaTime;
                    Debug.Log(duration);
                    yield return null;
                }
            }
            if(item.StartsWith("forward"))
            {
                rb.velocity = transform.forward * moveSpeed;
                yield return null;
            }
        }
        
    }
}
