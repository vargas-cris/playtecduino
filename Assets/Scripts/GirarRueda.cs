using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirarRueda : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float baseValue = 540;
    private float neverchanging = 540;
    // Start is called before the first frame update
    public void ChangeSpeed(int input)
    {
        Debug.Log(input);
        if (input == 1)
        {
            Debug.Log("Palante");
            speed = baseValue*1;
        }
        else if(input == -1)
        {
            Debug.Log("Patr�s");
            speed = baseValue * -1;
        }
        else if(input == 0)
        {
            Debug.Log("Quieto");
            speed = 0;
        }
    }

    public void ChangeBaseSpeed(float desiredSpeed)
    {
        baseValue = desiredSpeed*neverchanging;
    }
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(speed * Time.deltaTime, 0, 0);
    }
}
