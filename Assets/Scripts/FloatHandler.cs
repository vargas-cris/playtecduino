using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatHandler : MonoBehaviour
{
    public InputField number;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ClampNumber(number);
    }

    private void ClampNumber(InputField numberToCheck)
    {
        float n;
        if(!float.TryParse(numberToCheck.text, out n))
        {
            numberToCheck.text = "0";
        }
        else
        {
        }
    }
}
