using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitButton : Block
{
    public InputField input;
    public ArduinoGenerator ag;
    public Movement2 player;
    // Start is called before the first frame update
    private void Awake()
    {
        ag = FindObjectOfType<ArduinoGenerator>();
        player = FindObjectOfType<Movement2>();
    }

    public override void Command()
    {
        if (input.text == "")
            player.RellenarComando("wait" + player.separation + "0");
        else
            player.RellenarComando("wait" + player.separation + input.text);

    }
    private int CheckValue(string text)
    {
        if (input.text == "")
            return 0;
        else
            return (int)(float.Parse(text)*1000f);
    }
    public override void BlockFunction()
    {
        ag.AddToLoop("delay("+CheckValue(input.text)+");");
        Command();
    }
}
