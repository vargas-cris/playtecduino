using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragBlock : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    static public GameObject ItemBeingDragged;
    static public Command command;
    public void OnBeginDrag(PointerEventData eventData)
    {
        command = GetComponent<Command>();
        GameObject duplicate = Instantiate(gameObject);
        ItemBeingDragged = duplicate;
        RectTransform tempRT = gameObject.GetComponent<RectTransform>();
        RectTransform rt = ItemBeingDragged.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(tempRT.sizeDelta.x, tempRT.sizeDelta.y);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        Transform canvas = GameObject.FindGameObjectWithTag("UI Canvas").transform;

        ItemBeingDragged.transform.SetParent(canvas, false);
        ItemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;


    }

    public void OnDrag(PointerEventData eventData)
    {
        ItemBeingDragged.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        Destroy(ItemBeingDragged);
        DragBlock.ItemBeingDragged = null;
    }
    
}
