using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopBlock : Block
{
    public ArduinoGenerator ag;
    public Movement2 player;
    // Start is called before the first frame update
    private void Awake()
    {
        ag = FindObjectOfType<ArduinoGenerator>();
        player = FindObjectOfType<Movement2>();
    }
    public override void Command()
    {
        player.RellenarComando("stop");
    }
    public override void BlockFunction()
    {
        ag.AddToLoop("Detener();");
        Command();
    }
}
