using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropBlock : MonoBehaviour, IDropHandler
{
    public BlockList bl;
    public void OnDrop(PointerEventData eventData)
    {
        if(DragBlock.command != null)
        {
            bl.AddBlockToList(DragBlock.command.command);
            Destroy(DragBlock.ItemBeingDragged);
            DragBlock.ItemBeingDragged = null;
            DragBlock.command = null;
        }
        
    }
    
}
