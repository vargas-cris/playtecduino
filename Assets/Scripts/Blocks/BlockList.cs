using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockList : MonoBehaviour
{
    public GameObject robotBlock, forwardBlock, backBlock, turnRightBlock, turnLeftBlock, waitBlock, stopBlock;
    /*public WaitButton wb;
    public TurnLeftBlock lb;
    public TurnRightBlock rb;
    public ForwardBlock fb;
    public BackBlock bb;
    public RobotBlock robot;
    public StopBlock sb;*/
    public ArduinoGenerator ag;
    public Movement2 player;
    public GameObject blockList;
    public Scrollbar sb;
    private char separation = ' ';

    private void Awake()
    {
        ag = FindObjectOfType<ArduinoGenerator>();
        player = FindObjectOfType<Movement2>();
    }
    public void AddBlockToList(string blockName)
    {
        if(blockName.StartsWith("robotBlock"))
        {
            if (blockList.transform.childCount == 0)
            {
                var child = Instantiate(robotBlock);
                child.transform.SetParent(blockList.transform);
                child.transform.SetSiblingIndex(0);
                child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            }
            else
            {
                if (blockList.transform.GetChild(0).name != "RobotElement(Clone)")
                {
                    var child = Instantiate(robotBlock);
                    child.transform.SetParent(blockList.transform);
                    child.transform.SetSiblingIndex(0);
                    child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                }
            }
            
            
            
        }
        if (blockName == "forwardBlock")
        {
            var child = Instantiate(forwardBlock);
            child.transform.SetParent(blockList.transform);
            child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        if (blockName == "backBlock")
        {
            var child = Instantiate(backBlock);
            child.transform.SetParent(blockList.transform);
            child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        if (blockName == "turnRightBlock")
        {
            var child = Instantiate(turnRightBlock);
            child.transform.SetParent(blockList.transform);
            child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        if (blockName == "turnLeftBlock")
        {
            var child = Instantiate(turnLeftBlock);
            child.transform.SetParent(blockList.transform);
            child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        if (blockName == "waitBlock")
        {
            var child = Instantiate(waitBlock);
            child.transform.SetParent(blockList.transform);
            child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        if (blockName == "stopBlock")
        {
            var child = Instantiate(stopBlock);
            child.transform.SetParent(blockList.transform);
            child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        sb.value = 1;
    }
    public void Compile()
    {
        ag.Clear();
        player.VaciarLista();
        foreach(Transform child in blockList.transform)
        {
            //Hacer que unity encuentre los hijos de la lista y ejecute la funci�n del bloque de cada hijo
            child.gameObject.GetComponent<Button>().onClick.Invoke();
        }
        ag.Generate();

    }
    public void VaciarBloques()
    {
        foreach (Transform child in blockList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        ag.Clear();
        ag.Generate();
        player.VaciarLista();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
}
