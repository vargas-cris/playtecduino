using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackBlock : Block
{
    public ArduinoGenerator ag;
    public Movement2 player;
    // Start is called before the first frame update
    private void Awake()
    {
        ag = FindObjectOfType<ArduinoGenerator>();
        player = FindObjectOfType<Movement2>();
    }
    public override void Command()
    {
        player.RellenarComando("back");
    }
    public override void BlockFunction()
    {
        ag.AddToLoop("Retroceder();");
        Command();
    }
}
