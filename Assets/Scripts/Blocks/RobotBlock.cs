using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobotBlock : Block
{
    public ArduinoGenerator ag;
    public InputField RightEngine;
    public InputField LeftEngine;
    public Movement2 player;
    // Start is called before the first frame update
    private void Awake()
    {
        ag = FindObjectOfType<ArduinoGenerator>();
        player = FindObjectOfType<Movement2>();
    }
    public override void Command()
    {
        float re = float.Parse(RightEngine.text);
        float le = float.Parse(LeftEngine.text);
        float result = (float)(((re + le) / 2.0) / 255.0);
        string text = result.ToString();
        player.RellenarComando("moveSpeed" + player.separation + text);
    }
    public override void BlockFunction()
    {
        if (RightEngine.text == "")
            RightEngine.text = "0";
        if (LeftEngine.text == "")
            LeftEngine.text = "0";

        //Generar c�digo arduino
        ag.AddToDefinitions("int MotorD=" + RightEngine.text);
        ag.AddToDefinitions("int MotorI=" + LeftEngine.text);
        ag.AddFunctionDefinition("void", "Avanzar", "digitalWrite(4, HIGH);\ndigitalWrite(5, LOW);\ndigitalWrite(7, LOW);\ndigitalWrite(8, HIGH);");
        ag.AddFunctionDefinition("void", "Retroceder", "digitalWrite(4, LOW);\ndigitalWrite(5, HIGH);\ndigitalWrite(7, HIGH);\ndigitalWrite(8, LOW);");
        ag.AddFunctionDefinition("void", "Izquierda", "digitalWrite(4, LOW);\ndigitalWrite(5, HIGH);\ndigitalWrite(7, LOW);\ndigitalWrite(8, HIGH);");
        ag.AddFunctionDefinition("void", "Derecha", "digitalWrite(4, HIGH);\ndigitalWrite(5, LOW);\ndigitalWrite(7, HIGH);\ndigitalWrite(8, LOW);");
        ag.AddFunctionDefinition("void", "Detener", "digitalWrite(4, LOW);\ndigitalWrite(5, LOW);\ndigitalWrite(7, LOW);\ndigitalWrite(8, LOW);");
        ag.AddToSetup("pinMode(4, OUTPUT);");
        ag.AddToSetup("pinMode(5, OUTPUT);");
        ag.AddToSetup("pinMode(7, OUTPUT);");
        ag.AddToSetup("pinMode(8, OUTPUT);");
        ag.AddToSetup("pinMode(3, OUTPUT);");
        ag.AddToSetup("pinMode(6, OUTPUT);");
        ag.AddToSetup("analogWrite(" + 6 + "," + LeftEngine.text + ");");
        ag.AddToSetup("analogWrite(" + 6 + "," + RightEngine.text + ");");
        Command();
    }
}
