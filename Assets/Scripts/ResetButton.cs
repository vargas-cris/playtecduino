using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetButton : MonoBehaviour
{
    public Movement2 player;
    // Start is called before the first frame update
    private void Awake()
    {
       player = FindObjectOfType<Movement2>();
    }
    public void ResetFunc()
    {
        player.ResetPlayer();
    }
}
