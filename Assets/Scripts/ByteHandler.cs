using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ByteHandler : MonoBehaviour
{
    public InputField number;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ClampNumber(number);
    }

    private void ClampNumber(InputField numberToCheck)
    {
        int n;
        if(int.TryParse(numberToCheck.text, out n))
        {
            if(n>255)
            {
                numberToCheck.text = "255";
            }
            if(n<0)
            {
                numberToCheck.text = "0";
            }
        }
        else
        {
            numberToCheck.text = "0";
        }
    }
}
