﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ArduinoGenerator::Clear()
extern void ArduinoGenerator_Clear_mA8305D40B3DFE56CE5A93ACE8EBFE5CE57196BF8 (void);
// 0x00000002 System.Void ArduinoGenerator::AddToDefinitions(System.String)
extern void ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C (void);
// 0x00000003 System.Void ArduinoGenerator::AddToSetup(System.String)
extern void ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8 (void);
// 0x00000004 System.Void ArduinoGenerator::AddToLoop(System.String)
extern void ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8 (void);
// 0x00000005 System.Void ArduinoGenerator::AddFunctionDefinition(System.String,System.String,System.String)
extern void ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849 (void);
// 0x00000006 System.Void ArduinoGenerator::Generate()
extern void ArduinoGenerator_Generate_m7F205A8BD0DB80B6EC984B73C23A05C3CF62E700 (void);
// 0x00000007 System.Void ArduinoGenerator::CopyToClipboard()
extern void ArduinoGenerator_CopyToClipboard_m50D172EC16AACD0C38731DEB6F6EB782D76A7B18 (void);
// 0x00000008 System.Void ArduinoGenerator::Start()
extern void ArduinoGenerator_Start_m45F7F52A2A786D313401E6AA1389A12A35F875E7 (void);
// 0x00000009 System.Void ArduinoGenerator::Update()
extern void ArduinoGenerator_Update_m6B827819FBADF4829BA2785D55C0BA6150D5D5AA (void);
// 0x0000000A System.Void ArduinoGenerator::.ctor()
extern void ArduinoGenerator__ctor_mA56A1E42C71CDE11F860A38F59B870B35CCEE739 (void);
// 0x0000000B System.Void AssociateImage::Awake()
extern void AssociateImage_Awake_m3ED6C44BC84608F1586598CBB9DF89CF656845D0 (void);
// 0x0000000C System.Void AssociateImage::Start()
extern void AssociateImage_Start_m2E1DBD6AE26C361EC3033D2C20AF2723ED85581A (void);
// 0x0000000D System.Void AssociateImage::Update()
extern void AssociateImage_Update_mC476AF0982334873C6D21C24C74EE4940EA13FD5 (void);
// 0x0000000E System.Void AssociateImage::.ctor()
extern void AssociateImage__ctor_mFB4EA56F1EB4DF5D9AC224D272C5E8BD31BAA9EC (void);
// 0x0000000F System.Void BackBlock::Awake()
extern void BackBlock_Awake_m101C94C1F4A30D7F65131D23D74B42DF039D86EE (void);
// 0x00000010 System.Void BackBlock::Command()
extern void BackBlock_Command_m6875CC0E3342EC4896D6A944BD447016CD91E583 (void);
// 0x00000011 System.Void BackBlock::BlockFunction()
extern void BackBlock_BlockFunction_m7C31BEFADECAF08F0454157C12113EC6D6AA6DC8 (void);
// 0x00000012 System.Void BackBlock::.ctor()
extern void BackBlock__ctor_mEA9FBEA1AA24CAC453F990E61F8B2A04226F6A12 (void);
// 0x00000013 System.Void Block::Conectar()
extern void Block_Conectar_mE109D68AA825CB421D4A2D85C9BA7656151091F7 (void);
// 0x00000014 System.Void Block::Command()
extern void Block_Command_m7A46E3DE017076D867D95EB0A6CDCB974AB4894B (void);
// 0x00000015 System.Void Block::BlockFunction()
extern void Block_BlockFunction_m4829E0DFD55C256E3ACAB2E96B6BD32E6FFCB671 (void);
// 0x00000016 System.Void Block::.ctor()
extern void Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE (void);
// 0x00000017 System.Void BlockList::Awake()
extern void BlockList_Awake_m073AD1D615879CC826FD8E0EA8199259BB17A6BC (void);
// 0x00000018 System.Void BlockList::AddBlockToList(System.String)
extern void BlockList_AddBlockToList_mB508FF730A896E8C6C390B3A3039D40DBC53DBE0 (void);
// 0x00000019 System.Void BlockList::Compile()
extern void BlockList_Compile_m948C9BB29590F1EB9E2DA7A0945C08A418295382 (void);
// 0x0000001A System.Void BlockList::VaciarBloques()
extern void BlockList_VaciarBloques_m875BA9546A72F318AFFA63108692F774986C4E31 (void);
// 0x0000001B System.Void BlockList::Start()
extern void BlockList_Start_mF0C6A5D6FF0D0313EAA67039B872A1D2DE4FBC85 (void);
// 0x0000001C System.Void BlockList::Update()
extern void BlockList_Update_m7DE7BCDF6833F712D084DF0FA1F65966E23FA469 (void);
// 0x0000001D System.Void BlockList::.ctor()
extern void BlockList__ctor_m45E84FCBF29E3B5A2F2C9DC852BC01E10768DFE1 (void);
// 0x0000001E System.Void Command::.ctor()
extern void Command__ctor_mABFCAFBBC7B0C23A774E1438BE9D0C29440DE5B2 (void);
// 0x0000001F System.Void DragBlock::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragBlock_OnBeginDrag_m588B9E3946C652556BCF294A9338CD4903EE8C0B (void);
// 0x00000020 System.Void DragBlock::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragBlock_OnDrag_m993ED74611F526FB3A9D4E5131359D01046B7319 (void);
// 0x00000021 System.Void DragBlock::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragBlock_OnEndDrag_m7B52D9D0EBD9D4EA459ACB448BA20089E8DC8BA0 (void);
// 0x00000022 System.Void DragBlock::.ctor()
extern void DragBlock__ctor_m5BDF2E204B5FE3E2E0DB4C9EA9A4FE288A154DAF (void);
// 0x00000023 System.Void DropBlock::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void DropBlock_OnDrop_mF4D84654CB663F7464ADB5B07B80BCC0CE52FD58 (void);
// 0x00000024 System.Void DropBlock::.ctor()
extern void DropBlock__ctor_m669A575F06274C13EA30AB67CFFF9730765E6BF1 (void);
// 0x00000025 System.Void ForwardBlock::Awake()
extern void ForwardBlock_Awake_m255A948840163AC5131BC31683A15B4F2840E3E6 (void);
// 0x00000026 System.Void ForwardBlock::Command()
extern void ForwardBlock_Command_mFC7093C7174209A215AB778E366F34393C0CF77B (void);
// 0x00000027 System.Void ForwardBlock::BlockFunction()
extern void ForwardBlock_BlockFunction_mD18B4E40F955426BE574FE83A2F56E1ADCDE32B4 (void);
// 0x00000028 System.Void ForwardBlock::.ctor()
extern void ForwardBlock__ctor_m917CC80D6AE80A26528ABD98CEB16ACF6F96EE8B (void);
// 0x00000029 System.Void RobotBlock::Awake()
extern void RobotBlock_Awake_mD121CA1BFF00249D75AA1D6D9B6F50E156E9DA38 (void);
// 0x0000002A System.Void RobotBlock::Command()
extern void RobotBlock_Command_mA895708E4DA10F54AC44B41D71401CF2D1906824 (void);
// 0x0000002B System.Void RobotBlock::BlockFunction()
extern void RobotBlock_BlockFunction_m9C0573E4F824F0B50FD31284C255B0E1B50A6847 (void);
// 0x0000002C System.Void RobotBlock::.ctor()
extern void RobotBlock__ctor_m801C1BE4F983EF8300062906889C2CF51B81E960 (void);
// 0x0000002D System.Void StopBlock::Awake()
extern void StopBlock_Awake_m2CB9222D913E35E3E3CF5F4B0F6AB42D0BBE4747 (void);
// 0x0000002E System.Void StopBlock::Command()
extern void StopBlock_Command_mCA6304A721B125BDCD3D0BA6FEDD1467DB420176 (void);
// 0x0000002F System.Void StopBlock::BlockFunction()
extern void StopBlock_BlockFunction_mDE69EAD6C6BC4074EAB33932FE44F6A63DA79EA1 (void);
// 0x00000030 System.Void StopBlock::.ctor()
extern void StopBlock__ctor_m5986634D6A3740FF756C0BDF404CB7B0A451D005 (void);
// 0x00000031 System.Void TurnLeftBlock::Awake()
extern void TurnLeftBlock_Awake_mAB53CDCD1100230E369C34FCB74BF8C67905DB21 (void);
// 0x00000032 System.Void TurnLeftBlock::Command()
extern void TurnLeftBlock_Command_mF542404F51173279C09AB5A92ADD251C50E6AEC8 (void);
// 0x00000033 System.Void TurnLeftBlock::BlockFunction()
extern void TurnLeftBlock_BlockFunction_m84AF055FE76C840152090E0FBEFCCE4C61A79F60 (void);
// 0x00000034 System.Void TurnLeftBlock::.ctor()
extern void TurnLeftBlock__ctor_m030BA77CFBC0237496CC4AA6C5B382592664C515 (void);
// 0x00000035 System.Void TurnRightBlock::Awake()
extern void TurnRightBlock_Awake_m132FCBB93ABFF06881AC7BC3736F0F325C5C379D (void);
// 0x00000036 System.Void TurnRightBlock::Command()
extern void TurnRightBlock_Command_mB96F557241D00E7633695E74BD4E64BD735EE5B3 (void);
// 0x00000037 System.Void TurnRightBlock::BlockFunction()
extern void TurnRightBlock_BlockFunction_mC1EC1302DFC2E405E949BAC4F04ED2C4C725A7EE (void);
// 0x00000038 System.Void TurnRightBlock::.ctor()
extern void TurnRightBlock__ctor_m617637B41D0C72E9B00A15E8181B404B804D3025 (void);
// 0x00000039 System.Void WaitButton::Awake()
extern void WaitButton_Awake_m9120740B9F2F92AC9C4FD2E0054D29BF92B1FE9A (void);
// 0x0000003A System.Void WaitButton::Command()
extern void WaitButton_Command_m02AA59C63C0E60F96D4C0B5D69B5D9D4106F095C (void);
// 0x0000003B System.Int32 WaitButton::CheckValue(System.String)
extern void WaitButton_CheckValue_m7CCA307D27B82090E14654D4564924126C2EEC3A (void);
// 0x0000003C System.Void WaitButton::BlockFunction()
extern void WaitButton_BlockFunction_m8BCCB86E5A8F1A6DC92D3632EF3631A4415F37BE (void);
// 0x0000003D System.Void WaitButton::.ctor()
extern void WaitButton__ctor_m10CE988D4A5ED4BFDBA9250F7A4788BD96A0E437 (void);
// 0x0000003E System.Void ByteHandler::Start()
extern void ByteHandler_Start_mE9FB68D23D7F05ADD42830652AAA021CD920D919 (void);
// 0x0000003F System.Void ByteHandler::Update()
extern void ByteHandler_Update_mA9B6B094B3B3D2D397EFD8B77932DC7CF681A80E (void);
// 0x00000040 System.Void ByteHandler::ClampNumber(UnityEngine.UI.InputField)
extern void ByteHandler_ClampNumber_m3EB3885110EB83603E16F1E99E801168EB5A55E8 (void);
// 0x00000041 System.Void ByteHandler::.ctor()
extern void ByteHandler__ctor_m9BA2B5C614AD76E44CB88804661FC796C9D42496 (void);
// 0x00000042 System.Void CameraOnClick::Start()
extern void CameraOnClick_Start_m66D5DA3F9D8FE26F23558F3636E7DD24F2593C9D (void);
// 0x00000043 System.Single CameraOnClick::GetAxisCustom(System.String)
extern void CameraOnClick_GetAxisCustom_m6F5D6FBFD7F68E12C91A0E049542232AB786A125 (void);
// 0x00000044 System.Void CameraOnClick::.ctor()
extern void CameraOnClick__ctor_m772BDF09B30584BD43165345B6DB83A4C225C26E (void);
// 0x00000045 System.Void DeleteBlock::Start()
extern void DeleteBlock_Start_m60D1C37059958EE885F7CB0BF4677ADF62468BCE (void);
// 0x00000046 System.Void DeleteBlock::Update()
extern void DeleteBlock_Update_mA798156FE132D623D327B228CC8A54F27FCAE5ED (void);
// 0x00000047 System.Void DeleteBlock::.ctor()
extern void DeleteBlock__ctor_m6407EB0ED948B9166F5B4F85D4AF39E3068CD252 (void);
// 0x00000048 UnityEngine.GameObject DragOrderContainer::get_objectBeingDragged()
extern void DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D (void);
// 0x00000049 System.Void DragOrderContainer::set_objectBeingDragged(UnityEngine.GameObject)
extern void DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A (void);
// 0x0000004A System.Void DragOrderContainer::Awake()
extern void DragOrderContainer_Awake_mDFE27134C91A7F68B1B32866CE038567DF1BAE44 (void);
// 0x0000004B System.Void DragOrderContainer::.ctor()
extern void DragOrderContainer__ctor_mDF376A9B27529D23606F21C4C2E1841A4A862322 (void);
// 0x0000004C System.Void DragOrderObject::Start()
extern void DragOrderObject_Start_m8D6CF0B090C376ED9AF2360E1E6D3C56576CCD7F (void);
// 0x0000004D System.Void DragOrderObject::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragOrderObject_OnBeginDrag_m31FB3A82C667A42C239E222828DE8298C1D02E05 (void);
// 0x0000004E System.Void DragOrderObject::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragOrderObject_OnDrag_mF78A9A8A41ECBDDF71B2F1898DAD7A4CF5A01217 (void);
// 0x0000004F System.Void DragOrderObject::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragOrderObject_OnEndDrag_m7DACA09F08AEC316CC83D49B0C3C7F9B911F4443 (void);
// 0x00000050 System.Void DragOrderObject::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void DragOrderObject_OnPointerEnter_m3229E30DB5FA15FDC6B4052D028F47E9249CAB8A (void);
// 0x00000051 System.Void DragOrderObject::.ctor()
extern void DragOrderObject__ctor_m4E6F3606E426A939F95A162E9CB6EBA57596CD8F (void);
// 0x00000052 System.Void FloatHandler::Start()
extern void FloatHandler_Start_mC6B874C8EA776DD0DF431847416A4C5DA15BF808 (void);
// 0x00000053 System.Void FloatHandler::Update()
extern void FloatHandler_Update_m265D23A1541B810DB46393768A836DB61C539441 (void);
// 0x00000054 System.Void FloatHandler::ClampNumber(UnityEngine.UI.InputField)
extern void FloatHandler_ClampNumber_m506D253C7AF6A1AA355009A2EA2602593278A24D (void);
// 0x00000055 System.Void FloatHandler::.ctor()
extern void FloatHandler__ctor_m1DDABB8187BD45E5E7D26AAA3DBE45F6529398C6 (void);
// 0x00000056 System.Void GirarRueda::ChangeSpeed(System.Int32)
extern void GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933 (void);
// 0x00000057 System.Void GirarRueda::ChangeBaseSpeed(System.Single)
extern void GirarRueda_ChangeBaseSpeed_m0C947092E43F4688E114BE3F948648AB46ABCEF5 (void);
// 0x00000058 System.Void GirarRueda::Update()
extern void GirarRueda_Update_m9CCCEA05CCC7EE5B8292BEC8AFF2181AA55CBD1D (void);
// 0x00000059 System.Void GirarRueda::.ctor()
extern void GirarRueda__ctor_m70C1661D53191D8DD0A03A08A79249E7546F96C3 (void);
// 0x0000005A System.Void MenuAppearScript::MenuAppear()
extern void MenuAppearScript_MenuAppear_mA84A1D2B30815FF859DBC6B72EE3895BCCF2C7E2 (void);
// 0x0000005B System.Void MenuAppearScript::SetImage(System.Boolean)
extern void MenuAppearScript_SetImage_m3D6B513083873E48304D0CBB9CE2B5ADCB40F0E1 (void);
// 0x0000005C System.Void MenuAppearScript::Update()
extern void MenuAppearScript_Update_mAE56EC50D83B4D609C8025EB1B8B3F80FA7AF497 (void);
// 0x0000005D System.Void MenuAppearScript::.ctor()
extern void MenuAppearScript__ctor_m79276CBDFE0545CAC2ECB7CCEF1C3B6BF14AE5BF (void);
// 0x0000005E System.Void Movement::Update()
extern void Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1 (void);
// 0x0000005F System.Void Movement::StartMove()
extern void Movement_StartMove_mBEB1DDB430F9908AE15CE198A6BC28C10A7147DC (void);
// 0x00000060 System.Void Movement::RellenarComando(System.String)
extern void Movement_RellenarComando_m91988887F5B24BF61D2940F38220E42CD5596ACD (void);
// 0x00000061 System.Void Movement::RellenarComando()
extern void Movement_RellenarComando_mF515958A2160CF4D95865F31039CD11CD272CC99 (void);
// 0x00000062 System.Void Movement::ResetPlayer()
extern void Movement_ResetPlayer_m341D4D356FF0994B3E74FD27BF99AE1146531A6B (void);
// 0x00000063 System.Collections.IEnumerator Movement::Wait(System.Int32)
extern void Movement_Wait_m7E219A93C438A3519BB4F2943AD942FB8BD051CB (void);
// 0x00000064 System.Collections.IEnumerator Movement::StartCoroutine(System.Collections.Generic.List`1<System.String>)
extern void Movement_StartCoroutine_mDEB32D8D5168F232623CE5BCB97492E6DD77EDC4 (void);
// 0x00000065 System.Void Movement::.ctor()
extern void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (void);
// 0x00000066 System.Void Movement/<Wait>d__18::.ctor(System.Int32)
extern void U3CWaitU3Ed__18__ctor_mBFF89EB8921F8EB82D33BA0EF8228082DA395F2F (void);
// 0x00000067 System.Void Movement/<Wait>d__18::System.IDisposable.Dispose()
extern void U3CWaitU3Ed__18_System_IDisposable_Dispose_m68B5C16847F6AB85196610F29CCBCE59A80F1049 (void);
// 0x00000068 System.Boolean Movement/<Wait>d__18::MoveNext()
extern void U3CWaitU3Ed__18_MoveNext_m837F69776DDD410C89BCFF20AAF34AC93FE9896C (void);
// 0x00000069 System.Object Movement/<Wait>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B6B679437293F259D9ED2A9D10816ADFFF268A (void);
// 0x0000006A System.Void Movement/<Wait>d__18::System.Collections.IEnumerator.Reset()
extern void U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_mFF16C512E6FD39347FFCA92676A6C55D2FE6A8B5 (void);
// 0x0000006B System.Object Movement/<Wait>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CWaitU3Ed__18_System_Collections_IEnumerator_get_Current_mD244D0D827F1F8DB4334C341107854C934A3F5F4 (void);
// 0x0000006C System.Void Movement/<StartCoroutine>d__19::.ctor(System.Int32)
extern void U3CStartCoroutineU3Ed__19__ctor_m9BCECFBF92C11A847A748005D398FAA7B8922A8F (void);
// 0x0000006D System.Void Movement/<StartCoroutine>d__19::System.IDisposable.Dispose()
extern void U3CStartCoroutineU3Ed__19_System_IDisposable_Dispose_mB37ED72A8D68B4562B4D700B39D943ADDD7F9249 (void);
// 0x0000006E System.Boolean Movement/<StartCoroutine>d__19::MoveNext()
extern void U3CStartCoroutineU3Ed__19_MoveNext_m15ABADF3AF20EAC7A466EDFDFBACEF9AF4786572 (void);
// 0x0000006F System.Void Movement/<StartCoroutine>d__19::<>m__Finally1()
extern void U3CStartCoroutineU3Ed__19_U3CU3Em__Finally1_mB92A9D87A4F1B0276B8064AEB27FA56377C7DD20 (void);
// 0x00000070 System.Object Movement/<StartCoroutine>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartCoroutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07691173CCEF5431341BBCF26C89A5CB841EE9EE (void);
// 0x00000071 System.Void Movement/<StartCoroutine>d__19::System.Collections.IEnumerator.Reset()
extern void U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_mC90489290B60555B1313D687BA31CF48C2D7626F (void);
// 0x00000072 System.Object Movement/<StartCoroutine>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_get_Current_m06EB29559E733DCA5830140A17F9264BC13249CB (void);
// 0x00000073 System.Void Movement2::Start()
extern void Movement2_Start_m12F783FA1DF8188103E14AAEC7F1873C81F2DC1A (void);
// 0x00000074 System.Void Movement2::CheckMovementBools()
extern void Movement2_CheckMovementBools_m4A5E33DE213E35A668DF4F1E7BFCDC1D161966DC (void);
// 0x00000075 System.Void Movement2::Update()
extern void Movement2_Update_m0E01C423FC5685C606D1F8FB29209F8CF72DFAD7 (void);
// 0x00000076 System.Collections.IEnumerator Movement2::StartCoroutine(System.Collections.Generic.List`1<System.String>)
extern void Movement2_StartCoroutine_m833568432212E05FD60EB6B743476EB3450A3CA3 (void);
// 0x00000077 System.Void Movement2::StartMove()
extern void Movement2_StartMove_m289D60BE07D916B24EA3E20488556F4DE1A1E9B1 (void);
// 0x00000078 System.Void Movement2::RellenarComando(System.String)
extern void Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD (void);
// 0x00000079 System.Void Movement2::RellenarComando()
extern void Movement2_RellenarComando_m2185CD4FFDE06C5B691638A100B5A39F21D34143 (void);
// 0x0000007A System.Void Movement2::VaciarLista()
extern void Movement2_VaciarLista_m257E3481A363F42DBC27D7614E376D4DA5D8BD2D (void);
// 0x0000007B System.Void Movement2::ResetPlayer()
extern void Movement2_ResetPlayer_mC5AA3620F9717631A06588A233B369D3E54B0F1F (void);
// 0x0000007C System.Void Movement2::.ctor()
extern void Movement2__ctor_mC9B4E3CAFD71A1BC4EF6CAD384948C5E8C1B1D43 (void);
// 0x0000007D System.Void Movement2/<StartCoroutine>d__21::.ctor(System.Int32)
extern void U3CStartCoroutineU3Ed__21__ctor_m81430BEC68689F4A9093E91D3E7685D5832AF243 (void);
// 0x0000007E System.Void Movement2/<StartCoroutine>d__21::System.IDisposable.Dispose()
extern void U3CStartCoroutineU3Ed__21_System_IDisposable_Dispose_mA53B56E97C0611E61A5A03CD4F29F23BC70817D6 (void);
// 0x0000007F System.Boolean Movement2/<StartCoroutine>d__21::MoveNext()
extern void U3CStartCoroutineU3Ed__21_MoveNext_mCA96293BA9F9724AB205FEEF8FFD2FC71677752A (void);
// 0x00000080 System.Void Movement2/<StartCoroutine>d__21::<>m__Finally1()
extern void U3CStartCoroutineU3Ed__21_U3CU3Em__Finally1_m9E93B526D9B40F122720C047C1BF8980B27ECDC2 (void);
// 0x00000081 System.Object Movement2/<StartCoroutine>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7332937FCE0266371F4E8FFC90A79B92C8C4B26 (void);
// 0x00000082 System.Void Movement2/<StartCoroutine>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5F6A852BE1A4B60DD15B80646C5D9B06BFEF2607 (void);
// 0x00000083 System.Object Movement2/<StartCoroutine>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA3AFAF6DEDE798A50A95DB5391C076FFA3FFA921 (void);
// 0x00000084 System.Void PlayButton::Awake()
extern void PlayButton_Awake_mF5410C4C0FA5B54741C27CA899A337D14E54550A (void);
// 0x00000085 System.Void PlayButton::Play()
extern void PlayButton_Play_mB2459054EC2AD057C53B3C3435A557F65EB9B4C2 (void);
// 0x00000086 System.Void PlayButton::.ctor()
extern void PlayButton__ctor_m8DD493422840621101705045AA587CF823674391 (void);
// 0x00000087 System.Void ResetButton::Awake()
extern void ResetButton_Awake_m2D334496A60A83C42F635D33DB44D6379FF58446 (void);
// 0x00000088 System.Void ResetButton::ResetFunc()
extern void ResetButton_ResetFunc_mA401C3C6F815BDFB34D1FBD182AD19AF6ABFF8F6 (void);
// 0x00000089 System.Void ResetButton::.ctor()
extern void ResetButton__ctor_m5FD6F16682DCC2AC71EC79637BB2C5CB4DF06162 (void);
static Il2CppMethodPointer s_methodPointers[137] = 
{
	ArduinoGenerator_Clear_mA8305D40B3DFE56CE5A93ACE8EBFE5CE57196BF8,
	ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C,
	ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8,
	ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8,
	ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849,
	ArduinoGenerator_Generate_m7F205A8BD0DB80B6EC984B73C23A05C3CF62E700,
	ArduinoGenerator_CopyToClipboard_m50D172EC16AACD0C38731DEB6F6EB782D76A7B18,
	ArduinoGenerator_Start_m45F7F52A2A786D313401E6AA1389A12A35F875E7,
	ArduinoGenerator_Update_m6B827819FBADF4829BA2785D55C0BA6150D5D5AA,
	ArduinoGenerator__ctor_mA56A1E42C71CDE11F860A38F59B870B35CCEE739,
	AssociateImage_Awake_m3ED6C44BC84608F1586598CBB9DF89CF656845D0,
	AssociateImage_Start_m2E1DBD6AE26C361EC3033D2C20AF2723ED85581A,
	AssociateImage_Update_mC476AF0982334873C6D21C24C74EE4940EA13FD5,
	AssociateImage__ctor_mFB4EA56F1EB4DF5D9AC224D272C5E8BD31BAA9EC,
	BackBlock_Awake_m101C94C1F4A30D7F65131D23D74B42DF039D86EE,
	BackBlock_Command_m6875CC0E3342EC4896D6A944BD447016CD91E583,
	BackBlock_BlockFunction_m7C31BEFADECAF08F0454157C12113EC6D6AA6DC8,
	BackBlock__ctor_mEA9FBEA1AA24CAC453F990E61F8B2A04226F6A12,
	Block_Conectar_mE109D68AA825CB421D4A2D85C9BA7656151091F7,
	Block_Command_m7A46E3DE017076D867D95EB0A6CDCB974AB4894B,
	Block_BlockFunction_m4829E0DFD55C256E3ACAB2E96B6BD32E6FFCB671,
	Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE,
	BlockList_Awake_m073AD1D615879CC826FD8E0EA8199259BB17A6BC,
	BlockList_AddBlockToList_mB508FF730A896E8C6C390B3A3039D40DBC53DBE0,
	BlockList_Compile_m948C9BB29590F1EB9E2DA7A0945C08A418295382,
	BlockList_VaciarBloques_m875BA9546A72F318AFFA63108692F774986C4E31,
	BlockList_Start_mF0C6A5D6FF0D0313EAA67039B872A1D2DE4FBC85,
	BlockList_Update_m7DE7BCDF6833F712D084DF0FA1F65966E23FA469,
	BlockList__ctor_m45E84FCBF29E3B5A2F2C9DC852BC01E10768DFE1,
	Command__ctor_mABFCAFBBC7B0C23A774E1438BE9D0C29440DE5B2,
	DragBlock_OnBeginDrag_m588B9E3946C652556BCF294A9338CD4903EE8C0B,
	DragBlock_OnDrag_m993ED74611F526FB3A9D4E5131359D01046B7319,
	DragBlock_OnEndDrag_m7B52D9D0EBD9D4EA459ACB448BA20089E8DC8BA0,
	DragBlock__ctor_m5BDF2E204B5FE3E2E0DB4C9EA9A4FE288A154DAF,
	DropBlock_OnDrop_mF4D84654CB663F7464ADB5B07B80BCC0CE52FD58,
	DropBlock__ctor_m669A575F06274C13EA30AB67CFFF9730765E6BF1,
	ForwardBlock_Awake_m255A948840163AC5131BC31683A15B4F2840E3E6,
	ForwardBlock_Command_mFC7093C7174209A215AB778E366F34393C0CF77B,
	ForwardBlock_BlockFunction_mD18B4E40F955426BE574FE83A2F56E1ADCDE32B4,
	ForwardBlock__ctor_m917CC80D6AE80A26528ABD98CEB16ACF6F96EE8B,
	RobotBlock_Awake_mD121CA1BFF00249D75AA1D6D9B6F50E156E9DA38,
	RobotBlock_Command_mA895708E4DA10F54AC44B41D71401CF2D1906824,
	RobotBlock_BlockFunction_m9C0573E4F824F0B50FD31284C255B0E1B50A6847,
	RobotBlock__ctor_m801C1BE4F983EF8300062906889C2CF51B81E960,
	StopBlock_Awake_m2CB9222D913E35E3E3CF5F4B0F6AB42D0BBE4747,
	StopBlock_Command_mCA6304A721B125BDCD3D0BA6FEDD1467DB420176,
	StopBlock_BlockFunction_mDE69EAD6C6BC4074EAB33932FE44F6A63DA79EA1,
	StopBlock__ctor_m5986634D6A3740FF756C0BDF404CB7B0A451D005,
	TurnLeftBlock_Awake_mAB53CDCD1100230E369C34FCB74BF8C67905DB21,
	TurnLeftBlock_Command_mF542404F51173279C09AB5A92ADD251C50E6AEC8,
	TurnLeftBlock_BlockFunction_m84AF055FE76C840152090E0FBEFCCE4C61A79F60,
	TurnLeftBlock__ctor_m030BA77CFBC0237496CC4AA6C5B382592664C515,
	TurnRightBlock_Awake_m132FCBB93ABFF06881AC7BC3736F0F325C5C379D,
	TurnRightBlock_Command_mB96F557241D00E7633695E74BD4E64BD735EE5B3,
	TurnRightBlock_BlockFunction_mC1EC1302DFC2E405E949BAC4F04ED2C4C725A7EE,
	TurnRightBlock__ctor_m617637B41D0C72E9B00A15E8181B404B804D3025,
	WaitButton_Awake_m9120740B9F2F92AC9C4FD2E0054D29BF92B1FE9A,
	WaitButton_Command_m02AA59C63C0E60F96D4C0B5D69B5D9D4106F095C,
	WaitButton_CheckValue_m7CCA307D27B82090E14654D4564924126C2EEC3A,
	WaitButton_BlockFunction_m8BCCB86E5A8F1A6DC92D3632EF3631A4415F37BE,
	WaitButton__ctor_m10CE988D4A5ED4BFDBA9250F7A4788BD96A0E437,
	ByteHandler_Start_mE9FB68D23D7F05ADD42830652AAA021CD920D919,
	ByteHandler_Update_mA9B6B094B3B3D2D397EFD8B77932DC7CF681A80E,
	ByteHandler_ClampNumber_m3EB3885110EB83603E16F1E99E801168EB5A55E8,
	ByteHandler__ctor_m9BA2B5C614AD76E44CB88804661FC796C9D42496,
	CameraOnClick_Start_m66D5DA3F9D8FE26F23558F3636E7DD24F2593C9D,
	CameraOnClick_GetAxisCustom_m6F5D6FBFD7F68E12C91A0E049542232AB786A125,
	CameraOnClick__ctor_m772BDF09B30584BD43165345B6DB83A4C225C26E,
	DeleteBlock_Start_m60D1C37059958EE885F7CB0BF4677ADF62468BCE,
	DeleteBlock_Update_mA798156FE132D623D327B228CC8A54F27FCAE5ED,
	DeleteBlock__ctor_m6407EB0ED948B9166F5B4F85D4AF39E3068CD252,
	DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D,
	DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A,
	DragOrderContainer_Awake_mDFE27134C91A7F68B1B32866CE038567DF1BAE44,
	DragOrderContainer__ctor_mDF376A9B27529D23606F21C4C2E1841A4A862322,
	DragOrderObject_Start_m8D6CF0B090C376ED9AF2360E1E6D3C56576CCD7F,
	DragOrderObject_OnBeginDrag_m31FB3A82C667A42C239E222828DE8298C1D02E05,
	DragOrderObject_OnDrag_mF78A9A8A41ECBDDF71B2F1898DAD7A4CF5A01217,
	DragOrderObject_OnEndDrag_m7DACA09F08AEC316CC83D49B0C3C7F9B911F4443,
	DragOrderObject_OnPointerEnter_m3229E30DB5FA15FDC6B4052D028F47E9249CAB8A,
	DragOrderObject__ctor_m4E6F3606E426A939F95A162E9CB6EBA57596CD8F,
	FloatHandler_Start_mC6B874C8EA776DD0DF431847416A4C5DA15BF808,
	FloatHandler_Update_m265D23A1541B810DB46393768A836DB61C539441,
	FloatHandler_ClampNumber_m506D253C7AF6A1AA355009A2EA2602593278A24D,
	FloatHandler__ctor_m1DDABB8187BD45E5E7D26AAA3DBE45F6529398C6,
	GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933,
	GirarRueda_ChangeBaseSpeed_m0C947092E43F4688E114BE3F948648AB46ABCEF5,
	GirarRueda_Update_m9CCCEA05CCC7EE5B8292BEC8AFF2181AA55CBD1D,
	GirarRueda__ctor_m70C1661D53191D8DD0A03A08A79249E7546F96C3,
	MenuAppearScript_MenuAppear_mA84A1D2B30815FF859DBC6B72EE3895BCCF2C7E2,
	MenuAppearScript_SetImage_m3D6B513083873E48304D0CBB9CE2B5ADCB40F0E1,
	MenuAppearScript_Update_mAE56EC50D83B4D609C8025EB1B8B3F80FA7AF497,
	MenuAppearScript__ctor_m79276CBDFE0545CAC2ECB7CCEF1C3B6BF14AE5BF,
	Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1,
	Movement_StartMove_mBEB1DDB430F9908AE15CE198A6BC28C10A7147DC,
	Movement_RellenarComando_m91988887F5B24BF61D2940F38220E42CD5596ACD,
	Movement_RellenarComando_mF515958A2160CF4D95865F31039CD11CD272CC99,
	Movement_ResetPlayer_m341D4D356FF0994B3E74FD27BF99AE1146531A6B,
	Movement_Wait_m7E219A93C438A3519BB4F2943AD942FB8BD051CB,
	Movement_StartCoroutine_mDEB32D8D5168F232623CE5BCB97492E6DD77EDC4,
	Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB,
	U3CWaitU3Ed__18__ctor_mBFF89EB8921F8EB82D33BA0EF8228082DA395F2F,
	U3CWaitU3Ed__18_System_IDisposable_Dispose_m68B5C16847F6AB85196610F29CCBCE59A80F1049,
	U3CWaitU3Ed__18_MoveNext_m837F69776DDD410C89BCFF20AAF34AC93FE9896C,
	U3CWaitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B6B679437293F259D9ED2A9D10816ADFFF268A,
	U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_mFF16C512E6FD39347FFCA92676A6C55D2FE6A8B5,
	U3CWaitU3Ed__18_System_Collections_IEnumerator_get_Current_mD244D0D827F1F8DB4334C341107854C934A3F5F4,
	U3CStartCoroutineU3Ed__19__ctor_m9BCECFBF92C11A847A748005D398FAA7B8922A8F,
	U3CStartCoroutineU3Ed__19_System_IDisposable_Dispose_mB37ED72A8D68B4562B4D700B39D943ADDD7F9249,
	U3CStartCoroutineU3Ed__19_MoveNext_m15ABADF3AF20EAC7A466EDFDFBACEF9AF4786572,
	U3CStartCoroutineU3Ed__19_U3CU3Em__Finally1_mB92A9D87A4F1B0276B8064AEB27FA56377C7DD20,
	U3CStartCoroutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07691173CCEF5431341BBCF26C89A5CB841EE9EE,
	U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_mC90489290B60555B1313D687BA31CF48C2D7626F,
	U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_get_Current_m06EB29559E733DCA5830140A17F9264BC13249CB,
	Movement2_Start_m12F783FA1DF8188103E14AAEC7F1873C81F2DC1A,
	Movement2_CheckMovementBools_m4A5E33DE213E35A668DF4F1E7BFCDC1D161966DC,
	Movement2_Update_m0E01C423FC5685C606D1F8FB29209F8CF72DFAD7,
	Movement2_StartCoroutine_m833568432212E05FD60EB6B743476EB3450A3CA3,
	Movement2_StartMove_m289D60BE07D916B24EA3E20488556F4DE1A1E9B1,
	Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD,
	Movement2_RellenarComando_m2185CD4FFDE06C5B691638A100B5A39F21D34143,
	Movement2_VaciarLista_m257E3481A363F42DBC27D7614E376D4DA5D8BD2D,
	Movement2_ResetPlayer_mC5AA3620F9717631A06588A233B369D3E54B0F1F,
	Movement2__ctor_mC9B4E3CAFD71A1BC4EF6CAD384948C5E8C1B1D43,
	U3CStartCoroutineU3Ed__21__ctor_m81430BEC68689F4A9093E91D3E7685D5832AF243,
	U3CStartCoroutineU3Ed__21_System_IDisposable_Dispose_mA53B56E97C0611E61A5A03CD4F29F23BC70817D6,
	U3CStartCoroutineU3Ed__21_MoveNext_mCA96293BA9F9724AB205FEEF8FFD2FC71677752A,
	U3CStartCoroutineU3Ed__21_U3CU3Em__Finally1_m9E93B526D9B40F122720C047C1BF8980B27ECDC2,
	U3CStartCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7332937FCE0266371F4E8FFC90A79B92C8C4B26,
	U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5F6A852BE1A4B60DD15B80646C5D9B06BFEF2607,
	U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA3AFAF6DEDE798A50A95DB5391C076FFA3FFA921,
	PlayButton_Awake_mF5410C4C0FA5B54741C27CA899A337D14E54550A,
	PlayButton_Play_mB2459054EC2AD057C53B3C3435A557F65EB9B4C2,
	PlayButton__ctor_m8DD493422840621101705045AA587CF823674391,
	ResetButton_Awake_m2D334496A60A83C42F635D33DB44D6379FF58446,
	ResetButton_ResetFunc_mA401C3C6F815BDFB34D1FBD182AD19AF6ABFF8F6,
	ResetButton__ctor_m5FD6F16682DCC2AC71EC79637BB2C5CB4DF06162,
};
static const int32_t s_InvokerIndices[137] = 
{
	1985,
	1639,
	1639,
	1639,
	616,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1639,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1639,
	1639,
	1639,
	1985,
	1639,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1203,
	1985,
	1985,
	1985,
	1985,
	1639,
	1985,
	1985,
	1506,
	1985,
	1985,
	1985,
	1985,
	1939,
	1639,
	1985,
	1985,
	1985,
	1639,
	1639,
	1639,
	1639,
	1985,
	1985,
	1985,
	1639,
	1985,
	1625,
	1658,
	1985,
	1985,
	1985,
	1656,
	1985,
	1985,
	1985,
	1985,
	1639,
	1985,
	1985,
	1283,
	1286,
	1985,
	1625,
	1985,
	1961,
	1939,
	1985,
	1939,
	1625,
	1985,
	1961,
	1985,
	1939,
	1985,
	1939,
	1985,
	1985,
	1985,
	1286,
	1985,
	1639,
	1985,
	1985,
	1985,
	1985,
	1625,
	1985,
	1961,
	1985,
	1939,
	1985,
	1939,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	137,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
