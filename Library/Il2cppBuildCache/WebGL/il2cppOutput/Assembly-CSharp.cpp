﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38;
// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.CinemachineCore/UpdateStatus>
struct Dictionary_2_tEC0D7891F01585BC992E380FEDE139A2FEEB1C1D;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Cinemachine.CinemachineVirtualCameraBase>>
struct List_1_t404D2AF2D38681274B3783CCFE8103CC2AD8A790;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain>
struct List_1_tD11960D31B37E3E1E7900CD412637FCD8221ABA5;
// System.Collections.Generic.List`1<Cinemachine.CinemachineVirtualCameraBase>
struct List_1_tB774BAB8B129533A7AD9D22465C5D383A0A03AB5;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// ArduinoGenerator
struct ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0;
// AssociateImage
struct AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// BackBlock
struct BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876;
// Block
struct Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6;
// BlockList
struct BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// ByteHandler
struct ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87;
// CameraOnClick
struct CameraOnClick_t05317F351691E92E57D92AD28F0AEB457F442813;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5;
// Command
struct Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DeleteBlock
struct DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911;
// DragBlock
struct DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095;
// DragOrderContainer
struct DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C;
// DragOrderObject
struct DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF;
// DropBlock
struct DropBlock_tE708349ABBE8FEDF540361F3D1F2B5943A797EED;
// UnityEngine.Event
struct Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C;
// FloatHandler
struct FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// ForwardBlock
struct ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// GirarRueda
struct GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// MenuAppearScript
struct MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// Movement
struct Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C;
// Movement2
struct Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// PlayButton
struct PlayButton_tDDD3318D639982CFBD35C9DBF9E564031E331E74;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// ResetButton
struct ResetButton_tB0E49990488D04A82400DBE855AE500A97FEF862;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// RobotBlock
struct RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.Scrollbar
struct Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// StopBlock
struct StopBlock_t714D002381C475273632C74B513BC93BC959CE2B;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// TurnLeftBlock
struct TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51;
// TurnRightBlock
struct TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// WaitButton
struct WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// Cinemachine.CinemachineBrain/BrainEvent
struct BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E;
// Cinemachine.CinemachineCore/AxisInputDelegate
struct AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677;
// Cinemachine.CinemachineCore/GetBlendOverrideDelegate
struct GetBlendOverrideDelegate_t32AB037CCB52F195B40C363B70D34205538CE864;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// Movement/<StartCoroutine>d__19
struct U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653;
// Movement/<Wait>d__18
struct U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01;
// Movement2/<StartCoroutine>d__21
struct U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED;

IL2CPP_EXTERN_C RuntimeClass* AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral03E19BE510F62532D17030698FB484EE1A313C7B;
IL2CPP_EXTERN_C String_t* _stringLiteral0491ADD30E5917C8C04A75C1C1EB6BF687A9409A;
IL2CPP_EXTERN_C String_t* _stringLiteral05904614F13CDD99847CD524AF0AEEB5B54097C6;
IL2CPP_EXTERN_C String_t* _stringLiteral08DAB0E69E17233132E2852A52D5E9B216030586;
IL2CPP_EXTERN_C String_t* _stringLiteral097457485506FFE2120920CAFF6C9188A3633C28;
IL2CPP_EXTERN_C String_t* _stringLiteral119AA6FCB5FB898E8C39C3C9DCD4E9EC5617EDD0;
IL2CPP_EXTERN_C String_t* _stringLiteral1324C175BB20C5329A08CC59B116FA7352BD5345;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral186DE2ABC4189D1039B1EED81CC3519A6CF44C43;
IL2CPP_EXTERN_C String_t* _stringLiteral18A1641B70EB907EA2E51E5C3A353C146DB257AD;
IL2CPP_EXTERN_C String_t* _stringLiteral1A41CFC59BAB02C3D184BC2537B0806F27F3A8DA;
IL2CPP_EXTERN_C String_t* _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745;
IL2CPP_EXTERN_C String_t* _stringLiteral267CF425CCE42E050EFC2C100440735E81A83D3E;
IL2CPP_EXTERN_C String_t* _stringLiteral296C4C50A89B5CBD4B5F1A9FF0C9505E18EFAFE4;
IL2CPP_EXTERN_C String_t* _stringLiteral2E2F1EA0D344BCAB8D4F6CFC44175F4F7BE7426A;
IL2CPP_EXTERN_C String_t* _stringLiteral30E3EA07EC73E95C9F85B6230DEC6F3F0373CA42;
IL2CPP_EXTERN_C String_t* _stringLiteral381617D1A1C0C848CBE085A3C3BF523A03E9659F;
IL2CPP_EXTERN_C String_t* _stringLiteral3A3C5272E76787A96B4445991AF0A820F0D2D4EB;
IL2CPP_EXTERN_C String_t* _stringLiteral3E05657E3059A26857EC5E6B6D4F04751C2068C6;
IL2CPP_EXTERN_C String_t* _stringLiteral3EF05E27751242DAFA98456D71984290F4A7D9B7;
IL2CPP_EXTERN_C String_t* _stringLiteral40731BD58AF33D59E341C39E01AED51A758BB7D2;
IL2CPP_EXTERN_C String_t* _stringLiteral4124177097D1ACC17647DF0282FFDEAA6F8DE440;
IL2CPP_EXTERN_C String_t* _stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780;
IL2CPP_EXTERN_C String_t* _stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30;
IL2CPP_EXTERN_C String_t* _stringLiteral5A09B4D67FCE0A752E1575AA20BB3969E11DF7D0;
IL2CPP_EXTERN_C String_t* _stringLiteral61ED9CF67D51BD4F77B0D309402DEF714A30410F;
IL2CPP_EXTERN_C String_t* _stringLiteral62A1A6B5A17F0651671C4C3932F925E8905AC766;
IL2CPP_EXTERN_C String_t* _stringLiteral64FB9A957A6173E2C507D6355FB5425135847B8F;
IL2CPP_EXTERN_C String_t* _stringLiteral658ED20B6F1F35A1621E878CD7BC02F91BEF5CAD;
IL2CPP_EXTERN_C String_t* _stringLiteral6BD8D23564278A5521E9C41A48CE35C8D50840DB;
IL2CPP_EXTERN_C String_t* _stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9;
IL2CPP_EXTERN_C String_t* _stringLiteral71A91E1E2BA8A0E7A8B00B8A207C3A6271C0315C;
IL2CPP_EXTERN_C String_t* _stringLiteral7951DD056E1125FB16B9FBDD80EF24D3ACDF25DC;
IL2CPP_EXTERN_C String_t* _stringLiteral7F62D2472891FC0BE16BC43D30C6799DB2DFFCF6;
IL2CPP_EXTERN_C String_t* _stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral8D8BD8B8859DAA4E62064D44207BA69BCD02B29D;
IL2CPP_EXTERN_C String_t* _stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3;
IL2CPP_EXTERN_C String_t* _stringLiteral9F31E349F47EAB64F2C4E6FED306EED783D2FFBD;
IL2CPP_EXTERN_C String_t* _stringLiteralA14B87E468A7D39A25050FEDFC56E98BCD7EC451;
IL2CPP_EXTERN_C String_t* _stringLiteralA3C2AD8496590C39FDE4C8E6283F034228A22495;
IL2CPP_EXTERN_C String_t* _stringLiteralAA74EBB3519D8DB41082F53E2ED4D92CDB6F8E95;
IL2CPP_EXTERN_C String_t* _stringLiteralAC2071C756188E8EA70784BD8950905FBE44E605;
IL2CPP_EXTERN_C String_t* _stringLiteralAEEA6DA105A1A91802398EF794607D9EA3592322;
IL2CPP_EXTERN_C String_t* _stringLiteralB1CAC19850E8FBB5C3B416904FC8A2FFE585359E;
IL2CPP_EXTERN_C String_t* _stringLiteralB306896C52449864784FAC3C721A8B665DE5D0FC;
IL2CPP_EXTERN_C String_t* _stringLiteralB906955E0189BE72565D0EB9908073AD1D6FC350;
IL2CPP_EXTERN_C String_t* _stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB;
IL2CPP_EXTERN_C String_t* _stringLiteralC2ABD53443E87B1D4332B55DE89F3F4C04D71253;
IL2CPP_EXTERN_C String_t* _stringLiteralC5137AA17B77508EDDC5D36A6DC60C21D60C7249;
IL2CPP_EXTERN_C String_t* _stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65;
IL2CPP_EXTERN_C String_t* _stringLiteralC56A9D740D29F4C4F6B6057136491B75A782F6B6;
IL2CPP_EXTERN_C String_t* _stringLiteralC68705613E7D13EDE578EAD6DC3A12471F48A831;
IL2CPP_EXTERN_C String_t* _stringLiteralC8BCEAABDEC4BC12DC399591257F062D4C9A0971;
IL2CPP_EXTERN_C String_t* _stringLiteralD46A48F2F0F3D1880D5DCA203545EC790DDF514E;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE27198FCAB85C90DAADDF61FC3A44F1791D57C37;
IL2CPP_EXTERN_C String_t* _stringLiteralE353794CFB3E6B3AC8E2165765828E931F71E046;
IL2CPP_EXTERN_C String_t* _stringLiteralEECEAAC359EDB2E91D0F46F3CA9A65F47527BC5C;
IL2CPP_EXTERN_C String_t* _stringLiteralF279E3C88485EAF053B6870B5F6FE836949059D6;
IL2CPP_EXTERN_C String_t* _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024;
IL2CPP_EXTERN_C String_t* _stringLiteralF9F733B0E6A31938665A7D6E7FE78179490638A9;
IL2CPP_EXTERN_C String_t* _stringLiteralFDE5F3FA2740B216D61997915E97AF21359817A9;
IL2CPP_EXTERN_C const RuntimeMethod* CameraOnClick_GetAxisCustom_m6F5D6FBFD7F68E12C91A0E049542232AB786A125_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInParent_TisDragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C_mC5B7A6DD9A82DE4AAD11661768D2B40DC1BBC0F1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCommand_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5_m5A4F8E3AA330B41031E267A386DB12A4CBD69C99_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisEventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_m1B49BC63CCD7706DA1800174F4BB49E13E82A887_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisGraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_m3C58447B8D6B8A8D9F08E2A21B8058F221AF6008_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_m06FB4232ED756E269BDAE846E32BC8B0EA40B83A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_mC90489290B60555B1313D687BA31CF48C2D7626F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5F6A852BE1A4B60DD15B80646C5D9B06BFEF2607_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_mFF16C512E6FD39347FFCA92676A6C55D2FE6A8B5_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447, ____items_1)); }
	inline RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* get__items_1() const { return ____items_1; }
	inline RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447_StaticFields, ____emptyArray_5)); }
	inline RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____items_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// UnityEngine.EventSystems.AbstractEventData
struct AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// Movement/<Wait>d__18
struct U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01  : public RuntimeObject
{
public:
	// System.Int32 Movement/<Wait>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Movement/<Wait>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Movement/<Wait>d__18::seconds
	int32_t ___seconds_2;
	// System.Single Movement/<Wait>d__18::<duration>5__2
	float ___U3CdurationU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_seconds_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01, ___seconds_2)); }
	inline int32_t get_seconds_2() const { return ___seconds_2; }
	inline int32_t* get_address_of_seconds_2() { return &___seconds_2; }
	inline void set_seconds_2(int32_t value)
	{
		___seconds_2 = value;
	}

	inline static int32_t get_offset_of_U3CdurationU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01, ___U3CdurationU3E5__2_3)); }
	inline float get_U3CdurationU3E5__2_3() const { return ___U3CdurationU3E5__2_3; }
	inline float* get_address_of_U3CdurationU3E5__2_3() { return &___U3CdurationU3E5__2_3; }
	inline void set_U3CdurationU3E5__2_3(float value)
	{
		___U3CdurationU3E5__2_3 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___list_0)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_list_0() const { return ___list_0; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E  : public AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E, ___m_EventSystem_1)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___m_GameObject_0)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GameObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___module_1)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___module_1), (void*)value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldPosition_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldNormal_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___screenPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___screenPosition_9 = value;
	}

	inline static int32_t get_offset_of_displayIndex_10() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___displayIndex_10)); }
	inline int32_t get_displayIndex_10() const { return ___displayIndex_10; }
	inline int32_t* get_address_of_displayIndex_10() { return &___displayIndex_10; }
	inline void set_displayIndex_10(int32_t value)
	{
		___displayIndex_10 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// UnityEngine.TouchScreenKeyboardType
struct TouchScreenKeyboardType_tBD90DFB07923EC19E5EA59FAF26292AC2799A932 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_tBD90DFB07923EC19E5EA59FAF26292AC2799A932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// Cinemachine.CinemachineCore/UpdateFilter
struct UpdateFilter_t2C5466EF51B990B5103BE478BE3F326523407D3F 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/UpdateFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateFilter_t2C5466EF51B990B5103BE478BE3F326523407D3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.GraphicRaycaster/BlockingObjects
struct BlockingObjects_t3E2C52C921D1DE2C3EDB3FBC0685E319727BE810 
{
public:
	// System.Int32 UnityEngine.UI.GraphicRaycaster/BlockingObjects::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlockingObjects_t3E2C52C921D1DE2C3EDB3FBC0685E319727BE810, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/CharacterValidation
struct CharacterValidation_t03AFB752BBD6215579765978CE67D7159431FC41 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterValidation_t03AFB752BBD6215579765978CE67D7159431FC41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/ContentType
struct ContentType_t15FD47A38F32CADD417E3A07C787F1B3997B9AC1 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t15FD47A38F32CADD417E3A07C787F1B3997B9AC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/InputType
struct InputType_t43FE97C0C3EE1F7DB81E2F34420780D1DFBF03D2 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputType_t43FE97C0C3EE1F7DB81E2F34420780D1DFBF03D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/LineType
struct LineType_t3249F1C248D9D12DE265C49F371F2C3618AFEFCE 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t3249F1C248D9D12DE265C49F371F2C3618AFEFCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Movement/<StartCoroutine>d__19
struct U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653  : public RuntimeObject
{
public:
	// System.Int32 Movement/<StartCoroutine>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Movement/<StartCoroutine>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Movement Movement/<StartCoroutine>d__19::<>4__this
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<System.String> Movement/<StartCoroutine>d__19::comandos
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos_3;
	// System.Collections.Generic.List`1/Enumerator<System.String> Movement/<StartCoroutine>d__19::<>7__wrap1
	Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  ___U3CU3E7__wrap1_4;
	// System.String Movement/<StartCoroutine>d__19::<item>5__3
	String_t* ___U3CitemU3E5__3_5;
	// UnityEngine.Vector3 Movement/<StartCoroutine>d__19::<targetPosition>5__4
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CtargetPositionU3E5__4_6;
	// System.Int32 Movement/<StartCoroutine>d__19::<direction>5__5
	int32_t ___U3CdirectionU3E5__5_7;
	// UnityEngine.Vector3 Movement/<StartCoroutine>d__19::<transformRotationVec>5__6
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CtransformRotationVecU3E5__6_8;
	// System.Single Movement/<StartCoroutine>d__19::<duration>5__7
	float ___U3CdurationU3E5__7_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CU3E4__this_2)); }
	inline Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_comandos_3() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___comandos_3)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_comandos_3() const { return ___comandos_3; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_comandos_3() { return &___comandos_3; }
	inline void set_comandos_3(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___comandos_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comandos_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CU3E7__wrap1_4)); }
	inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_4))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_4))->___current_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CitemU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CitemU3E5__3_5)); }
	inline String_t* get_U3CitemU3E5__3_5() const { return ___U3CitemU3E5__3_5; }
	inline String_t** get_address_of_U3CitemU3E5__3_5() { return &___U3CitemU3E5__3_5; }
	inline void set_U3CitemU3E5__3_5(String_t* value)
	{
		___U3CitemU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CitemU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtargetPositionU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CtargetPositionU3E5__4_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CtargetPositionU3E5__4_6() const { return ___U3CtargetPositionU3E5__4_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CtargetPositionU3E5__4_6() { return &___U3CtargetPositionU3E5__4_6; }
	inline void set_U3CtargetPositionU3E5__4_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CtargetPositionU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CdirectionU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CdirectionU3E5__5_7)); }
	inline int32_t get_U3CdirectionU3E5__5_7() const { return ___U3CdirectionU3E5__5_7; }
	inline int32_t* get_address_of_U3CdirectionU3E5__5_7() { return &___U3CdirectionU3E5__5_7; }
	inline void set_U3CdirectionU3E5__5_7(int32_t value)
	{
		___U3CdirectionU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CtransformRotationVecU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CtransformRotationVecU3E5__6_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CtransformRotationVecU3E5__6_8() const { return ___U3CtransformRotationVecU3E5__6_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CtransformRotationVecU3E5__6_8() { return &___U3CtransformRotationVecU3E5__6_8; }
	inline void set_U3CtransformRotationVecU3E5__6_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CtransformRotationVecU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CdurationU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653, ___U3CdurationU3E5__7_9)); }
	inline float get_U3CdurationU3E5__7_9() const { return ___U3CdurationU3E5__7_9; }
	inline float* get_address_of_U3CdurationU3E5__7_9() { return &___U3CdurationU3E5__7_9; }
	inline void set_U3CdurationU3E5__7_9(float value)
	{
		___U3CdurationU3E5__7_9 = value;
	}
};


// Movement2/<StartCoroutine>d__21
struct U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360  : public RuntimeObject
{
public:
	// System.Int32 Movement2/<StartCoroutine>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Movement2/<StartCoroutine>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<System.String> Movement2/<StartCoroutine>d__21::comandos
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos_2;
	// Movement2 Movement2/<StartCoroutine>d__21::<>4__this
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1/Enumerator<System.String> Movement2/<StartCoroutine>d__21::<>7__wrap1
	Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  ___U3CU3E7__wrap1_4;
	// System.String Movement2/<StartCoroutine>d__21::<item>5__3
	String_t* ___U3CitemU3E5__3_5;
	// System.Single Movement2/<StartCoroutine>d__21::<duration>5__4
	float ___U3CdurationU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_comandos_2() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___comandos_2)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_comandos_2() const { return ___comandos_2; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_comandos_2() { return &___comandos_2; }
	inline void set_comandos_2(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___comandos_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comandos_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___U3CU3E4__this_3)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___U3CU3E7__wrap1_4)); }
	inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_4))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_4))->___current_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CitemU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___U3CitemU3E5__3_5)); }
	inline String_t* get_U3CitemU3E5__3_5() const { return ___U3CitemU3E5__3_5; }
	inline String_t** get_address_of_U3CitemU3E5__3_5() { return &___U3CitemU3E5__3_5; }
	inline void set_U3CitemU3E5__3_5(String_t* value)
	{
		___U3CitemU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CitemU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdurationU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360, ___U3CdurationU3E5__4_6)); }
	inline float get_U3CdurationU3E5__4_6() const { return ___U3CdurationU3E5__4_6; }
	inline float* get_address_of_U3CdurationU3E5__4_6() { return &___U3CdurationU3E5__4_6; }
	inline void set_U3CdurationU3E5__4_6(float value)
	{
		___U3CdurationU3E5__4_6 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData/InputButton
struct InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar/Direction
struct Direction_tCE7C4B78403A18007E901268411DB754E7B784B7 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tCE7C4B78403A18007E901268411DB754E7B784B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineCore
struct CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain> Cinemachine.CinemachineCore::mActiveBrains
	List_1_tD11960D31B37E3E1E7900CD412637FCD8221ABA5 * ___mActiveBrains_10;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineVirtualCameraBase> Cinemachine.CinemachineCore::mActiveCameras
	List_1_tB774BAB8B129533A7AD9D22465C5D383A0A03AB5 * ___mActiveCameras_11;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Cinemachine.CinemachineVirtualCameraBase>> Cinemachine.CinemachineCore::mAllCameras
	List_1_t404D2AF2D38681274B3783CCFE8103CC2AD8A790 * ___mAllCameras_12;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineCore::mRoundRobinVcamLastFrame
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___mRoundRobinVcamLastFrame_13;
	// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,Cinemachine.CinemachineCore/UpdateStatus> Cinemachine.CinemachineCore::mUpdateStatus
	Dictionary_2_tEC0D7891F01585BC992E380FEDE139A2FEEB1C1D * ___mUpdateStatus_16;
	// Cinemachine.CinemachineCore/UpdateFilter Cinemachine.CinemachineCore::<CurrentUpdateFilter>k__BackingField
	int32_t ___U3CCurrentUpdateFilterU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_mActiveBrains_10() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534, ___mActiveBrains_10)); }
	inline List_1_tD11960D31B37E3E1E7900CD412637FCD8221ABA5 * get_mActiveBrains_10() const { return ___mActiveBrains_10; }
	inline List_1_tD11960D31B37E3E1E7900CD412637FCD8221ABA5 ** get_address_of_mActiveBrains_10() { return &___mActiveBrains_10; }
	inline void set_mActiveBrains_10(List_1_tD11960D31B37E3E1E7900CD412637FCD8221ABA5 * value)
	{
		___mActiveBrains_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mActiveBrains_10), (void*)value);
	}

	inline static int32_t get_offset_of_mActiveCameras_11() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534, ___mActiveCameras_11)); }
	inline List_1_tB774BAB8B129533A7AD9D22465C5D383A0A03AB5 * get_mActiveCameras_11() const { return ___mActiveCameras_11; }
	inline List_1_tB774BAB8B129533A7AD9D22465C5D383A0A03AB5 ** get_address_of_mActiveCameras_11() { return &___mActiveCameras_11; }
	inline void set_mActiveCameras_11(List_1_tB774BAB8B129533A7AD9D22465C5D383A0A03AB5 * value)
	{
		___mActiveCameras_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mActiveCameras_11), (void*)value);
	}

	inline static int32_t get_offset_of_mAllCameras_12() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534, ___mAllCameras_12)); }
	inline List_1_t404D2AF2D38681274B3783CCFE8103CC2AD8A790 * get_mAllCameras_12() const { return ___mAllCameras_12; }
	inline List_1_t404D2AF2D38681274B3783CCFE8103CC2AD8A790 ** get_address_of_mAllCameras_12() { return &___mAllCameras_12; }
	inline void set_mAllCameras_12(List_1_t404D2AF2D38681274B3783CCFE8103CC2AD8A790 * value)
	{
		___mAllCameras_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mAllCameras_12), (void*)value);
	}

	inline static int32_t get_offset_of_mRoundRobinVcamLastFrame_13() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534, ___mRoundRobinVcamLastFrame_13)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_mRoundRobinVcamLastFrame_13() const { return ___mRoundRobinVcamLastFrame_13; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_mRoundRobinVcamLastFrame_13() { return &___mRoundRobinVcamLastFrame_13; }
	inline void set_mRoundRobinVcamLastFrame_13(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___mRoundRobinVcamLastFrame_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRoundRobinVcamLastFrame_13), (void*)value);
	}

	inline static int32_t get_offset_of_mUpdateStatus_16() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534, ___mUpdateStatus_16)); }
	inline Dictionary_2_tEC0D7891F01585BC992E380FEDE139A2FEEB1C1D * get_mUpdateStatus_16() const { return ___mUpdateStatus_16; }
	inline Dictionary_2_tEC0D7891F01585BC992E380FEDE139A2FEEB1C1D ** get_address_of_mUpdateStatus_16() { return &___mUpdateStatus_16; }
	inline void set_mUpdateStatus_16(Dictionary_2_tEC0D7891F01585BC992E380FEDE139A2FEEB1C1D * value)
	{
		___mUpdateStatus_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mUpdateStatus_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentUpdateFilterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534, ___U3CCurrentUpdateFilterU3Ek__BackingField_17)); }
	inline int32_t get_U3CCurrentUpdateFilterU3Ek__BackingField_17() const { return ___U3CCurrentUpdateFilterU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CCurrentUpdateFilterU3Ek__BackingField_17() { return &___U3CCurrentUpdateFilterU3Ek__BackingField_17; }
	inline void set_U3CCurrentUpdateFilterU3Ek__BackingField_17(int32_t value)
	{
		___U3CCurrentUpdateFilterU3Ek__BackingField_17 = value;
	}
};

struct CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields
{
public:
	// System.Int32 Cinemachine.CinemachineCore::kStreamingVersion
	int32_t ___kStreamingVersion_0;
	// System.String Cinemachine.CinemachineCore::kVersionString
	String_t* ___kVersionString_1;
	// Cinemachine.CinemachineCore Cinemachine.CinemachineCore::sInstance
	CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534 * ___sInstance_2;
	// System.Boolean Cinemachine.CinemachineCore::sShowHiddenObjects
	bool ___sShowHiddenObjects_3;
	// Cinemachine.CinemachineCore/AxisInputDelegate Cinemachine.CinemachineCore::GetInputAxis
	AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 * ___GetInputAxis_4;
	// System.Single Cinemachine.CinemachineCore::UniformDeltaTimeOverride
	float ___UniformDeltaTimeOverride_5;
	// System.Single Cinemachine.CinemachineCore::CurrentTimeOverride
	float ___CurrentTimeOverride_6;
	// Cinemachine.CinemachineCore/GetBlendOverrideDelegate Cinemachine.CinemachineCore::GetBlendOverride
	GetBlendOverrideDelegate_t32AB037CCB52F195B40C363B70D34205538CE864 * ___GetBlendOverride_7;
	// Cinemachine.CinemachineBrain/BrainEvent Cinemachine.CinemachineCore::CameraUpdatedEvent
	BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * ___CameraUpdatedEvent_8;
	// Cinemachine.CinemachineBrain/BrainEvent Cinemachine.CinemachineCore::CameraCutEvent
	BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * ___CameraCutEvent_9;
	// System.Single Cinemachine.CinemachineCore::mLastUpdateTime
	float ___mLastUpdateTime_14;
	// System.Int32 Cinemachine.CinemachineCore::<FixedFrameCount>k__BackingField
	int32_t ___U3CFixedFrameCountU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_kStreamingVersion_0() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___kStreamingVersion_0)); }
	inline int32_t get_kStreamingVersion_0() const { return ___kStreamingVersion_0; }
	inline int32_t* get_address_of_kStreamingVersion_0() { return &___kStreamingVersion_0; }
	inline void set_kStreamingVersion_0(int32_t value)
	{
		___kStreamingVersion_0 = value;
	}

	inline static int32_t get_offset_of_kVersionString_1() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___kVersionString_1)); }
	inline String_t* get_kVersionString_1() const { return ___kVersionString_1; }
	inline String_t** get_address_of_kVersionString_1() { return &___kVersionString_1; }
	inline void set_kVersionString_1(String_t* value)
	{
		___kVersionString_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kVersionString_1), (void*)value);
	}

	inline static int32_t get_offset_of_sInstance_2() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___sInstance_2)); }
	inline CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534 * get_sInstance_2() const { return ___sInstance_2; }
	inline CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534 ** get_address_of_sInstance_2() { return &___sInstance_2; }
	inline void set_sInstance_2(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534 * value)
	{
		___sInstance_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sInstance_2), (void*)value);
	}

	inline static int32_t get_offset_of_sShowHiddenObjects_3() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___sShowHiddenObjects_3)); }
	inline bool get_sShowHiddenObjects_3() const { return ___sShowHiddenObjects_3; }
	inline bool* get_address_of_sShowHiddenObjects_3() { return &___sShowHiddenObjects_3; }
	inline void set_sShowHiddenObjects_3(bool value)
	{
		___sShowHiddenObjects_3 = value;
	}

	inline static int32_t get_offset_of_GetInputAxis_4() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___GetInputAxis_4)); }
	inline AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 * get_GetInputAxis_4() const { return ___GetInputAxis_4; }
	inline AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 ** get_address_of_GetInputAxis_4() { return &___GetInputAxis_4; }
	inline void set_GetInputAxis_4(AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 * value)
	{
		___GetInputAxis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetInputAxis_4), (void*)value);
	}

	inline static int32_t get_offset_of_UniformDeltaTimeOverride_5() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___UniformDeltaTimeOverride_5)); }
	inline float get_UniformDeltaTimeOverride_5() const { return ___UniformDeltaTimeOverride_5; }
	inline float* get_address_of_UniformDeltaTimeOverride_5() { return &___UniformDeltaTimeOverride_5; }
	inline void set_UniformDeltaTimeOverride_5(float value)
	{
		___UniformDeltaTimeOverride_5 = value;
	}

	inline static int32_t get_offset_of_CurrentTimeOverride_6() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___CurrentTimeOverride_6)); }
	inline float get_CurrentTimeOverride_6() const { return ___CurrentTimeOverride_6; }
	inline float* get_address_of_CurrentTimeOverride_6() { return &___CurrentTimeOverride_6; }
	inline void set_CurrentTimeOverride_6(float value)
	{
		___CurrentTimeOverride_6 = value;
	}

	inline static int32_t get_offset_of_GetBlendOverride_7() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___GetBlendOverride_7)); }
	inline GetBlendOverrideDelegate_t32AB037CCB52F195B40C363B70D34205538CE864 * get_GetBlendOverride_7() const { return ___GetBlendOverride_7; }
	inline GetBlendOverrideDelegate_t32AB037CCB52F195B40C363B70D34205538CE864 ** get_address_of_GetBlendOverride_7() { return &___GetBlendOverride_7; }
	inline void set_GetBlendOverride_7(GetBlendOverrideDelegate_t32AB037CCB52F195B40C363B70D34205538CE864 * value)
	{
		___GetBlendOverride_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetBlendOverride_7), (void*)value);
	}

	inline static int32_t get_offset_of_CameraUpdatedEvent_8() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___CameraUpdatedEvent_8)); }
	inline BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * get_CameraUpdatedEvent_8() const { return ___CameraUpdatedEvent_8; }
	inline BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E ** get_address_of_CameraUpdatedEvent_8() { return &___CameraUpdatedEvent_8; }
	inline void set_CameraUpdatedEvent_8(BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * value)
	{
		___CameraUpdatedEvent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CameraUpdatedEvent_8), (void*)value);
	}

	inline static int32_t get_offset_of_CameraCutEvent_9() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___CameraCutEvent_9)); }
	inline BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * get_CameraCutEvent_9() const { return ___CameraCutEvent_9; }
	inline BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E ** get_address_of_CameraCutEvent_9() { return &___CameraCutEvent_9; }
	inline void set_CameraCutEvent_9(BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * value)
	{
		___CameraCutEvent_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CameraCutEvent_9), (void*)value);
	}

	inline static int32_t get_offset_of_mLastUpdateTime_14() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___mLastUpdateTime_14)); }
	inline float get_mLastUpdateTime_14() const { return ___mLastUpdateTime_14; }
	inline float* get_address_of_mLastUpdateTime_14() { return &___mLastUpdateTime_14; }
	inline void set_mLastUpdateTime_14(float value)
	{
		___mLastUpdateTime_14 = value;
	}

	inline static int32_t get_offset_of_U3CFixedFrameCountU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields, ___U3CFixedFrameCountU3Ek__BackingField_15)); }
	inline int32_t get_U3CFixedFrameCountU3Ek__BackingField_15() const { return ___U3CFixedFrameCountU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CFixedFrameCountU3Ek__BackingField_15() { return &___U3CFixedFrameCountU3Ek__BackingField_15; }
	inline void set_U3CFixedFrameCountU3Ek__BackingField_15(int32_t value)
	{
		___U3CFixedFrameCountU3Ek__BackingField_15 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954  : public BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerClick>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerClickU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerCurrentRaycastU3Ek__BackingField_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerPressRaycastU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___hovered_10;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_11;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaU3Ek__BackingField_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldPositionU3Ek__BackingField_16;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldNormalU3Ek__BackingField_17;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_18;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscrollDeltaU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_21;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_22;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerEnterU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___m_PointerPress_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerPress_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClastPressU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrawPointerPressU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerDragU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerClickU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerClickU3Ek__BackingField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerClickU3Ek__BackingField_7() const { return ___U3CpointerClickU3Ek__BackingField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerClickU3Ek__BackingField_7() { return &___U3CpointerClickU3Ek__BackingField_7; }
	inline void set_U3CpointerClickU3Ek__BackingField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerClickU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerClickU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerCurrentRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerCurrentRaycastU3Ek__BackingField_8() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_8(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerPressRaycastU3Ek__BackingField_9)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerPressRaycastU3Ek__BackingField_9() const { return ___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return &___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_9(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hovered_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___hovered_10)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_hovered_10() const { return ___hovered_10; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_hovered_10() { return &___hovered_10; }
	inline void set_hovered_10(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___hovered_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hovered_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CeligibleForClickU3Ek__BackingField_11)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_11() const { return ___U3CeligibleForClickU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_11() { return &___U3CeligibleForClickU3Ek__BackingField_11; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_11(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerIdU3Ek__BackingField_12)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_12() const { return ___U3CpointerIdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_12() { return &___U3CpointerIdU3Ek__BackingField_12; }
	inline void set_U3CpointerIdU3Ek__BackingField_12(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpositionU3Ek__BackingField_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpositionU3Ek__BackingField_13() const { return ___U3CpositionU3Ek__BackingField_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpositionU3Ek__BackingField_13() { return &___U3CpositionU3Ek__BackingField_13; }
	inline void set_U3CpositionU3Ek__BackingField_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpositionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdeltaU3Ek__BackingField_14)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CdeltaU3Ek__BackingField_14() const { return ___U3CdeltaU3Ek__BackingField_14; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CdeltaU3Ek__BackingField_14() { return &___U3CdeltaU3Ek__BackingField_14; }
	inline void set_U3CdeltaU3Ek__BackingField_14(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CdeltaU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpressPositionU3Ek__BackingField_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressPositionU3Ek__BackingField_15() const { return ___U3CpressPositionU3Ek__BackingField_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressPositionU3Ek__BackingField_15() { return &___U3CpressPositionU3Ek__BackingField_15; }
	inline void set_U3CpressPositionU3Ek__BackingField_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldPositionU3Ek__BackingField_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldPositionU3Ek__BackingField_16() const { return ___U3CworldPositionU3Ek__BackingField_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldPositionU3Ek__BackingField_16() { return &___U3CworldPositionU3Ek__BackingField_16; }
	inline void set_U3CworldPositionU3Ek__BackingField_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldPositionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldNormalU3Ek__BackingField_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldNormalU3Ek__BackingField_17() const { return ___U3CworldNormalU3Ek__BackingField_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldNormalU3Ek__BackingField_17() { return &___U3CworldNormalU3Ek__BackingField_17; }
	inline void set_U3CworldNormalU3Ek__BackingField_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldNormalU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickTimeU3Ek__BackingField_18)); }
	inline float get_U3CclickTimeU3Ek__BackingField_18() const { return ___U3CclickTimeU3Ek__BackingField_18; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_18() { return &___U3CclickTimeU3Ek__BackingField_18; }
	inline void set_U3CclickTimeU3Ek__BackingField_18(float value)
	{
		___U3CclickTimeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickCountU3Ek__BackingField_19)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_19() const { return ___U3CclickCountU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_19() { return &___U3CclickCountU3Ek__BackingField_19; }
	inline void set_U3CclickCountU3Ek__BackingField_19(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CscrollDeltaU3Ek__BackingField_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CscrollDeltaU3Ek__BackingField_20() const { return ___U3CscrollDeltaU3Ek__BackingField_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CscrollDeltaU3Ek__BackingField_20() { return &___U3CscrollDeltaU3Ek__BackingField_20; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CuseDragThresholdU3Ek__BackingField_21)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_21() const { return ___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_21() { return &___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_21(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdraggingU3Ek__BackingField_22)); }
	inline bool get_U3CdraggingU3Ek__BackingField_22() const { return ___U3CdraggingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_22() { return &___U3CdraggingU3Ek__BackingField_22; }
	inline void set_U3CdraggingU3Ek__BackingField_22(bool value)
	{
		___U3CdraggingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CbuttonU3Ek__BackingField_23)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_23() const { return ___U3CbuttonU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_23() { return &___U3CbuttonU3Ek__BackingField_23; }
	inline void set_U3CbuttonU3Ek__BackingField_23(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_23 = value;
	}
};


// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// Cinemachine.CinemachineCore/AxisInputDelegate
struct AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// ArduinoGenerator
struct ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text ArduinoGenerator::code
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___code_4;
	// System.String ArduinoGenerator::mensajeAutomatizado
	String_t* ___mensajeAutomatizado_5;
	// System.Collections.Generic.List`1<System.String> ArduinoGenerator::definitions
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___definitions_6;
	// System.Collections.Generic.List`1<System.String> ArduinoGenerator::setup
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___setup_7;
	// System.Collections.Generic.List`1<System.String> ArduinoGenerator::loop
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___loop_8;

public:
	inline static int32_t get_offset_of_code_4() { return static_cast<int32_t>(offsetof(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0, ___code_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_code_4() const { return ___code_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_code_4() { return &___code_4; }
	inline void set_code_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___code_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___code_4), (void*)value);
	}

	inline static int32_t get_offset_of_mensajeAutomatizado_5() { return static_cast<int32_t>(offsetof(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0, ___mensajeAutomatizado_5)); }
	inline String_t* get_mensajeAutomatizado_5() const { return ___mensajeAutomatizado_5; }
	inline String_t** get_address_of_mensajeAutomatizado_5() { return &___mensajeAutomatizado_5; }
	inline void set_mensajeAutomatizado_5(String_t* value)
	{
		___mensajeAutomatizado_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mensajeAutomatizado_5), (void*)value);
	}

	inline static int32_t get_offset_of_definitions_6() { return static_cast<int32_t>(offsetof(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0, ___definitions_6)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_definitions_6() const { return ___definitions_6; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_definitions_6() { return &___definitions_6; }
	inline void set_definitions_6(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___definitions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___definitions_6), (void*)value);
	}

	inline static int32_t get_offset_of_setup_7() { return static_cast<int32_t>(offsetof(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0, ___setup_7)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_setup_7() const { return ___setup_7; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_setup_7() { return &___setup_7; }
	inline void set_setup_7(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___setup_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setup_7), (void*)value);
	}

	inline static int32_t get_offset_of_loop_8() { return static_cast<int32_t>(offsetof(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0, ___loop_8)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_loop_8() const { return ___loop_8; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_loop_8() { return &___loop_8; }
	inline void set_loop_8(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___loop_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loop_8), (void*)value);
	}
};


// AssociateImage
struct AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single AssociateImage::scaleFactorX
	float ___scaleFactorX_4;
	// System.Single AssociateImage::scaleFactorY
	float ___scaleFactorY_5;
	// System.Single AssociateImage::scaleFactorZ
	float ___scaleFactorZ_6;
	// UnityEngine.SpriteRenderer AssociateImage::image
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___image_7;
	// UnityEngine.Material AssociateImage::mat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat_8;
	// UnityEngine.GameObject AssociateImage::display
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___display_9;

public:
	inline static int32_t get_offset_of_scaleFactorX_4() { return static_cast<int32_t>(offsetof(AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8, ___scaleFactorX_4)); }
	inline float get_scaleFactorX_4() const { return ___scaleFactorX_4; }
	inline float* get_address_of_scaleFactorX_4() { return &___scaleFactorX_4; }
	inline void set_scaleFactorX_4(float value)
	{
		___scaleFactorX_4 = value;
	}

	inline static int32_t get_offset_of_scaleFactorY_5() { return static_cast<int32_t>(offsetof(AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8, ___scaleFactorY_5)); }
	inline float get_scaleFactorY_5() const { return ___scaleFactorY_5; }
	inline float* get_address_of_scaleFactorY_5() { return &___scaleFactorY_5; }
	inline void set_scaleFactorY_5(float value)
	{
		___scaleFactorY_5 = value;
	}

	inline static int32_t get_offset_of_scaleFactorZ_6() { return static_cast<int32_t>(offsetof(AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8, ___scaleFactorZ_6)); }
	inline float get_scaleFactorZ_6() const { return ___scaleFactorZ_6; }
	inline float* get_address_of_scaleFactorZ_6() { return &___scaleFactorZ_6; }
	inline void set_scaleFactorZ_6(float value)
	{
		___scaleFactorZ_6 = value;
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8, ___image_7)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_image_7() const { return ___image_7; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_7), (void*)value);
	}

	inline static int32_t get_offset_of_mat_8() { return static_cast<int32_t>(offsetof(AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8, ___mat_8)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_mat_8() const { return ___mat_8; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_mat_8() { return &___mat_8; }
	inline void set_mat_8(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___mat_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mat_8), (void*)value);
	}

	inline static int32_t get_offset_of_display_9() { return static_cast<int32_t>(offsetof(AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8, ___display_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_display_9() const { return ___display_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_display_9() { return &___display_9; }
	inline void set_display_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___display_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___display_9), (void*)value);
	}
};


// Block
struct Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Block::conectaParriba
	bool ___conectaParriba_4;
	// System.Boolean Block::conectaPabajo
	bool ___conectaPabajo_5;

public:
	inline static int32_t get_offset_of_conectaParriba_4() { return static_cast<int32_t>(offsetof(Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6, ___conectaParriba_4)); }
	inline bool get_conectaParriba_4() const { return ___conectaParriba_4; }
	inline bool* get_address_of_conectaParriba_4() { return &___conectaParriba_4; }
	inline void set_conectaParriba_4(bool value)
	{
		___conectaParriba_4 = value;
	}

	inline static int32_t get_offset_of_conectaPabajo_5() { return static_cast<int32_t>(offsetof(Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6, ___conectaPabajo_5)); }
	inline bool get_conectaPabajo_5() const { return ___conectaPabajo_5; }
	inline bool* get_address_of_conectaPabajo_5() { return &___conectaPabajo_5; }
	inline void set_conectaPabajo_5(bool value)
	{
		___conectaPabajo_5 = value;
	}
};


// BlockList
struct BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject BlockList::robotBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___robotBlock_4;
	// UnityEngine.GameObject BlockList::forwardBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___forwardBlock_5;
	// UnityEngine.GameObject BlockList::backBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___backBlock_6;
	// UnityEngine.GameObject BlockList::turnRightBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___turnRightBlock_7;
	// UnityEngine.GameObject BlockList::turnLeftBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___turnLeftBlock_8;
	// UnityEngine.GameObject BlockList::waitBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___waitBlock_9;
	// UnityEngine.GameObject BlockList::stopBlock
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___stopBlock_10;
	// ArduinoGenerator BlockList::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_11;
	// Movement2 BlockList::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_12;
	// UnityEngine.GameObject BlockList::blockList
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___blockList_13;
	// UnityEngine.UI.Scrollbar BlockList::sb
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___sb_14;
	// System.Char BlockList::separation
	Il2CppChar ___separation_15;

public:
	inline static int32_t get_offset_of_robotBlock_4() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___robotBlock_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_robotBlock_4() const { return ___robotBlock_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_robotBlock_4() { return &___robotBlock_4; }
	inline void set_robotBlock_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___robotBlock_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___robotBlock_4), (void*)value);
	}

	inline static int32_t get_offset_of_forwardBlock_5() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___forwardBlock_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_forwardBlock_5() const { return ___forwardBlock_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_forwardBlock_5() { return &___forwardBlock_5; }
	inline void set_forwardBlock_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___forwardBlock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___forwardBlock_5), (void*)value);
	}

	inline static int32_t get_offset_of_backBlock_6() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___backBlock_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_backBlock_6() const { return ___backBlock_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_backBlock_6() { return &___backBlock_6; }
	inline void set_backBlock_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___backBlock_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backBlock_6), (void*)value);
	}

	inline static int32_t get_offset_of_turnRightBlock_7() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___turnRightBlock_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_turnRightBlock_7() const { return ___turnRightBlock_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_turnRightBlock_7() { return &___turnRightBlock_7; }
	inline void set_turnRightBlock_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___turnRightBlock_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnRightBlock_7), (void*)value);
	}

	inline static int32_t get_offset_of_turnLeftBlock_8() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___turnLeftBlock_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_turnLeftBlock_8() const { return ___turnLeftBlock_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_turnLeftBlock_8() { return &___turnLeftBlock_8; }
	inline void set_turnLeftBlock_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___turnLeftBlock_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnLeftBlock_8), (void*)value);
	}

	inline static int32_t get_offset_of_waitBlock_9() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___waitBlock_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_waitBlock_9() const { return ___waitBlock_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_waitBlock_9() { return &___waitBlock_9; }
	inline void set_waitBlock_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___waitBlock_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waitBlock_9), (void*)value);
	}

	inline static int32_t get_offset_of_stopBlock_10() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___stopBlock_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_stopBlock_10() const { return ___stopBlock_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_stopBlock_10() { return &___stopBlock_10; }
	inline void set_stopBlock_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___stopBlock_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stopBlock_10), (void*)value);
	}

	inline static int32_t get_offset_of_ag_11() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___ag_11)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_11() const { return ___ag_11; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_11() { return &___ag_11; }
	inline void set_ag_11(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_11), (void*)value);
	}

	inline static int32_t get_offset_of_player_12() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___player_12)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_12() const { return ___player_12; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_12() { return &___player_12; }
	inline void set_player_12(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_12), (void*)value);
	}

	inline static int32_t get_offset_of_blockList_13() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___blockList_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_blockList_13() const { return ___blockList_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_blockList_13() { return &___blockList_13; }
	inline void set_blockList_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___blockList_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blockList_13), (void*)value);
	}

	inline static int32_t get_offset_of_sb_14() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___sb_14)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_sb_14() const { return ___sb_14; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_sb_14() { return &___sb_14; }
	inline void set_sb_14(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___sb_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sb_14), (void*)value);
	}

	inline static int32_t get_offset_of_separation_15() { return static_cast<int32_t>(offsetof(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F, ___separation_15)); }
	inline Il2CppChar get_separation_15() const { return ___separation_15; }
	inline Il2CppChar* get_address_of_separation_15() { return &___separation_15; }
	inline void set_separation_15(Il2CppChar value)
	{
		___separation_15 = value;
	}
};


// ByteHandler
struct ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.InputField ByteHandler::number
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___number_4;

public:
	inline static int32_t get_offset_of_number_4() { return static_cast<int32_t>(offsetof(ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87, ___number_4)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_number_4() const { return ___number_4; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_number_4() { return &___number_4; }
	inline void set_number_4(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___number_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___number_4), (void*)value);
	}
};


// CameraOnClick
struct CameraOnClick_t05317F351691E92E57D92AD28F0AEB457F442813  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Command
struct Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Command::command
	String_t* ___command_4;

public:
	inline static int32_t get_offset_of_command_4() { return static_cast<int32_t>(offsetof(Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5, ___command_4)); }
	inline String_t* get_command_4() const { return ___command_4; }
	inline String_t** get_address_of_command_4() { return &___command_4; }
	inline void set_command_4(String_t* value)
	{
		___command_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___command_4), (void*)value);
	}
};


// DeleteBlock
struct DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.GraphicRaycaster DeleteBlock::m_Raycaster
	GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * ___m_Raycaster_4;
	// UnityEngine.EventSystems.PointerEventData DeleteBlock::m_PointerEventData
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___m_PointerEventData_5;
	// UnityEngine.EventSystems.EventSystem DeleteBlock::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_6;

public:
	inline static int32_t get_offset_of_m_Raycaster_4() { return static_cast<int32_t>(offsetof(DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911, ___m_Raycaster_4)); }
	inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * get_m_Raycaster_4() const { return ___m_Raycaster_4; }
	inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 ** get_address_of_m_Raycaster_4() { return &___m_Raycaster_4; }
	inline void set_m_Raycaster_4(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * value)
	{
		___m_Raycaster_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Raycaster_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerEventData_5() { return static_cast<int32_t>(offsetof(DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911, ___m_PointerEventData_5)); }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * get_m_PointerEventData_5() const { return ___m_PointerEventData_5; }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 ** get_address_of_m_PointerEventData_5() { return &___m_PointerEventData_5; }
	inline void set_m_PointerEventData_5(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * value)
	{
		___m_PointerEventData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerEventData_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911, ___m_EventSystem_6)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_6), (void*)value);
	}
};


// DragBlock
struct DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields
{
public:
	// UnityEngine.GameObject DragBlock::ItemBeingDragged
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ItemBeingDragged_4;
	// Command DragBlock::command
	Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * ___command_5;

public:
	inline static int32_t get_offset_of_ItemBeingDragged_4() { return static_cast<int32_t>(offsetof(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields, ___ItemBeingDragged_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ItemBeingDragged_4() const { return ___ItemBeingDragged_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ItemBeingDragged_4() { return &___ItemBeingDragged_4; }
	inline void set_ItemBeingDragged_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ItemBeingDragged_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ItemBeingDragged_4), (void*)value);
	}

	inline static int32_t get_offset_of_command_5() { return static_cast<int32_t>(offsetof(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields, ___command_5)); }
	inline Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * get_command_5() const { return ___command_5; }
	inline Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 ** get_address_of_command_5() { return &___command_5; }
	inline void set_command_5(Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * value)
	{
		___command_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___command_5), (void*)value);
	}
};


// DragOrderContainer
struct DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject DragOrderContainer::<objectBeingDragged>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CobjectBeingDraggedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CobjectBeingDraggedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C, ___U3CobjectBeingDraggedU3Ek__BackingField_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CobjectBeingDraggedU3Ek__BackingField_4() const { return ___U3CobjectBeingDraggedU3Ek__BackingField_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CobjectBeingDraggedU3Ek__BackingField_4() { return &___U3CobjectBeingDraggedU3Ek__BackingField_4; }
	inline void set_U3CobjectBeingDraggedU3Ek__BackingField_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CobjectBeingDraggedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CobjectBeingDraggedU3Ek__BackingField_4), (void*)value);
	}
};


// DragOrderObject
struct DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DragOrderContainer DragOrderObject::container
	DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * ___container_4;

public:
	inline static int32_t get_offset_of_container_4() { return static_cast<int32_t>(offsetof(DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF, ___container_4)); }
	inline DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * get_container_4() const { return ___container_4; }
	inline DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C ** get_address_of_container_4() { return &___container_4; }
	inline void set_container_4(DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * value)
	{
		___container_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___container_4), (void*)value);
	}
};


// DropBlock
struct DropBlock_tE708349ABBE8FEDF540361F3D1F2B5943A797EED  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// BlockList DropBlock::bl
	BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * ___bl_4;

public:
	inline static int32_t get_offset_of_bl_4() { return static_cast<int32_t>(offsetof(DropBlock_tE708349ABBE8FEDF540361F3D1F2B5943A797EED, ___bl_4)); }
	inline BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * get_bl_4() const { return ___bl_4; }
	inline BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F ** get_address_of_bl_4() { return &___bl_4; }
	inline void set_bl_4(BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * value)
	{
		___bl_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bl_4), (void*)value);
	}
};


// FloatHandler
struct FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.InputField FloatHandler::number
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___number_4;

public:
	inline static int32_t get_offset_of_number_4() { return static_cast<int32_t>(offsetof(FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC, ___number_4)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_number_4() const { return ___number_4; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_number_4() { return &___number_4; }
	inline void set_number_4(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___number_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___number_4), (void*)value);
	}
};


// GirarRueda
struct GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single GirarRueda::speed
	float ___speed_4;
	// System.Single GirarRueda::baseValue
	float ___baseValue_5;
	// System.Single GirarRueda::neverchanging
	float ___neverchanging_6;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_baseValue_5() { return static_cast<int32_t>(offsetof(GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B, ___baseValue_5)); }
	inline float get_baseValue_5() const { return ___baseValue_5; }
	inline float* get_address_of_baseValue_5() { return &___baseValue_5; }
	inline void set_baseValue_5(float value)
	{
		___baseValue_5 = value;
	}

	inline static int32_t get_offset_of_neverchanging_6() { return static_cast<int32_t>(offsetof(GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B, ___neverchanging_6)); }
	inline float get_neverchanging_6() const { return ___neverchanging_6; }
	inline float* get_address_of_neverchanging_6() { return &___neverchanging_6; }
	inline void set_neverchanging_6(float value)
	{
		___neverchanging_6 = value;
	}
};


// MenuAppearScript
struct MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite MenuAppearScript::first
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___first_4;
	// UnityEngine.Sprite MenuAppearScript::second
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___second_5;
	// UnityEngine.GameObject MenuAppearScript::menu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___menu_6;
	// System.Boolean MenuAppearScript::isShowing
	bool ___isShowing_7;

public:
	inline static int32_t get_offset_of_first_4() { return static_cast<int32_t>(offsetof(MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8, ___first_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_first_4() const { return ___first_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_first_4() { return &___first_4; }
	inline void set_first_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___first_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___first_4), (void*)value);
	}

	inline static int32_t get_offset_of_second_5() { return static_cast<int32_t>(offsetof(MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8, ___second_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_second_5() const { return ___second_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_second_5() { return &___second_5; }
	inline void set_second_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___second_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___second_5), (void*)value);
	}

	inline static int32_t get_offset_of_menu_6() { return static_cast<int32_t>(offsetof(MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8, ___menu_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_menu_6() const { return ___menu_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_menu_6() { return &___menu_6; }
	inline void set_menu_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___menu_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menu_6), (void*)value);
	}

	inline static int32_t get_offset_of_isShowing_7() { return static_cast<int32_t>(offsetof(MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8, ___isShowing_7)); }
	inline bool get_isShowing_7() const { return ___isShowing_7; }
	inline bool* get_address_of_isShowing_7() { return &___isShowing_7; }
	inline void set_isShowing_7(bool value)
	{
		___isShowing_7 = value;
	}
};


// Movement
struct Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Rigidbody Movement::rb
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___rb_4;
	// System.Single Movement::turnSpeed
	float ___turnSpeed_5;
	// System.Single Movement::moveSpeed
	float ___moveSpeed_6;
	// System.Collections.Generic.List`1<System.String> Movement::comandos
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos_7;
	// UnityEngine.Transform Movement::startPos
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___startPos_8;
	// UnityEngine.UI.Text Movement::commandText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___commandText_9;
	// System.Boolean Movement::startMove
	bool ___startMove_10;
	// System.Boolean Movement::stopped
	bool ___stopped_11;
	// System.Char Movement::separation
	Il2CppChar ___separation_12;
	// System.Single Movement::distance
	float ___distance_13;
	// System.Single Movement::waitSeconds
	float ___waitSeconds_14;
	// UnityEngine.Vector3 Movement::targetOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetOffset_15;
	// UnityEngine.Quaternion Movement::targetRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___targetRotation_16;

public:
	inline static int32_t get_offset_of_rb_4() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___rb_4)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_rb_4() const { return ___rb_4; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_rb_4() { return &___rb_4; }
	inline void set_rb_4(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___rb_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_4), (void*)value);
	}

	inline static int32_t get_offset_of_turnSpeed_5() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___turnSpeed_5)); }
	inline float get_turnSpeed_5() const { return ___turnSpeed_5; }
	inline float* get_address_of_turnSpeed_5() { return &___turnSpeed_5; }
	inline void set_turnSpeed_5(float value)
	{
		___turnSpeed_5 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_comandos_7() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___comandos_7)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_comandos_7() const { return ___comandos_7; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_comandos_7() { return &___comandos_7; }
	inline void set_comandos_7(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___comandos_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comandos_7), (void*)value);
	}

	inline static int32_t get_offset_of_startPos_8() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___startPos_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_startPos_8() const { return ___startPos_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_startPos_8() { return &___startPos_8; }
	inline void set_startPos_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___startPos_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPos_8), (void*)value);
	}

	inline static int32_t get_offset_of_commandText_9() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___commandText_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_commandText_9() const { return ___commandText_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_commandText_9() { return &___commandText_9; }
	inline void set_commandText_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___commandText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___commandText_9), (void*)value);
	}

	inline static int32_t get_offset_of_startMove_10() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___startMove_10)); }
	inline bool get_startMove_10() const { return ___startMove_10; }
	inline bool* get_address_of_startMove_10() { return &___startMove_10; }
	inline void set_startMove_10(bool value)
	{
		___startMove_10 = value;
	}

	inline static int32_t get_offset_of_stopped_11() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___stopped_11)); }
	inline bool get_stopped_11() const { return ___stopped_11; }
	inline bool* get_address_of_stopped_11() { return &___stopped_11; }
	inline void set_stopped_11(bool value)
	{
		___stopped_11 = value;
	}

	inline static int32_t get_offset_of_separation_12() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___separation_12)); }
	inline Il2CppChar get_separation_12() const { return ___separation_12; }
	inline Il2CppChar* get_address_of_separation_12() { return &___separation_12; }
	inline void set_separation_12(Il2CppChar value)
	{
		___separation_12 = value;
	}

	inline static int32_t get_offset_of_distance_13() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___distance_13)); }
	inline float get_distance_13() const { return ___distance_13; }
	inline float* get_address_of_distance_13() { return &___distance_13; }
	inline void set_distance_13(float value)
	{
		___distance_13 = value;
	}

	inline static int32_t get_offset_of_waitSeconds_14() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___waitSeconds_14)); }
	inline float get_waitSeconds_14() const { return ___waitSeconds_14; }
	inline float* get_address_of_waitSeconds_14() { return &___waitSeconds_14; }
	inline void set_waitSeconds_14(float value)
	{
		___waitSeconds_14 = value;
	}

	inline static int32_t get_offset_of_targetOffset_15() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___targetOffset_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetOffset_15() const { return ___targetOffset_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetOffset_15() { return &___targetOffset_15; }
	inline void set_targetOffset_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetOffset_15 = value;
	}

	inline static int32_t get_offset_of_targetRotation_16() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___targetRotation_16)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_targetRotation_16() const { return ___targetRotation_16; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_targetRotation_16() { return &___targetRotation_16; }
	inline void set_targetRotation_16(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___targetRotation_16 = value;
	}
};


// Movement2
struct Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// GirarRueda Movement2::RuedaDerecha
	GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * ___RuedaDerecha_4;
	// GirarRueda Movement2::RuedaIzquierda
	GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * ___RuedaIzquierda_5;
	// UnityEngine.Rigidbody Movement2::rb
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___rb_6;
	// UnityEngine.Transform Movement2::startPos
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___startPos_7;
	// UnityEngine.UI.Text Movement2::commandText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___commandText_8;
	// System.Boolean Movement2::advance
	bool ___advance_9;
	// System.Boolean Movement2::back
	bool ___back_10;
	// System.Boolean Movement2::turnLeft
	bool ___turnLeft_11;
	// System.Boolean Movement2::turnRight
	bool ___turnRight_12;
	// System.Boolean Movement2::stop
	bool ___stop_13;
	// System.Boolean Movement2::startMove
	bool ___startMove_14;
	// System.Collections.Generic.List`1<System.String> Movement2::comandos
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos_15;
	// System.Single Movement2::turnSpeed
	float ___turnSpeed_16;
	// System.Single Movement2::baseTurnSpeed
	float ___baseTurnSpeed_17;
	// System.Single Movement2::moveSpeed
	float ___moveSpeed_18;
	// System.Single Movement2::baseMoveSpeed
	float ___baseMoveSpeed_19;
	// System.Single Movement2::waitTime
	float ___waitTime_20;
	// System.Char Movement2::separation
	Il2CppChar ___separation_21;

public:
	inline static int32_t get_offset_of_RuedaDerecha_4() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___RuedaDerecha_4)); }
	inline GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * get_RuedaDerecha_4() const { return ___RuedaDerecha_4; }
	inline GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B ** get_address_of_RuedaDerecha_4() { return &___RuedaDerecha_4; }
	inline void set_RuedaDerecha_4(GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * value)
	{
		___RuedaDerecha_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RuedaDerecha_4), (void*)value);
	}

	inline static int32_t get_offset_of_RuedaIzquierda_5() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___RuedaIzquierda_5)); }
	inline GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * get_RuedaIzquierda_5() const { return ___RuedaIzquierda_5; }
	inline GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B ** get_address_of_RuedaIzquierda_5() { return &___RuedaIzquierda_5; }
	inline void set_RuedaIzquierda_5(GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * value)
	{
		___RuedaIzquierda_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RuedaIzquierda_5), (void*)value);
	}

	inline static int32_t get_offset_of_rb_6() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___rb_6)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_rb_6() const { return ___rb_6; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_rb_6() { return &___rb_6; }
	inline void set_rb_6(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___rb_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_6), (void*)value);
	}

	inline static int32_t get_offset_of_startPos_7() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___startPos_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_startPos_7() const { return ___startPos_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_startPos_7() { return &___startPos_7; }
	inline void set_startPos_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___startPos_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPos_7), (void*)value);
	}

	inline static int32_t get_offset_of_commandText_8() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___commandText_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_commandText_8() const { return ___commandText_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_commandText_8() { return &___commandText_8; }
	inline void set_commandText_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___commandText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___commandText_8), (void*)value);
	}

	inline static int32_t get_offset_of_advance_9() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___advance_9)); }
	inline bool get_advance_9() const { return ___advance_9; }
	inline bool* get_address_of_advance_9() { return &___advance_9; }
	inline void set_advance_9(bool value)
	{
		___advance_9 = value;
	}

	inline static int32_t get_offset_of_back_10() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___back_10)); }
	inline bool get_back_10() const { return ___back_10; }
	inline bool* get_address_of_back_10() { return &___back_10; }
	inline void set_back_10(bool value)
	{
		___back_10 = value;
	}

	inline static int32_t get_offset_of_turnLeft_11() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___turnLeft_11)); }
	inline bool get_turnLeft_11() const { return ___turnLeft_11; }
	inline bool* get_address_of_turnLeft_11() { return &___turnLeft_11; }
	inline void set_turnLeft_11(bool value)
	{
		___turnLeft_11 = value;
	}

	inline static int32_t get_offset_of_turnRight_12() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___turnRight_12)); }
	inline bool get_turnRight_12() const { return ___turnRight_12; }
	inline bool* get_address_of_turnRight_12() { return &___turnRight_12; }
	inline void set_turnRight_12(bool value)
	{
		___turnRight_12 = value;
	}

	inline static int32_t get_offset_of_stop_13() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___stop_13)); }
	inline bool get_stop_13() const { return ___stop_13; }
	inline bool* get_address_of_stop_13() { return &___stop_13; }
	inline void set_stop_13(bool value)
	{
		___stop_13 = value;
	}

	inline static int32_t get_offset_of_startMove_14() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___startMove_14)); }
	inline bool get_startMove_14() const { return ___startMove_14; }
	inline bool* get_address_of_startMove_14() { return &___startMove_14; }
	inline void set_startMove_14(bool value)
	{
		___startMove_14 = value;
	}

	inline static int32_t get_offset_of_comandos_15() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___comandos_15)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_comandos_15() const { return ___comandos_15; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_comandos_15() { return &___comandos_15; }
	inline void set_comandos_15(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___comandos_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comandos_15), (void*)value);
	}

	inline static int32_t get_offset_of_turnSpeed_16() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___turnSpeed_16)); }
	inline float get_turnSpeed_16() const { return ___turnSpeed_16; }
	inline float* get_address_of_turnSpeed_16() { return &___turnSpeed_16; }
	inline void set_turnSpeed_16(float value)
	{
		___turnSpeed_16 = value;
	}

	inline static int32_t get_offset_of_baseTurnSpeed_17() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___baseTurnSpeed_17)); }
	inline float get_baseTurnSpeed_17() const { return ___baseTurnSpeed_17; }
	inline float* get_address_of_baseTurnSpeed_17() { return &___baseTurnSpeed_17; }
	inline void set_baseTurnSpeed_17(float value)
	{
		___baseTurnSpeed_17 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_18() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___moveSpeed_18)); }
	inline float get_moveSpeed_18() const { return ___moveSpeed_18; }
	inline float* get_address_of_moveSpeed_18() { return &___moveSpeed_18; }
	inline void set_moveSpeed_18(float value)
	{
		___moveSpeed_18 = value;
	}

	inline static int32_t get_offset_of_baseMoveSpeed_19() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___baseMoveSpeed_19)); }
	inline float get_baseMoveSpeed_19() const { return ___baseMoveSpeed_19; }
	inline float* get_address_of_baseMoveSpeed_19() { return &___baseMoveSpeed_19; }
	inline void set_baseMoveSpeed_19(float value)
	{
		___baseMoveSpeed_19 = value;
	}

	inline static int32_t get_offset_of_waitTime_20() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___waitTime_20)); }
	inline float get_waitTime_20() const { return ___waitTime_20; }
	inline float* get_address_of_waitTime_20() { return &___waitTime_20; }
	inline void set_waitTime_20(float value)
	{
		___waitTime_20 = value;
	}

	inline static int32_t get_offset_of_separation_21() { return static_cast<int32_t>(offsetof(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103, ___separation_21)); }
	inline Il2CppChar get_separation_21() const { return ___separation_21; }
	inline Il2CppChar* get_address_of_separation_21() { return &___separation_21; }
	inline void set_separation_21(Il2CppChar value)
	{
		___separation_21 = value;
	}
};


// PlayButton
struct PlayButton_tDDD3318D639982CFBD35C9DBF9E564031E331E74  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Movement2 PlayButton::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_4;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(PlayButton_tDDD3318D639982CFBD35C9DBF9E564031E331E74, ___player_4)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_4() const { return ___player_4; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_4), (void*)value);
	}
};


// ResetButton
struct ResetButton_tB0E49990488D04A82400DBE855AE500A97FEF862  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Movement2 ResetButton::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_4;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(ResetButton_tB0E49990488D04A82400DBE855AE500A97FEF862, ___player_4)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_4() const { return ___player_4; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// BackBlock
struct BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// ArduinoGenerator BackBlock::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_6;
	// Movement2 BackBlock::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_7;

public:
	inline static int32_t get_offset_of_ag_6() { return static_cast<int32_t>(offsetof(BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB, ___ag_6)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_6() const { return ___ag_6; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_6() { return &___ag_6; }
	inline void set_ag_6(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_6), (void*)value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB, ___player_7)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_7() const { return ___player_7; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_7), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.BaseRaycaster::m_RootRaycaster
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___m_RootRaycaster_4;

public:
	inline static int32_t get_offset_of_m_RootRaycaster_4() { return static_cast<int32_t>(offsetof(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876, ___m_RootRaycaster_4)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_m_RootRaycaster_4() const { return ___m_RootRaycaster_4; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_m_RootRaycaster_4() { return &___m_RootRaycaster_4; }
	inline void set_m_RootRaycaster_4(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___m_RootRaycaster_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RootRaycaster_4), (void*)value);
	}
};


// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SystemInputModules_4)); }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SystemInputModules_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentInputModule_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_FirstSelected_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstSelected_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentSelected_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentSelected_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DummyData_13)); }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DummyData_13), (void*)value);
	}
};

struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * ___s_RaycastComparer_14;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___m_EventSystems_6)); }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystems_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastComparer_14), (void*)value);
	}
};


// ForwardBlock
struct ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// ArduinoGenerator ForwardBlock::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_6;
	// Movement2 ForwardBlock::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_7;

public:
	inline static int32_t get_offset_of_ag_6() { return static_cast<int32_t>(offsetof(ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69, ___ag_6)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_6() const { return ___ag_6; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_6() { return &___ag_6; }
	inline void set_ag_6(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_6), (void*)value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69, ___player_7)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_7() const { return ___player_7; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_7), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// RobotBlock
struct RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// ArduinoGenerator RobotBlock::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_6;
	// UnityEngine.UI.InputField RobotBlock::RightEngine
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___RightEngine_7;
	// UnityEngine.UI.InputField RobotBlock::LeftEngine
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___LeftEngine_8;
	// Movement2 RobotBlock::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_9;

public:
	inline static int32_t get_offset_of_ag_6() { return static_cast<int32_t>(offsetof(RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D, ___ag_6)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_6() const { return ___ag_6; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_6() { return &___ag_6; }
	inline void set_ag_6(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_6), (void*)value);
	}

	inline static int32_t get_offset_of_RightEngine_7() { return static_cast<int32_t>(offsetof(RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D, ___RightEngine_7)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_RightEngine_7() const { return ___RightEngine_7; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_RightEngine_7() { return &___RightEngine_7; }
	inline void set_RightEngine_7(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___RightEngine_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RightEngine_7), (void*)value);
	}

	inline static int32_t get_offset_of_LeftEngine_8() { return static_cast<int32_t>(offsetof(RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D, ___LeftEngine_8)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_LeftEngine_8() const { return ___LeftEngine_8; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_LeftEngine_8() { return &___LeftEngine_8; }
	inline void set_LeftEngine_8(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___LeftEngine_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LeftEngine_8), (void*)value);
	}

	inline static int32_t get_offset_of_player_9() { return static_cast<int32_t>(offsetof(RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D, ___player_9)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_9() const { return ___player_9; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_9() { return &___player_9; }
	inline void set_player_9(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_9), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// StopBlock
struct StopBlock_t714D002381C475273632C74B513BC93BC959CE2B  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// ArduinoGenerator StopBlock::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_6;
	// Movement2 StopBlock::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_7;

public:
	inline static int32_t get_offset_of_ag_6() { return static_cast<int32_t>(offsetof(StopBlock_t714D002381C475273632C74B513BC93BC959CE2B, ___ag_6)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_6() const { return ___ag_6; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_6() { return &___ag_6; }
	inline void set_ag_6(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_6), (void*)value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(StopBlock_t714D002381C475273632C74B513BC93BC959CE2B, ___player_7)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_7() const { return ___player_7; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_7), (void*)value);
	}
};


// TurnLeftBlock
struct TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// ArduinoGenerator TurnLeftBlock::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_6;
	// Movement2 TurnLeftBlock::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_7;

public:
	inline static int32_t get_offset_of_ag_6() { return static_cast<int32_t>(offsetof(TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51, ___ag_6)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_6() const { return ___ag_6; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_6() { return &___ag_6; }
	inline void set_ag_6(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_6), (void*)value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51, ___player_7)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_7() const { return ___player_7; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_7), (void*)value);
	}
};


// TurnRightBlock
struct TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// ArduinoGenerator TurnRightBlock::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_6;
	// Movement2 TurnRightBlock::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_7;

public:
	inline static int32_t get_offset_of_ag_6() { return static_cast<int32_t>(offsetof(TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089, ___ag_6)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_6() const { return ___ag_6; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_6() { return &___ag_6; }
	inline void set_ag_6(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_6), (void*)value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089, ___player_7)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_7() const { return ___player_7; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_7), (void*)value);
	}
};


// WaitButton
struct WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3  : public Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6
{
public:
	// UnityEngine.UI.InputField WaitButton::input
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___input_6;
	// ArduinoGenerator WaitButton::ag
	ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * ___ag_7;
	// Movement2 WaitButton::player
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * ___player_8;

public:
	inline static int32_t get_offset_of_input_6() { return static_cast<int32_t>(offsetof(WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3, ___input_6)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_input_6() const { return ___input_6; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_input_6() { return &___input_6; }
	inline void set_input_6(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___input_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_6), (void*)value);
	}

	inline static int32_t get_offset_of_ag_7() { return static_cast<int32_t>(offsetof(WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3, ___ag_7)); }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * get_ag_7() const { return ___ag_7; }
	inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 ** get_address_of_ag_7() { return &___ag_7; }
	inline void set_ag_7(ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * value)
	{
		___ag_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ag_7), (void*)value);
	}

	inline static int32_t get_offset_of_player_8() { return static_cast<int32_t>(offsetof(WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3, ___player_8)); }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * get_player_8() const { return ___player_8; }
	inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 ** get_address_of_player_8() { return &___player_8; }
	inline void set_player_8(Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * value)
	{
		___player_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_8), (void*)value);
	}
};


// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6  : public BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876
{
public:
	// System.Boolean UnityEngine.UI.GraphicRaycaster::m_IgnoreReversedGraphics
	bool ___m_IgnoreReversedGraphics_6;
	// UnityEngine.UI.GraphicRaycaster/BlockingObjects UnityEngine.UI.GraphicRaycaster::m_BlockingObjects
	int32_t ___m_BlockingObjects_7;
	// UnityEngine.LayerMask UnityEngine.UI.GraphicRaycaster::m_BlockingMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_BlockingMask_8;
	// UnityEngine.Canvas UnityEngine.UI.GraphicRaycaster::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_9;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRaycaster::m_RaycastResults
	List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA * ___m_RaycastResults_10;

public:
	inline static int32_t get_offset_of_m_IgnoreReversedGraphics_6() { return static_cast<int32_t>(offsetof(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6, ___m_IgnoreReversedGraphics_6)); }
	inline bool get_m_IgnoreReversedGraphics_6() const { return ___m_IgnoreReversedGraphics_6; }
	inline bool* get_address_of_m_IgnoreReversedGraphics_6() { return &___m_IgnoreReversedGraphics_6; }
	inline void set_m_IgnoreReversedGraphics_6(bool value)
	{
		___m_IgnoreReversedGraphics_6 = value;
	}

	inline static int32_t get_offset_of_m_BlockingObjects_7() { return static_cast<int32_t>(offsetof(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6, ___m_BlockingObjects_7)); }
	inline int32_t get_m_BlockingObjects_7() const { return ___m_BlockingObjects_7; }
	inline int32_t* get_address_of_m_BlockingObjects_7() { return &___m_BlockingObjects_7; }
	inline void set_m_BlockingObjects_7(int32_t value)
	{
		___m_BlockingObjects_7 = value;
	}

	inline static int32_t get_offset_of_m_BlockingMask_8() { return static_cast<int32_t>(offsetof(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6, ___m_BlockingMask_8)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_BlockingMask_8() const { return ___m_BlockingMask_8; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_BlockingMask_8() { return &___m_BlockingMask_8; }
	inline void set_m_BlockingMask_8(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_BlockingMask_8 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6, ___m_Canvas_9)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_RaycastResults_10() { return static_cast<int32_t>(offsetof(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6, ___m_RaycastResults_10)); }
	inline List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA * get_m_RaycastResults_10() const { return ___m_RaycastResults_10; }
	inline List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA ** get_address_of_m_RaycastResults_10() { return &___m_RaycastResults_10; }
	inline void set_m_RaycastResults_10(List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA * value)
	{
		___m_RaycastResults_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastResults_10), (void*)value);
	}
};

struct GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRaycaster::s_SortedGraphics
	List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA * ___s_SortedGraphics_11;

public:
	inline static int32_t get_offset_of_s_SortedGraphics_11() { return static_cast<int32_t>(offsetof(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_StaticFields, ___s_SortedGraphics_11)); }
	inline List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA * get_s_SortedGraphics_11() const { return ___s_SortedGraphics_11; }
	inline List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA ** get_address_of_s_SortedGraphics_11() { return &___s_SortedGraphics_11; }
	inline void set_s_SortedGraphics_11(List_1_t2B519B7CD269238D4C71A96E4B005CF88488FACA * value)
	{
		___s_SortedGraphics_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SortedGraphics_11), (void*)value);
	}
};


// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E * ___m_Keyboard_20;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_TextComponent_22;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_Placeholder_23;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_24;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_25;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_26;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_27;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_28;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_29;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_30;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_31;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 * ___m_OnEndEdit_32;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 * ___m_OnValueChanged_33;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F * ___m_OnValidateInput_34;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_CaretColor_35;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_36;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectionColor_37;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_38;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_39;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_40;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_41;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateOnSelect
	bool ___m_ShouldActivateOnSelect_42;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_43;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_44;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___caretRectTrans_45;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_CursorVerts_46;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_InputTextCache_47;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CachedInputRenderer_48;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_49;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_Mesh_50;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_51;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_52;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_53;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_54;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_57;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_BlinkCoroutine_58;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_59;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_60;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_61;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_DragCoroutine_62;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_63;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_64;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_65;
	// UnityEngine.WaitForSecondsRealtime UnityEngine.UI.InputField::m_WaitForSecondsRealtime
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ___m_WaitForSecondsRealtime_66;
	// System.Boolean UnityEngine.UI.InputField::m_TouchKeyboardAllowsInPlaceEditing
	bool ___m_TouchKeyboardAllowsInPlaceEditing_67;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E * ___m_ProcessingEvent_69;

public:
	inline static int32_t get_offset_of_m_Keyboard_20() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Keyboard_20)); }
	inline TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E * get_m_Keyboard_20() const { return ___m_Keyboard_20; }
	inline TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E ** get_address_of_m_Keyboard_20() { return &___m_Keyboard_20; }
	inline void set_m_Keyboard_20(TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E * value)
	{
		___m_Keyboard_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Keyboard_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextComponent_22() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_TextComponent_22)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_TextComponent_22() const { return ___m_TextComponent_22; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_TextComponent_22() { return &___m_TextComponent_22; }
	inline void set_m_TextComponent_22(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_TextComponent_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_Placeholder_23() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Placeholder_23)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_Placeholder_23() const { return ___m_Placeholder_23; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_Placeholder_23() { return &___m_Placeholder_23; }
	inline void set_m_Placeholder_23(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_Placeholder_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Placeholder_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContentType_24() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ContentType_24)); }
	inline int32_t get_m_ContentType_24() const { return ___m_ContentType_24; }
	inline int32_t* get_address_of_m_ContentType_24() { return &___m_ContentType_24; }
	inline void set_m_ContentType_24(int32_t value)
	{
		___m_ContentType_24 = value;
	}

	inline static int32_t get_offset_of_m_InputType_25() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_InputType_25)); }
	inline int32_t get_m_InputType_25() const { return ___m_InputType_25; }
	inline int32_t* get_address_of_m_InputType_25() { return &___m_InputType_25; }
	inline void set_m_InputType_25(int32_t value)
	{
		___m_InputType_25 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_26() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_AsteriskChar_26)); }
	inline Il2CppChar get_m_AsteriskChar_26() const { return ___m_AsteriskChar_26; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_26() { return &___m_AsteriskChar_26; }
	inline void set_m_AsteriskChar_26(Il2CppChar value)
	{
		___m_AsteriskChar_26 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_27() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_KeyboardType_27)); }
	inline int32_t get_m_KeyboardType_27() const { return ___m_KeyboardType_27; }
	inline int32_t* get_address_of_m_KeyboardType_27() { return &___m_KeyboardType_27; }
	inline void set_m_KeyboardType_27(int32_t value)
	{
		___m_KeyboardType_27 = value;
	}

	inline static int32_t get_offset_of_m_LineType_28() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_LineType_28)); }
	inline int32_t get_m_LineType_28() const { return ___m_LineType_28; }
	inline int32_t* get_address_of_m_LineType_28() { return &___m_LineType_28; }
	inline void set_m_LineType_28(int32_t value)
	{
		___m_LineType_28 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_29() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_HideMobileInput_29)); }
	inline bool get_m_HideMobileInput_29() const { return ___m_HideMobileInput_29; }
	inline bool* get_address_of_m_HideMobileInput_29() { return &___m_HideMobileInput_29; }
	inline void set_m_HideMobileInput_29(bool value)
	{
		___m_HideMobileInput_29 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_30() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CharacterValidation_30)); }
	inline int32_t get_m_CharacterValidation_30() const { return ___m_CharacterValidation_30; }
	inline int32_t* get_address_of_m_CharacterValidation_30() { return &___m_CharacterValidation_30; }
	inline void set_m_CharacterValidation_30(int32_t value)
	{
		___m_CharacterValidation_30 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_31() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CharacterLimit_31)); }
	inline int32_t get_m_CharacterLimit_31() const { return ___m_CharacterLimit_31; }
	inline int32_t* get_address_of_m_CharacterLimit_31() { return &___m_CharacterLimit_31; }
	inline void set_m_CharacterLimit_31(int32_t value)
	{
		___m_CharacterLimit_31 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_32() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OnEndEdit_32)); }
	inline SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 * get_m_OnEndEdit_32() const { return ___m_OnEndEdit_32; }
	inline SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 ** get_address_of_m_OnEndEdit_32() { return &___m_OnEndEdit_32; }
	inline void set_m_OnEndEdit_32(SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 * value)
	{
		___m_OnEndEdit_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnEndEdit_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_33() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OnValueChanged_33)); }
	inline OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 * get_m_OnValueChanged_33() const { return ___m_OnValueChanged_33; }
	inline OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 ** get_address_of_m_OnValueChanged_33() { return &___m_OnValueChanged_33; }
	inline void set_m_OnValueChanged_33(OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 * value)
	{
		___m_OnValueChanged_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_34() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OnValidateInput_34)); }
	inline OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F * get_m_OnValidateInput_34() const { return ___m_OnValidateInput_34; }
	inline OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F ** get_address_of_m_OnValidateInput_34() { return &___m_OnValidateInput_34; }
	inline void set_m_OnValidateInput_34(OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F * value)
	{
		___m_OnValidateInput_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValidateInput_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaretColor_35() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretColor_35)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_CaretColor_35() const { return ___m_CaretColor_35; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_CaretColor_35() { return &___m_CaretColor_35; }
	inline void set_m_CaretColor_35(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_CaretColor_35 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_36() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CustomCaretColor_36)); }
	inline bool get_m_CustomCaretColor_36() const { return ___m_CustomCaretColor_36; }
	inline bool* get_address_of_m_CustomCaretColor_36() { return &___m_CustomCaretColor_36; }
	inline void set_m_CustomCaretColor_36(bool value)
	{
		___m_CustomCaretColor_36 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_37() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_SelectionColor_37)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectionColor_37() const { return ___m_SelectionColor_37; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectionColor_37() { return &___m_SelectionColor_37; }
	inline void set_m_SelectionColor_37(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectionColor_37 = value;
	}

	inline static int32_t get_offset_of_m_Text_38() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Text_38)); }
	inline String_t* get_m_Text_38() const { return ___m_Text_38; }
	inline String_t** get_address_of_m_Text_38() { return &___m_Text_38; }
	inline void set_m_Text_38(String_t* value)
	{
		___m_Text_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_39() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretBlinkRate_39)); }
	inline float get_m_CaretBlinkRate_39() const { return ___m_CaretBlinkRate_39; }
	inline float* get_address_of_m_CaretBlinkRate_39() { return &___m_CaretBlinkRate_39; }
	inline void set_m_CaretBlinkRate_39(float value)
	{
		___m_CaretBlinkRate_39 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_40() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretWidth_40)); }
	inline int32_t get_m_CaretWidth_40() const { return ___m_CaretWidth_40; }
	inline int32_t* get_address_of_m_CaretWidth_40() { return &___m_CaretWidth_40; }
	inline void set_m_CaretWidth_40(int32_t value)
	{
		___m_CaretWidth_40 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_41() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ReadOnly_41)); }
	inline bool get_m_ReadOnly_41() const { return ___m_ReadOnly_41; }
	inline bool* get_address_of_m_ReadOnly_41() { return &___m_ReadOnly_41; }
	inline void set_m_ReadOnly_41(bool value)
	{
		___m_ReadOnly_41 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateOnSelect_42() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ShouldActivateOnSelect_42)); }
	inline bool get_m_ShouldActivateOnSelect_42() const { return ___m_ShouldActivateOnSelect_42; }
	inline bool* get_address_of_m_ShouldActivateOnSelect_42() { return &___m_ShouldActivateOnSelect_42; }
	inline void set_m_ShouldActivateOnSelect_42(bool value)
	{
		___m_ShouldActivateOnSelect_42 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_43() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretPosition_43)); }
	inline int32_t get_m_CaretPosition_43() const { return ___m_CaretPosition_43; }
	inline int32_t* get_address_of_m_CaretPosition_43() { return &___m_CaretPosition_43; }
	inline void set_m_CaretPosition_43(int32_t value)
	{
		___m_CaretPosition_43 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_44() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretSelectPosition_44)); }
	inline int32_t get_m_CaretSelectPosition_44() const { return ___m_CaretSelectPosition_44; }
	inline int32_t* get_address_of_m_CaretSelectPosition_44() { return &___m_CaretSelectPosition_44; }
	inline void set_m_CaretSelectPosition_44(int32_t value)
	{
		___m_CaretSelectPosition_44 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_45() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___caretRectTrans_45)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_caretRectTrans_45() const { return ___caretRectTrans_45; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_caretRectTrans_45() { return &___caretRectTrans_45; }
	inline void set_caretRectTrans_45(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___caretRectTrans_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___caretRectTrans_45), (void*)value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_46() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CursorVerts_46)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_CursorVerts_46() const { return ___m_CursorVerts_46; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_CursorVerts_46() { return &___m_CursorVerts_46; }
	inline void set_m_CursorVerts_46(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_CursorVerts_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CursorVerts_46), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_47() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_InputTextCache_47)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_InputTextCache_47() const { return ___m_InputTextCache_47; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_InputTextCache_47() { return &___m_InputTextCache_47; }
	inline void set_m_InputTextCache_47(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_InputTextCache_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputTextCache_47), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_48() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CachedInputRenderer_48)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CachedInputRenderer_48() const { return ___m_CachedInputRenderer_48; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CachedInputRenderer_48() { return &___m_CachedInputRenderer_48; }
	inline void set_m_CachedInputRenderer_48(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CachedInputRenderer_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedInputRenderer_48), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_49() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_PreventFontCallback_49)); }
	inline bool get_m_PreventFontCallback_49() const { return ___m_PreventFontCallback_49; }
	inline bool* get_address_of_m_PreventFontCallback_49() { return &___m_PreventFontCallback_49; }
	inline void set_m_PreventFontCallback_49(bool value)
	{
		___m_PreventFontCallback_49 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_50() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Mesh_50)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_Mesh_50() const { return ___m_Mesh_50; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_Mesh_50() { return &___m_Mesh_50; }
	inline void set_m_Mesh_50(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_Mesh_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Mesh_50), (void*)value);
	}

	inline static int32_t get_offset_of_m_AllowInput_51() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_AllowInput_51)); }
	inline bool get_m_AllowInput_51() const { return ___m_AllowInput_51; }
	inline bool* get_address_of_m_AllowInput_51() { return &___m_AllowInput_51; }
	inline void set_m_AllowInput_51(bool value)
	{
		___m_AllowInput_51 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_52() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ShouldActivateNextUpdate_52)); }
	inline bool get_m_ShouldActivateNextUpdate_52() const { return ___m_ShouldActivateNextUpdate_52; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_52() { return &___m_ShouldActivateNextUpdate_52; }
	inline void set_m_ShouldActivateNextUpdate_52(bool value)
	{
		___m_ShouldActivateNextUpdate_52 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_53() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_UpdateDrag_53)); }
	inline bool get_m_UpdateDrag_53() const { return ___m_UpdateDrag_53; }
	inline bool* get_address_of_m_UpdateDrag_53() { return &___m_UpdateDrag_53; }
	inline void set_m_UpdateDrag_53(bool value)
	{
		___m_UpdateDrag_53 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_54() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DragPositionOutOfBounds_54)); }
	inline bool get_m_DragPositionOutOfBounds_54() const { return ___m_DragPositionOutOfBounds_54; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_54() { return &___m_DragPositionOutOfBounds_54; }
	inline void set_m_DragPositionOutOfBounds_54(bool value)
	{
		___m_DragPositionOutOfBounds_54 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_57() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretVisible_57)); }
	inline bool get_m_CaretVisible_57() const { return ___m_CaretVisible_57; }
	inline bool* get_address_of_m_CaretVisible_57() { return &___m_CaretVisible_57; }
	inline void set_m_CaretVisible_57(bool value)
	{
		___m_CaretVisible_57 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_58() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_BlinkCoroutine_58)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_BlinkCoroutine_58() const { return ___m_BlinkCoroutine_58; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_BlinkCoroutine_58() { return &___m_BlinkCoroutine_58; }
	inline void set_m_BlinkCoroutine_58(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_BlinkCoroutine_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BlinkCoroutine_58), (void*)value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_59() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_BlinkStartTime_59)); }
	inline float get_m_BlinkStartTime_59() const { return ___m_BlinkStartTime_59; }
	inline float* get_address_of_m_BlinkStartTime_59() { return &___m_BlinkStartTime_59; }
	inline void set_m_BlinkStartTime_59(float value)
	{
		___m_BlinkStartTime_59 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_60() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DrawStart_60)); }
	inline int32_t get_m_DrawStart_60() const { return ___m_DrawStart_60; }
	inline int32_t* get_address_of_m_DrawStart_60() { return &___m_DrawStart_60; }
	inline void set_m_DrawStart_60(int32_t value)
	{
		___m_DrawStart_60 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_61() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DrawEnd_61)); }
	inline int32_t get_m_DrawEnd_61() const { return ___m_DrawEnd_61; }
	inline int32_t* get_address_of_m_DrawEnd_61() { return &___m_DrawEnd_61; }
	inline void set_m_DrawEnd_61(int32_t value)
	{
		___m_DrawEnd_61 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_62() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DragCoroutine_62)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_DragCoroutine_62() const { return ___m_DragCoroutine_62; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_DragCoroutine_62() { return &___m_DragCoroutine_62; }
	inline void set_m_DragCoroutine_62(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_DragCoroutine_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DragCoroutine_62), (void*)value);
	}

	inline static int32_t get_offset_of_m_OriginalText_63() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OriginalText_63)); }
	inline String_t* get_m_OriginalText_63() const { return ___m_OriginalText_63; }
	inline String_t** get_address_of_m_OriginalText_63() { return &___m_OriginalText_63; }
	inline void set_m_OriginalText_63(String_t* value)
	{
		___m_OriginalText_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OriginalText_63), (void*)value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_64() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_WasCanceled_64)); }
	inline bool get_m_WasCanceled_64() const { return ___m_WasCanceled_64; }
	inline bool* get_address_of_m_WasCanceled_64() { return &___m_WasCanceled_64; }
	inline void set_m_WasCanceled_64(bool value)
	{
		___m_WasCanceled_64 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_65() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_HasDoneFocusTransition_65)); }
	inline bool get_m_HasDoneFocusTransition_65() const { return ___m_HasDoneFocusTransition_65; }
	inline bool* get_address_of_m_HasDoneFocusTransition_65() { return &___m_HasDoneFocusTransition_65; }
	inline void set_m_HasDoneFocusTransition_65(bool value)
	{
		___m_HasDoneFocusTransition_65 = value;
	}

	inline static int32_t get_offset_of_m_WaitForSecondsRealtime_66() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_WaitForSecondsRealtime_66)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get_m_WaitForSecondsRealtime_66() const { return ___m_WaitForSecondsRealtime_66; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of_m_WaitForSecondsRealtime_66() { return &___m_WaitForSecondsRealtime_66; }
	inline void set_m_WaitForSecondsRealtime_66(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		___m_WaitForSecondsRealtime_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WaitForSecondsRealtime_66), (void*)value);
	}

	inline static int32_t get_offset_of_m_TouchKeyboardAllowsInPlaceEditing_67() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_TouchKeyboardAllowsInPlaceEditing_67)); }
	inline bool get_m_TouchKeyboardAllowsInPlaceEditing_67() const { return ___m_TouchKeyboardAllowsInPlaceEditing_67; }
	inline bool* get_address_of_m_TouchKeyboardAllowsInPlaceEditing_67() { return &___m_TouchKeyboardAllowsInPlaceEditing_67; }
	inline void set_m_TouchKeyboardAllowsInPlaceEditing_67(bool value)
	{
		___m_TouchKeyboardAllowsInPlaceEditing_67 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_69() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ProcessingEvent_69)); }
	inline Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E * get_m_ProcessingEvent_69() const { return ___m_ProcessingEvent_69; }
	inline Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E ** get_address_of_m_ProcessingEvent_69() { return &___m_ProcessingEvent_69; }
	inline void set_m_ProcessingEvent_69(Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E * value)
	{
		___m_ProcessingEvent_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ProcessingEvent_69), (void*)value);
	}
};

struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___kSeparators_21;

public:
	inline static int32_t get_offset_of_kSeparators_21() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_StaticFields, ___kSeparators_21)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_kSeparators_21() const { return ___kSeparators_21; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_kSeparators_21() { return &___kSeparators_21; }
	inline void set_kSeparators_21(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___kSeparators_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kSeparators_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Scrollbar
struct Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_20;
	// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::m_Direction
	int32_t ___m_Direction_21;
	// System.Single UnityEngine.UI.Scrollbar::m_Value
	float ___m_Value_22;
	// System.Single UnityEngine.UI.Scrollbar::m_Size
	float ___m_Size_23;
	// System.Int32 UnityEngine.UI.Scrollbar::m_NumberOfSteps
	int32_t ___m_NumberOfSteps_24;
	// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::m_OnValueChanged
	ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED * ___m_OnValueChanged_25;
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_ContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_ContainerRect_26;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_27;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Scrollbar::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_28;
	// UnityEngine.Coroutine UnityEngine.UI.Scrollbar::m_PointerDownRepeat
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_PointerDownRepeat_29;
	// System.Boolean UnityEngine.UI.Scrollbar::isPointerDownAndNotDragging
	bool ___isPointerDownAndNotDragging_30;
	// System.Boolean UnityEngine.UI.Scrollbar::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_31;

public:
	inline static int32_t get_offset_of_m_HandleRect_20() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_HandleRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_20() const { return ___m_HandleRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_20() { return &___m_HandleRect_20; }
	inline void set_m_HandleRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_21() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Direction_21)); }
	inline int32_t get_m_Direction_21() const { return ___m_Direction_21; }
	inline int32_t* get_address_of_m_Direction_21() { return &___m_Direction_21; }
	inline void set_m_Direction_21(int32_t value)
	{
		___m_Direction_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_Size_23() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Size_23)); }
	inline float get_m_Size_23() const { return ___m_Size_23; }
	inline float* get_address_of_m_Size_23() { return &___m_Size_23; }
	inline void set_m_Size_23(float value)
	{
		___m_Size_23 = value;
	}

	inline static int32_t get_offset_of_m_NumberOfSteps_24() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_NumberOfSteps_24)); }
	inline int32_t get_m_NumberOfSteps_24() const { return ___m_NumberOfSteps_24; }
	inline int32_t* get_address_of_m_NumberOfSteps_24() { return &___m_NumberOfSteps_24; }
	inline void set_m_NumberOfSteps_24(int32_t value)
	{
		___m_NumberOfSteps_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_OnValueChanged_25)); }
	inline ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContainerRect_26() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_ContainerRect_26)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_ContainerRect_26() const { return ___m_ContainerRect_26; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_ContainerRect_26() { return &___m_ContainerRect_26; }
	inline void set_m_ContainerRect_26(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_ContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ContainerRect_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_27() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Offset_27)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_27() const { return ___m_Offset_27; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_27() { return &___m_Offset_27; }
	inline void set_m_Offset_27(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_27 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_28() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Tracker_28)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_28() const { return ___m_Tracker_28; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_28() { return &___m_Tracker_28; }
	inline void set_m_Tracker_28(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_28 = value;
	}

	inline static int32_t get_offset_of_m_PointerDownRepeat_29() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_PointerDownRepeat_29)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_PointerDownRepeat_29() const { return ___m_PointerDownRepeat_29; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_PointerDownRepeat_29() { return &___m_PointerDownRepeat_29; }
	inline void set_m_PointerDownRepeat_29(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_PointerDownRepeat_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerDownRepeat_29), (void*)value);
	}

	inline static int32_t get_offset_of_isPointerDownAndNotDragging_30() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___isPointerDownAndNotDragging_30)); }
	inline bool get_isPointerDownAndNotDragging_30() const { return ___isPointerDownAndNotDragging_30; }
	inline bool* get_address_of_isPointerDownAndNotDragging_30() { return &___isPointerDownAndNotDragging_30; }
	inline void set_isPointerDownAndNotDragging_30(bool value)
	{
		___isPointerDownAndNotDragging_30 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_31() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_DelayedUpdateVisuals_31)); }
	inline bool get_m_DelayedUpdateVisuals_31() const { return ___m_DelayedUpdateVisuals_31; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_31() { return &___m_DelayedUpdateVisuals_31; }
	inline void set_m_DelayedUpdateVisuals_31(bool value)
	{
		___m_DelayedUpdateVisuals_31 = value;
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  m_Items[1];

public:
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___module_1), (void*)NULL);
		#endif
	}
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___module_1), (void*)NULL);
		#endif
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3_gshared (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_gshared_inline (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_gshared_inline (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInParent_TisRuntimeObject_mADA186D1675BEA6779C469918206294354385EC3_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<System.String>::Clear()
inline void List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, String_t*, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___values0, const RuntimeMethod* method);
// System.Void ArduinoGenerator::AddToDefinitions(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___input0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54 (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7 (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIUtility_set_systemCopyBuffer_m1C5EAC38441C94C430AA13DF9942E1786CFCAC95 (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.SpriteRenderer::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  SpriteRenderer_get_size_mB0C8D3133ABDB73AA1BCC468F23DD9EE0A8C97C7 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<ArduinoGenerator>()
inline ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016 (const RuntimeMethod* method)
{
	return ((  ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// !!0 UnityEngine.Object::FindObjectOfType<Movement2>()
inline Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15 (const RuntimeMethod* method)
{
	return ((  Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// System.Void Movement2::RellenarComando(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, String_t* ___comando0, const RuntimeMethod* method);
// System.Void ArduinoGenerator::AddToLoop(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___input0, const RuntimeMethod* method);
// System.Void Block::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE (Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6 * __this, const RuntimeMethod* method);
// System.Boolean System.String::StartsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared)(___original0, method);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetSiblingIndex_mC69C3B37E6C731AA2A0B9BD787CF47AA5B8001FC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Scrollbar::set_value(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Scrollbar_set_value_mEDFFDDF8153EA01B897198648DCFB1D1EA539197 (Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * __this, float ___value0, const RuntimeMethod* method);
// System.Void ArduinoGenerator::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_Clear_mA8305D40B3DFE56CE5A93ACE8EBFE5CE57196BF8 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method);
// System.Void Movement2::VaciarLista()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_VaciarLista_m257E3481A363F42DBC27D7614E376D4DA5D8BD2D (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void ArduinoGenerator::Generate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_Generate_m7F205A8BD0DB80B6EC984B73C23A05C3CF62E700 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void ByteHandler::ClampNumber(UnityEngine.UI.InputField)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ByteHandler_ClampNumber_m3EB3885110EB83603E16F1E99E801168EB5A55E8 (ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87 * __this, InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___numberToCheck0, const RuntimeMethod* method);
// System.String UnityEngine.UI.InputField::get_text()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline (InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * __this, const RuntimeMethod* method);
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_m748B8DB1D0C9D25C3D1812D7887411C4AFC1DDC2 (String_t* ___s0, int32_t* ___result1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.InputField::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF (InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Cinemachine.CinemachineCore/AxisInputDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AxisInputDelegate__ctor_m55631A6E688731D441A5822CE96218E7E9CC5B01 (AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1 (int32_t ___button0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.GraphicRaycaster>()
inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * Component_GetComponent_TisGraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_m3C58447B8D6B8A8D9F08E2A21B8058F221AF6008 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.EventSystems.EventSystem>()
inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * Component_GetComponent_TisEventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_m1B49BC63CCD7706DA1800174F4BB49E13E82A887 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.PointerEventData::.ctor(UnityEngine.EventSystems.EventSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEventData__ctor_m3A877590C20995B4F549C6923BBE2B0901A684F2 (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___eventSystem0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.PointerEventData::set_position(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointerEventData_set_position_m65960EBCA54317C91CEFFC4893466F87FB168BBF_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
inline void List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3 (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 *, const RuntimeMethod*))List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_inline (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  (*) (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 *, int32_t, const RuntimeMethod*))List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::get_gameObject()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * RaycastResult_get_gameObject_mABA10AC828B2E6603A6C088A4CCD40932F6AF5FF_inline (RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * __this, const RuntimeMethod* method);
// System.String UnityEngine.GameObject::get_tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
inline int32_t List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_inline (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 *, const RuntimeMethod*))List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_gshared_inline)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<Command>()
inline Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * Component_GetComponent_TisCommand_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5_m5A4F8E3AA330B41031E267A386DB12A4CBD69C99 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanvasGroup_set_blocksRaycasts_m322FC5A1B70A23524463A84CC707BF50FD284B3A (CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (String_t* ___tag0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CanvasGroup>()
inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * GameObject_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_m06FB4232ED756E269BDAE846E32BC8B0EA40B83A (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void DragOrderContainer::set_objectBeingDragged(UnityEngine.GameObject)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A_inline (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInParent<DragOrderContainer>()
inline DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * Component_GetComponentInParent_TisDragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C_mC5B7A6DD9A82DE4AAD11661768D2B40DC1BBC0F1 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInParent_TisRuntimeObject_mADA186D1675BEA6779C469918206294354385EC3_gshared)(__this, method);
}
// UnityEngine.GameObject DragOrderContainer::get_objectBeingDragged()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D_inline (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::GetSiblingIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_GetSiblingIndex_mEF9DF6406920F8EBCFBC87C6D0630FE3E9E3C1EE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void BlockList::AddBlockToList(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_AddBlockToList_mB508FF730A896E8C6C390B3A3039D40DBC53DBE0 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, String_t* ___blockName0, const RuntimeMethod* method);
// System.Void FloatHandler::ClampNumber(UnityEngine.UI.InputField)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatHandler_ClampNumber_m506D253C7AF6A1AA355009A2EA2602593278A24D (FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC * __this, InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___numberToCheck0, const RuntimeMethod* method);
// System.Boolean System.Single::TryParse(System.String,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_TryParse_mC2E0086EAB164A81380FD03BDE671C574F52E373 (String_t* ___s0, float* ___result1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method);
// System.Void MenuAppearScript::SetImage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuAppearScript_SetImage_m3D6B513083873E48304D0CBB9CE2B5ADCB40F0E1 (MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8 * __this, bool ___state0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Collections.IEnumerator Movement::StartCoroutine(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Movement_StartCoroutine_mDEB32D8D5168F232623CE5BCB97492E6DD77EDC4 (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void Movement/<Wait>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__18__ctor_mBFF89EB8921F8EB82D33BA0EF8228082DA395F2F (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void Movement/<StartCoroutine>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19__ctor_m9BCECFBF92C11A847A748005D398FAA7B8922A8F (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void GirarRueda::ChangeSpeed(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933 (GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * __this, int32_t ___input0, const RuntimeMethod* method);
// System.Void Movement2::CheckMovementBools()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_CheckMovementBools_m4A5E33DE213E35A668DF4F1E7BFCDC1D161966DC (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void Movement2/<StartCoroutine>d__21::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21__ctor_m81430BEC68689F4A9093E91D3E7685D5832AF243 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.IEnumerator Movement2::StartCoroutine(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Movement2_StartCoroutine_m833568432212E05FD60EB6B743476EB3450A3CA3 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void Movement2::StartMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_StartMove_m289D60BE07D916B24EA3E20488556F4DE1A1E9B1 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method);
// System.Void Movement2::ResetPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_ResetPlayer_mC5AA3620F9717631A06588A233B369D3E54B0F1F (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method);
// System.Single System.Single::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F (String_t* ___s0, const RuntimeMethod* method);
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010 (float* __this, const RuntimeMethod* method);
// System.String System.Char::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8 (Il2CppChar* __this, const RuntimeMethod* method);
// System.Void ArduinoGenerator::AddFunctionDefinition(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___type0, String_t* ___name1, String_t* ___content2, const RuntimeMethod* method);
// System.Void ArduinoGenerator::AddToSetup(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___input0, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Int32 WaitButton::CheckValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WaitButton_CheckValue_m7CCA307D27B82090E14654D4564924126C2EEC3A (WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Movement/<StartCoroutine>d__19::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19_U3CU3Em__Finally1_mB92A9D87A4F1B0276B8064AEB27FA56377C7DD20 (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___separator0, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C (String_t* ___s0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * __this, const RuntimeMethod* method);
// System.Void Movement/<StartCoroutine>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19_System_IDisposable_Dispose_mB37ED72A8D68B4562B4D700B39D943ADDD7F9249 (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void Movement2/<StartCoroutine>d__21::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21_U3CU3Em__Finally1_m9E93B526D9B40F122720C047C1BF8980B27ECDC2 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method);
// System.Void GirarRueda::ChangeBaseSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GirarRueda_ChangeBaseSpeed_m0C947092E43F4688E114BE3F948648AB46ABCEF5 (GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * __this, float ___desiredSpeed0, const RuntimeMethod* method);
// System.Void Movement2/<StartCoroutine>d__21::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21_System_IDisposable_Dispose_mA53B56E97C0611E61A5A03CD4F29F23BC70817D6 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArduinoGenerator::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_Clear_mA8305D40B3DFE56CE5A93ACE8EBFE5CE57196BF8 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// definitions.Clear();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_definitions_6();
		List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640(L_0, /*hidden argument*/List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var);
		// setup.Clear();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_1 = __this->get_setup_7();
		List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640(L_1, /*hidden argument*/List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var);
		// loop.Clear();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_2 = __this->get_loop_8();
		List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640(L_2, /*hidden argument*/List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::AddToDefinitions(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// definitions.Add(input);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_definitions_6();
		String_t* L_1 = ___input0;
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_1, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::AddToSetup(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// setup.Add(input);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_setup_7();
		String_t* L_1 = ___input0;
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_1, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::AddToLoop(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// loop.Add(input);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_loop_8();
		String_t* L_1 = ___input0;
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_1, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::AddFunctionDefinition(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, String_t* ___type0, String_t* ___name1, String_t* ___content2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0491ADD30E5917C8C04A75C1C1EB6BF687A9409A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1324C175BB20C5329A08CC59B116FA7352BD5345);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string result = "\n"+type + " " + name + "() {\n" + content + "\n}";
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		ArrayElementTypeCheck (L_1, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		String_t* L_3 = ___type0;
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_2;
		ArrayElementTypeCheck (L_4, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_4;
		String_t* L_6 = ___name1;
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_5;
		ArrayElementTypeCheck (L_7, _stringLiteral1324C175BB20C5329A08CC59B116FA7352BD5345);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1324C175BB20C5329A08CC59B116FA7352BD5345);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		String_t* L_9 = ___content2;
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (String_t*)L_9);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_8;
		ArrayElementTypeCheck (L_10, _stringLiteral0491ADD30E5917C8C04A75C1C1EB6BF687A9409A);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral0491ADD30E5917C8C04A75C1C1EB6BF687A9409A);
		String_t* L_11;
		L_11 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		// AddToDefinitions(result);
		String_t* L_12 = V_0;
		ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C(__this, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::Generate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_Generate_m7F205A8BD0DB80B6EC984B73C23A05C3CF62E700 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral08DAB0E69E17233132E2852A52D5E9B216030586);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral658ED20B6F1F35A1621E878CD7BC02F91BEF5CAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC2ABD53443E87B1D4332B55DE89F3F4C04D71253);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 3> __leave_targets;
	{
		// code.text = mensajeAutomatizado + "\n\n";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_code_4();
		String_t* L_1 = __this->get_mensajeAutomatizado_5();
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_1, _stringLiteralC2ABD53443E87B1D4332B55DE89F3F4C04D71253, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		// foreach(var item in definitions)
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3 = __this->get_definitions_6();
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_4;
		L_4 = List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF(L_3, /*hidden argument*/List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		V_0 = L_4;
	}

IL_0027:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0029:
		{
			// foreach(var item in definitions)
			String_t* L_5;
			L_5 = Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
			V_1 = L_5;
			// code.text = code.text + item + "\n";
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_code_4();
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_7 = __this->get_code_4();
			String_t* L_8;
			L_8 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_7);
			String_t* L_9 = V_1;
			String_t* L_10;
			L_10 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_8, L_9, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
			VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_10);
		}

IL_0052:
		{
			// foreach(var item in definitions)
			bool L_11;
			L_11 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0029;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x6B, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(93)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
	}

IL_006b:
	{
		// code.text = code.text + "\nsetup()\n{\n";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_code_4();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_13 = __this->get_code_4();
		String_t* L_14;
		L_14 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_13);
		String_t* L_15;
		L_15 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_14, _stringLiteral08DAB0E69E17233132E2852A52D5E9B216030586, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_15);
		// foreach (var item in setup)
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_16 = __this->get_setup_7();
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_17;
		L_17 = List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF(L_16, /*hidden argument*/List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		V_0 = L_17;
	}

IL_0097:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c2;
		}

IL_0099:
		{
			// foreach (var item in setup)
			String_t* L_18;
			L_18 = Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
			V_2 = L_18;
			// code.text = code.text + item + "\n";
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_19 = __this->get_code_4();
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_20 = __this->get_code_4();
			String_t* L_21;
			L_21 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_20);
			String_t* L_22 = V_2;
			String_t* L_23;
			L_23 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_21, L_22, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
			VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_23);
		}

IL_00c2:
		{
			// foreach (var item in setup)
			bool L_24;
			L_24 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_24)
			{
				goto IL_0099;
			}
		}

IL_00cb:
		{
			IL2CPP_LEAVE(0xDB, FINALLY_00cd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00cd;
	}

FINALLY_00cd:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(205)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(205)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xDB, IL_00db)
	}

IL_00db:
	{
		// code.text = code.text + "}\n\nloop()\n{\n";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_25 = __this->get_code_4();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_26 = __this->get_code_4();
		String_t* L_27;
		L_27 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_26);
		String_t* L_28;
		L_28 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_27, _stringLiteral658ED20B6F1F35A1621E878CD7BC02F91BEF5CAD, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_28);
		// foreach (var item in loop)
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_29 = __this->get_loop_8();
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_30;
		L_30 = List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF(L_29, /*hidden argument*/List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		V_0 = L_30;
	}

IL_0107:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0132;
		}

IL_0109:
		{
			// foreach (var item in loop)
			String_t* L_31;
			L_31 = Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
			V_3 = L_31;
			// code.text = code.text + item + "\n";
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_32 = __this->get_code_4();
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_33 = __this->get_code_4();
			String_t* L_34;
			L_34 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_33);
			String_t* L_35 = V_3;
			String_t* L_36;
			L_36 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_34, L_35, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
			VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_32, L_36);
		}

IL_0132:
		{
			// foreach (var item in loop)
			bool L_37;
			L_37 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_37)
			{
				goto IL_0109;
			}
		}

IL_013b:
		{
			IL2CPP_LEAVE(0x14B, FINALLY_013d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_013d;
	}

FINALLY_013d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_0), /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(317)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(317)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x14B, IL_014b)
	}

IL_014b:
	{
		// code.text = code.text + "}";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_38 = __this->get_code_4();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_39 = __this->get_code_4();
		String_t* L_40;
		L_40 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_39);
		String_t* L_41;
		L_41 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_40, _stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, L_41);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::CopyToClipboard()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_CopyToClipboard_m50D172EC16AACD0C38731DEB6F6EB782D76A7B18 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method)
{
	{
		// GUIUtility.systemCopyBuffer = code.text;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_code_4();
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		GUIUtility_set_systemCopyBuffer_m1C5EAC38441C94C430AA13DF9942E1786CFCAC95(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ArduinoGenerator::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_Start_m45F7F52A2A786D313401E6AA1389A12A35F875E7 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ArduinoGenerator::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator_Update_m6B827819FBADF4829BA2785D55C0BA6150D5D5AA (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ArduinoGenerator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArduinoGenerator__ctor_mA56A1E42C71CDE11F860A38F59B870B35CCEE739 (ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD46A48F2F0F3D1880D5DCA203545EC790DDF514E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string mensajeAutomatizado = "/* Beep boop mensaje creado por un robot.*/";
		__this->set_mensajeAutomatizado_5(_stringLiteralD46A48F2F0F3D1880D5DCA203545EC790DDF514E);
		// public List<string> definitions = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_0, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_definitions_6(L_0);
		// public List<string> setup = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_1 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_1, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_setup_7(L_1);
		// public List<string> loop = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_2 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_2, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_loop_8(L_2);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AssociateImage::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssociateImage_Awake_m3ED6C44BC84608F1586598CBB9DF89CF656845D0 (AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 scale = new Vector3((image.size.x / image.size.y) * scaleFactorX, scaleFactorY, scaleFactorZ);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_image_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = SpriteRenderer_get_size_mB0C8D3133ABDB73AA1BCC468F23DD9EE0A8C97C7(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_0();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_image_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = SpriteRenderer_get_size_mB0C8D3133ABDB73AA1BCC468F23DD9EE0A8C97C7(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_y_1();
		float L_6 = __this->get_scaleFactorX_4();
		float L_7 = __this->get_scaleFactorY_5();
		float L_8 = __this->get_scaleFactorZ_6();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), ((float)il2cpp_codegen_multiply((float)((float)((float)L_2/(float)L_5)), (float)L_6)), L_7, L_8, /*hidden argument*/NULL);
		// this.transform.localScale = scale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_9, L_10, /*hidden argument*/NULL);
		// var x = display.GetComponent<MeshRenderer>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_display_9();
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_12;
		L_12 = GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B(L_11, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		// x.material = mat;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_13 = __this->get_mat_8();
		Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA(L_12, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AssociateImage::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssociateImage_Start_m2E1DBD6AE26C361EC3033D2C20AF2723ED85581A (AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AssociateImage::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssociateImage_Update_mC476AF0982334873C6D21C24C74EE4940EA13FD5 (AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AssociateImage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssociateImage__ctor_mFB4EA56F1EB4DF5D9AC224D272C5E8BD31BAA9EC (AssociateImage_t7600A9DDD6DA9F69D34B114D74AF4B1C51D038B8 * __this, const RuntimeMethod* method)
{
	{
		// public float scaleFactorX = 2.5f;
		__this->set_scaleFactorX_4((2.5f));
		// public float scaleFactorY = 2.5f;
		__this->set_scaleFactorY_5((2.5f));
		// public float scaleFactorZ = 2.5f;
		__this->set_scaleFactorZ_6((2.5f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BackBlock::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackBlock_Awake_m101C94C1F4A30D7F65131D23D74B42DF039D86EE (BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_6(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_7(L_1);
		// }
		return;
	}
}
// System.Void BackBlock::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackBlock_Command_m6875CC0E3342EC4896D6A944BD447016CD91E583 (BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral381617D1A1C0C848CBE085A3C3BF523A03E9659F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player.RellenarComando("back");
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_7();
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_0, _stringLiteral381617D1A1C0C848CBE085A3C3BF523A03E9659F, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BackBlock::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackBlock_BlockFunction_m7C31BEFADECAF08F0454157C12113EC6D6AA6DC8 (BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4124177097D1ACC17647DF0282FFDEAA6F8DE440);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag.AddToLoop("Retroceder();");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_6();
		ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8(L_0, _stringLiteral4124177097D1ACC17647DF0282FFDEAA6F8DE440, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void BackBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackBlock__ctor_mEA9FBEA1AA24CAC453F990E61F8B2A04226F6A12 (BackBlock_tB4C6BF1E803328371E4415E7F034DE1AF0404BFB * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Block::Conectar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Block_Conectar_mE109D68AA825CB421D4A2D85C9BA7656151091F7 (Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Block::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Block_Command_m7A46E3DE017076D867D95EB0A6CDCB974AB4894B (Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6 * __this, const RuntimeMethod* method)
{
	{
		// return;
		return;
	}
}
// System.Void Block::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Block_BlockFunction_m4829E0DFD55C256E3ACAB2E96B6BD32E6FFCB671 (Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6 * __this, const RuntimeMethod* method)
{
	{
		// return;
		return;
	}
}
// System.Void Block::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE (Block_tD1B94B0BEA1476ED81C42F0F6E53EFBF129FF3A6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BlockList::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_Awake_m073AD1D615879CC826FD8E0EA8199259BB17A6BC (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_11(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_12(L_1);
		// }
		return;
	}
}
// System.Void BlockList::AddBlockToList(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_AddBlockToList_mB508FF730A896E8C6C390B3A3039D40DBC53DBE0 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, String_t* ___blockName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2E2F1EA0D344BCAB8D4F6CFC44175F4F7BE7426A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62A1A6B5A17F0651671C4C3932F925E8905AC766);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6BD8D23564278A5521E9C41A48CE35C8D50840DB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F62D2472891FC0BE16BC43D30C6799DB2DFFCF6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9F31E349F47EAB64F2C4E6FED306EED783D2FFBD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAC2071C756188E8EA70784BD8950905FBE44E605);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAEEA6DA105A1A91802398EF794607D9EA3592322);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1CAC19850E8FBB5C3B416904FC8A2FFE585359E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(blockName.StartsWith("robotBlock"))
		String_t* L_0 = ___blockName0;
		bool L_1;
		L_1 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_0, _stringLiteral7F62D2472891FC0BE16BC43D30C6799DB2DFFCF6, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00dc;
		}
	}
	{
		// if (blockList.transform.childCount == 0)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_2, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_006f;
		}
	}
	{
		// var child = Instantiate(robotBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_robotBlock_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_5, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = L_6;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_7, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_9, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_8, L_10, /*hidden argument*/NULL);
		// child.transform.SetSiblingIndex(0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = L_7;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_11, /*hidden argument*/NULL);
		Transform_SetSiblingIndex_mC69C3B37E6C731AA2A0B9BD787CF47AA5B8001FC(L_12, 0, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_13;
		L_13 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_11, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_14), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_13, L_14, /*hidden argument*/NULL);
		// }
		goto IL_00dc;
	}

IL_006f:
	{
		// if (blockList.transform.GetChild(0).name != "RobotElement(Clone)")
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_16, 0, /*hidden argument*/NULL);
		String_t* L_18;
		L_18 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_17, /*hidden argument*/NULL);
		bool L_19;
		L_19 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_18, _stringLiteral2E2F1EA0D344BCAB8D4F6CFC44175F4F7BE7426A, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00dc;
		}
	}
	{
		// var child = Instantiate(robotBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20 = __this->get_robotBlock_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21;
		L_21 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_20, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = L_21;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
		L_23 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_22, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_24, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_23, L_25, /*hidden argument*/NULL);
		// child.transform.SetSiblingIndex(0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26 = L_22;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_26, /*hidden argument*/NULL);
		Transform_SetSiblingIndex_mC69C3B37E6C731AA2A0B9BD787CF47AA5B8001FC(L_27, 0, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_28;
		L_28 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_26, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		memset((&L_29), 0, sizeof(L_29));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_29), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		// if (blockName == "forwardBlock")
		String_t* L_30 = ___blockName0;
		bool L_31;
		L_31 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_30, _stringLiteralB1CAC19850E8FBB5C3B416904FC8A2FFE585359E, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0128;
		}
	}
	{
		// var child = Instantiate(forwardBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_32 = __this->get_forwardBlock_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33;
		L_33 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_32, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_34 = L_33;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_35;
		L_35 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_34, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_36 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_37;
		L_37 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_36, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_35, L_37, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_38;
		L_38 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_34, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		memset((&L_39), 0, sizeof(L_39));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_39), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_38, L_39, /*hidden argument*/NULL);
	}

IL_0128:
	{
		// if (blockName == "backBlock")
		String_t* L_40 = ___blockName0;
		bool L_41;
		L_41 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_40, _stringLiteral9F31E349F47EAB64F2C4E6FED306EED783D2FFBD, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0174;
		}
	}
	{
		// var child = Instantiate(backBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_42 = __this->get_backBlock_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_43;
		L_43 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_42, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44 = L_43;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_45;
		L_45 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_44, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_46 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_47;
		L_47 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_46, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_45, L_47, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_48;
		L_48 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_44, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		memset((&L_49), 0, sizeof(L_49));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_49), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_48, L_49, /*hidden argument*/NULL);
	}

IL_0174:
	{
		// if (blockName == "turnRightBlock")
		String_t* L_50 = ___blockName0;
		bool L_51;
		L_51 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_50, _stringLiteralAEEA6DA105A1A91802398EF794607D9EA3592322, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01c0;
		}
	}
	{
		// var child = Instantiate(turnRightBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52 = __this->get_turnRightBlock_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_53;
		L_53 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_52, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_54 = L_53;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_55;
		L_55 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_54, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_56 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_57;
		L_57 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_56, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_55, L_57, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_58;
		L_58 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_54, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59;
		memset((&L_59), 0, sizeof(L_59));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_59), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_58, L_59, /*hidden argument*/NULL);
	}

IL_01c0:
	{
		// if (blockName == "turnLeftBlock")
		String_t* L_60 = ___blockName0;
		bool L_61;
		L_61 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_60, _stringLiteral62A1A6B5A17F0651671C4C3932F925E8905AC766, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_020c;
		}
	}
	{
		// var child = Instantiate(turnLeftBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_62 = __this->get_turnLeftBlock_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_63;
		L_63 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_62, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_64 = L_63;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_65;
		L_65 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_64, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_66 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_67;
		L_67 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_66, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_65, L_67, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_68;
		L_68 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_64, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_69;
		memset((&L_69), 0, sizeof(L_69));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_69), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_68, L_69, /*hidden argument*/NULL);
	}

IL_020c:
	{
		// if (blockName == "waitBlock")
		String_t* L_70 = ___blockName0;
		bool L_71;
		L_71 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_70, _stringLiteral6BD8D23564278A5521E9C41A48CE35C8D50840DB, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0258;
		}
	}
	{
		// var child = Instantiate(waitBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_72 = __this->get_waitBlock_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_73;
		L_73 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_72, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_74 = L_73;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_75;
		L_75 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_74, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_76 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_77;
		L_77 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_76, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_75, L_77, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_78;
		L_78 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_74, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_79;
		memset((&L_79), 0, sizeof(L_79));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_79), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_78, L_79, /*hidden argument*/NULL);
	}

IL_0258:
	{
		// if (blockName == "stopBlock")
		String_t* L_80 = ___blockName0;
		bool L_81;
		L_81 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_80, _stringLiteralAC2071C756188E8EA70784BD8950905FBE44E605, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_02a4;
		}
	}
	{
		// var child = Instantiate(stopBlock);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_82 = __this->get_stopBlock_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_83;
		L_83 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_82, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// child.transform.SetParent(blockList.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_84 = L_83;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_85;
		L_85 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_84, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_86 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_87;
		L_87 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_86, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_85, L_87, /*hidden argument*/NULL);
		// child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_88;
		L_88 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_84, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_89;
		memset((&L_89), 0, sizeof(L_89));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_89), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_88, L_89, /*hidden argument*/NULL);
	}

IL_02a4:
	{
		// sb.value = 1;
		Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * L_90 = __this->get_sb_14();
		Scrollbar_set_value_mEDFFDDF8153EA01B897198648DCFB1D1EA539197(L_90, (1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BlockList::Compile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_Compile_m948C9BB29590F1EB9E2DA7A0945C08A418295382 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// ag.Clear();
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_11();
		ArduinoGenerator_Clear_mA8305D40B3DFE56CE5A93ACE8EBFE5CE57196BF8(L_0, /*hidden argument*/NULL);
		// player.VaciarLista();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1 = __this->get_player_12();
		Movement2_VaciarLista_m257E3481A363F42DBC27D7614E376D4DA5D8BD2D(L_1, /*hidden argument*/NULL);
		// foreach(Transform child in blockList.transform)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_2, /*hidden argument*/NULL);
		RuntimeObject* L_4;
		L_4 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0027:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0048;
		}

IL_0029:
		{
			// foreach(Transform child in blockList.transform)
			RuntimeObject* L_5 = V_0;
			RuntimeObject * L_6;
			L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_5);
			// child.gameObject.GetComponent<Button>().onClick.Invoke();
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
			L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_8;
			L_8 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_7, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
			ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_9;
			L_9 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_8, /*hidden argument*/NULL);
			UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_9, /*hidden argument*/NULL);
		}

IL_0048:
		{
			// foreach(Transform child in blockList.transform)
			RuntimeObject* L_10 = V_0;
			bool L_11;
			L_11 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0029;
			}
		}

IL_0050:
		{
			IL2CPP_LEAVE(0x63, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_12 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_12, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_13 = V_1;
			if (!L_13)
			{
				goto IL_0062;
			}
		}

IL_005c:
		{
			RuntimeObject* L_14 = V_1;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_14);
		}

IL_0062:
		{
			IL2CPP_END_FINALLY(82)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x63, IL_0063)
	}

IL_0063:
	{
		// ag.Generate();
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_15 = __this->get_ag_11();
		ArduinoGenerator_Generate_m7F205A8BD0DB80B6EC984B73C23A05C3CF62E700(L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BlockList::VaciarBloques()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_VaciarBloques_m875BA9546A72F318AFFA63108692F774986C4E31 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (Transform child in blockList.transform)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_blockList_13();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		RuntimeObject* L_2;
		L_2 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_0013:
		{
			// foreach (Transform child in blockList.transform)
			RuntimeObject* L_3 = V_0;
			RuntimeObject * L_4;
			L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_3);
			// GameObject.Destroy(child.gameObject);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
			L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_4, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		}

IL_0028:
		{
			// foreach (Transform child in blockList.transform)
			RuntimeObject* L_6 = V_0;
			bool L_7;
			L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_6);
			if (L_7)
			{
				goto IL_0013;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0032);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_8 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_8, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0042;
			}
		}

IL_003c:
		{
			RuntimeObject* L_10 = V_1;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_10);
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(50)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x43, IL_0043)
	}

IL_0043:
	{
		// ag.Clear();
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_11 = __this->get_ag_11();
		ArduinoGenerator_Clear_mA8305D40B3DFE56CE5A93ACE8EBFE5CE57196BF8(L_11, /*hidden argument*/NULL);
		// ag.Generate();
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_12 = __this->get_ag_11();
		ArduinoGenerator_Generate_m7F205A8BD0DB80B6EC984B73C23A05C3CF62E700(L_12, /*hidden argument*/NULL);
		// player.VaciarLista();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_13 = __this->get_player_12();
		Movement2_VaciarLista_m257E3481A363F42DBC27D7614E376D4DA5D8BD2D(L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BlockList::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_Start_mF0C6A5D6FF0D0313EAA67039B872A1D2DE4FBC85 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void BlockList::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList_Update_m7DE7BCDF6833F712D084DF0FA1F65966E23FA469 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void BlockList::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlockList__ctor_m45E84FCBF29E3B5A2F2C9DC852BC01E10768DFE1 (BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * __this, const RuntimeMethod* method)
{
	{
		// private char separation = ' ';
		__this->set_separation_15(((int32_t)32));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ByteHandler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ByteHandler_Start_mE9FB68D23D7F05ADD42830652AAA021CD920D919 (ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ByteHandler::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ByteHandler_Update_mA9B6B094B3B3D2D397EFD8B77932DC7CF681A80E (ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87 * __this, const RuntimeMethod* method)
{
	{
		// ClampNumber(number);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = __this->get_number_4();
		ByteHandler_ClampNumber_m3EB3885110EB83603E16F1E99E801168EB5A55E8(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ByteHandler::ClampNumber(UnityEngine.UI.InputField)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ByteHandler_ClampNumber_m3EB3885110EB83603E16F1E99E801168EB5A55E8 (ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87 * __this, InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___numberToCheck0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8D8BD8B8859DAA4E62064D44207BA69BCD02B29D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if(int.TryParse(numberToCheck.text, out n))
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = ___numberToCheck0;
		String_t* L_1;
		L_1 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = Int32_TryParse_m748B8DB1D0C9D25C3D1812D7887411C4AFC1DDC2(L_1, (int32_t*)(&V_0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		// if(n>255)
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0022;
		}
	}
	{
		// numberToCheck.text = "255";
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_4 = ___numberToCheck0;
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_4, _stringLiteral8D8BD8B8859DAA4E62064D44207BA69BCD02B29D, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// if(n<0)
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		// numberToCheck.text = "0";
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_6 = ___numberToCheck0;
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_6, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0032:
	{
		// numberToCheck.text = "0";
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_7 = ___numberToCheck0;
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_7, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// }
		return;
	}
}
// System.Void ByteHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ByteHandler__ctor_m9BA2B5C614AD76E44CB88804661FC796C9D42496 (ByteHandler_tE113450FFFFF716788C6CF9A67B1770290217F87 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraOnClick::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraOnClick_Start_m66D5DA3F9D8FE26F23558F3636E7DD24F2593C9D (CameraOnClick_t05317F351691E92E57D92AD28F0AEB457F442813 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CameraOnClick_GetAxisCustom_m6F5D6FBFD7F68E12C91A0E049542232AB786A125_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CinemachineCore.GetInputAxis = GetAxisCustom;
		AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 * L_0 = (AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677 *)il2cpp_codegen_object_new(AxisInputDelegate_tC74BFB577983EB520E974CB9EB9D0758BAD72677_il2cpp_TypeInfo_var);
		AxisInputDelegate__ctor_m55631A6E688731D441A5822CE96218E7E9CC5B01(L_0, __this, (intptr_t)((intptr_t)CameraOnClick_GetAxisCustom_m6F5D6FBFD7F68E12C91A0E049542232AB786A125_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_il2cpp_TypeInfo_var);
		((CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_StaticFields*)il2cpp_codegen_static_fields_for(CinemachineCore_tCA2075A0438C6810B7F437CF91AEBB9FE989F534_il2cpp_TypeInfo_var))->set_GetInputAxis_4(L_0);
		// }
		return;
	}
}
// System.Single CameraOnClick::GetAxisCustom(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CameraOnClick_GetAxisCustom_m6F5D6FBFD7F68E12C91A0E049542232AB786A125 (CameraOnClick_t05317F351691E92E57D92AD28F0AEB457F442813 * __this, String_t* ___axisName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (axisName == "Mouse X")
		String_t* L_0 = ___axisName0;
		bool L_1;
		L_1 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_0, _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		// if (Input.GetMouseButton(1)|| Input.GetMouseButton(0))
		bool L_2;
		L_2 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		bool L_3;
		L_3 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(0, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}

IL_001d:
	{
		// return UnityEngine.Input.GetAxis("Mouse X");
		float L_4;
		L_4 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		return L_4;
	}

IL_0028:
	{
		// return 0;
		return (0.0f);
	}

IL_002e:
	{
		// else if (axisName == "Mouse Y")
		String_t* L_5 = ___axisName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005c;
		}
	}
	{
		// if (Input.GetMouseButton(1)||Input.GetMouseButton(0))
		bool L_7;
		L_7 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(1, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004b;
		}
	}
	{
		bool L_8;
		L_8 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(0, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}

IL_004b:
	{
		// return UnityEngine.Input.GetAxis("Mouse Y");
		float L_9;
		L_9 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		return L_9;
	}

IL_0056:
	{
		// return 0;
		return (0.0f);
	}

IL_005c:
	{
		// return UnityEngine.Input.GetAxis(axisName);
		String_t* L_10 = ___axisName0;
		float L_11;
		L_11 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void CameraOnClick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraOnClick__ctor_m772BDF09B30584BD43165345B6DB83A4C225C26E (CameraOnClick_t05317F351691E92E57D92AD28F0AEB457F442813 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Command::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Command__ctor_mABFCAFBBC7B0C23A774E1438BE9D0C29440DE5B2 (Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DeleteBlock::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeleteBlock_Start_m60D1C37059958EE885F7CB0BF4677ADF62468BCE (DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_m1B49BC63CCD7706DA1800174F4BB49E13E82A887_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisGraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_m3C58447B8D6B8A8D9F08E2A21B8058F221AF6008_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Raycaster = GetComponent<GraphicRaycaster>();
		GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * L_0;
		L_0 = Component_GetComponent_TisGraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_m3C58447B8D6B8A8D9F08E2A21B8058F221AF6008(__this, /*hidden argument*/Component_GetComponent_TisGraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_m3C58447B8D6B8A8D9F08E2A21B8058F221AF6008_RuntimeMethod_var);
		__this->set_m_Raycaster_4(L_0);
		// m_EventSystem = GetComponent<EventSystem>();
		EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * L_1;
		L_1 = Component_GetComponent_TisEventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_m1B49BC63CCD7706DA1800174F4BB49E13E82A887(__this, /*hidden argument*/Component_GetComponent_TisEventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_m1B49BC63CCD7706DA1800174F4BB49E13E82A887_RuntimeMethod_var);
		__this->set_m_EventSystem_6(L_1);
		// }
		return;
	}
}
// System.Void DeleteBlock::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeleteBlock_Update_mA798156FE132D623D327B228CC8A54F27FCAE5ED (DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral18A1641B70EB907EA2E51E5C3A353C146DB257AD);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * V_0 = NULL;
	int32_t V_1 = 0;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (Input.GetKeyDown(KeyCode.Mouse1))
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)324), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0092;
		}
	}
	{
		// m_PointerEventData = new PointerEventData(m_EventSystem);
		EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * L_1 = __this->get_m_EventSystem_6();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_2 = (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 *)il2cpp_codegen_object_new(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m3A877590C20995B4F549C6923BBE2B0901A684F2(L_2, L_1, /*hidden argument*/NULL);
		__this->set_m_PointerEventData_5(L_2);
		// m_PointerEventData.position = Input.mousePosition;
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_3 = __this->get_m_PointerEventData_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E(/*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_4, /*hidden argument*/NULL);
		PointerEventData_set_position_m65960EBCA54317C91CEFFC4893466F87FB168BBF_inline(L_3, L_5, /*hidden argument*/NULL);
		// List<RaycastResult> results = new List<RaycastResult>();
		List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * L_6 = (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 *)il2cpp_codegen_object_new(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447_il2cpp_TypeInfo_var);
		List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3(L_6, /*hidden argument*/List_1__ctor_m3B54A4C20C7EA7D3350768955A3CD521D7F069D3_RuntimeMethod_var);
		V_0 = L_6;
		// m_Raycaster.Raycast(m_PointerEventData, results);
		GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * L_7 = __this->get_m_Raycaster_4();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_8 = __this->get_m_PointerEventData_5();
		List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * L_9 = V_0;
		VirtActionInvoker2< PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 *, List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * >::Invoke(17 /* System.Void UnityEngine.EventSystems.BaseRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>) */, L_7, L_8, L_9);
		// for (int i = 0; i < results.Count; i++)
		V_1 = 0;
		goto IL_0089;
	}

IL_0051:
	{
		// if(results[i].gameObject.tag == "BlockElement")
		List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * L_10 = V_0;
		int32_t L_11 = V_1;
		RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  L_12;
		L_12 = List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_inline(L_10, L_11, /*hidden argument*/List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_RuntimeMethod_var);
		V_2 = L_12;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = RaycastResult_get_gameObject_mABA10AC828B2E6603A6C088A4CCD40932F6AF5FF_inline((RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE *)(&V_2), /*hidden argument*/NULL);
		String_t* L_14;
		L_14 = GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33(L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_14, _stringLiteral18A1641B70EB907EA2E51E5C3A353C146DB257AD, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0085;
		}
	}
	{
		// Destroy(results[i].gameObject);
		List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * L_16 = V_0;
		int32_t L_17 = V_1;
		RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  L_18;
		L_18 = List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_inline(L_16, L_17, /*hidden argument*/List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_RuntimeMethod_var);
		V_2 = L_18;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19;
		L_19 = RaycastResult_get_gameObject_mABA10AC828B2E6603A6C088A4CCD40932F6AF5FF_inline((RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE *)(&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_19, /*hidden argument*/NULL);
	}

IL_0085:
	{
		// for (int i = 0; i < results.Count; i++)
		int32_t L_20 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0089:
	{
		// for (int i = 0; i < results.Count; i++)
		int32_t L_21 = V_1;
		List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * L_22 = V_0;
		int32_t L_23;
		L_23 = List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_inline(L_22, /*hidden argument*/List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_RuntimeMethod_var);
		if ((((int32_t)L_21) < ((int32_t)L_23)))
		{
			goto IL_0051;
		}
	}

IL_0092:
	{
		// }
		return;
	}
}
// System.Void DeleteBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeleteBlock__ctor_m6407EB0ED948B9166F5B4F85D4AF39E3068CD252 (DeleteBlock_tF77B6D86CEFB25066BA864110217458F0F9EF911 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DragBlock::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragBlock_OnBeginDrag_m588B9E3946C652556BCF294A9338CD4903EE8C0B (DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCommand_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5_m5A4F8E3AA330B41031E267A386DB12A4CBD69C99_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_m06FB4232ED756E269BDAE846E32BC8B0EA40B83A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE353794CFB3E6B3AC8E2165765828E931F71E046);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * V_0 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	{
		// command = GetComponent<Command>();
		Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * L_0;
		L_0 = Component_GetComponent_TisCommand_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5_m5A4F8E3AA330B41031E267A386DB12A4CBD69C99(__this, /*hidden argument*/Component_GetComponent_TisCommand_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5_m5A4F8E3AA330B41031E267A386DB12A4CBD69C99_RuntimeMethod_var);
		((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->set_command_5(L_0);
		// GameObject duplicate = Instantiate(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		// ItemBeingDragged = duplicate;
		((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->set_ItemBeingDragged_4(L_2);
		// RectTransform tempRT = gameObject.GetComponent<RectTransform>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4;
		L_4 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_3, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		V_0 = L_4;
		// RectTransform rt = ItemBeingDragged.GetComponent<RectTransform>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_ItemBeingDragged_4();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_6;
		L_6 = GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30(L_5, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m88DECD5A0B4E3A263DD9D40D8B518F878681ED30_RuntimeMethod_var);
		// rt.sizeDelta = new Vector2(tempRT.sizeDelta.x, tempRT.sizeDelta.y);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_7 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_x_0();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_10 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11;
		L_11 = RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_13), L_9, L_12, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0(L_6, L_13, /*hidden argument*/NULL);
		// GetComponent<CanvasGroup>().blocksRaycasts = false;
		CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * L_14;
		L_14 = Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4_RuntimeMethod_var);
		CanvasGroup_set_blocksRaycasts_m322FC5A1B70A23524463A84CC707BF50FD284B3A(L_14, (bool)0, /*hidden argument*/NULL);
		// Transform canvas = GameObject.FindGameObjectWithTag("UI Canvas").transform;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralE353794CFB3E6B3AC8E2165765828E931F71E046, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		// ItemBeingDragged.transform.SetParent(canvas, false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_ItemBeingDragged_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_17, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19 = V_1;
		Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A(L_18, L_19, (bool)0, /*hidden argument*/NULL);
		// ItemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_ItemBeingDragged_4();
		CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * L_21;
		L_21 = GameObject_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_m06FB4232ED756E269BDAE846E32BC8B0EA40B83A(L_20, /*hidden argument*/GameObject_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_m06FB4232ED756E269BDAE846E32BC8B0EA40B83A_RuntimeMethod_var);
		CanvasGroup_set_blocksRaycasts_m322FC5A1B70A23524463A84CC707BF50FD284B3A(L_21, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DragBlock::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragBlock_OnDrag_m993ED74611F526FB3A9D4E5131359D01046B7319 (DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ItemBeingDragged.transform.position = eventData.position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_ItemBeingDragged_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_2 = ___eventData0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_3, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_1, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DragBlock::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragBlock_OnEndDrag_m7B52D9D0EBD9D4EA459ACB448BA20089E8DC8BA0 (DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<CanvasGroup>().blocksRaycasts = true;
		CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * L_0;
		L_0 = Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_mFED0C73400AFB37A709212A6C61F9BF44DBB88C4_RuntimeMethod_var);
		CanvasGroup_set_blocksRaycasts_m322FC5A1B70A23524463A84CC707BF50FD284B3A(L_0, (bool)1, /*hidden argument*/NULL);
		// Destroy(ItemBeingDragged);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_ItemBeingDragged_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_1, /*hidden argument*/NULL);
		// DragBlock.ItemBeingDragged = null;
		((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->set_ItemBeingDragged_4((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
		// }
		return;
	}
}
// System.Void DragBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragBlock__ctor_m5BDF2E204B5FE3E2E0DB4C9EA9A4FE288A154DAF (DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.GameObject DragOrderContainer::get_objectBeingDragged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, const RuntimeMethod* method)
{
	{
		// public GameObject objectBeingDragged { get; set; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_U3CobjectBeingDraggedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void DragOrderContainer::set_objectBeingDragged(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___value0, const RuntimeMethod* method)
{
	{
		// public GameObject objectBeingDragged { get; set; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___value0;
		__this->set_U3CobjectBeingDraggedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void DragOrderContainer::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderContainer_Awake_mDFE27134C91A7F68B1B32866CE038567DF1BAE44 (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, const RuntimeMethod* method)
{
	{
		// objectBeingDragged = null;
		DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A_inline(__this, (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DragOrderContainer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderContainer__ctor_mDF376A9B27529D23606F21C4C2E1841A4A862322 (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DragOrderObject::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderObject_Start_m8D6CF0B090C376ED9AF2360E1E6D3C56576CCD7F (DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInParent_TisDragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C_mC5B7A6DD9A82DE4AAD11661768D2B40DC1BBC0F1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// container = GetComponentInParent<DragOrderContainer>();
		DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * L_0;
		L_0 = Component_GetComponentInParent_TisDragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C_mC5B7A6DD9A82DE4AAD11661768D2B40DC1BBC0F1(__this, /*hidden argument*/Component_GetComponentInParent_TisDragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C_mC5B7A6DD9A82DE4AAD11661768D2B40DC1BBC0F1_RuntimeMethod_var);
		__this->set_container_4(L_0);
		// }
		return;
	}
}
// System.Void DragOrderObject::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderObject_OnBeginDrag_m31FB3A82C667A42C239E222828DE8298C1D02E05 (DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	{
		// container.objectBeingDragged = this.gameObject;
		DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * L_0 = __this->get_container_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A_inline(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DragOrderObject::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderObject_OnDrag_mF78A9A8A41ECBDDF71B2F1898DAD7A4CF5A01217 (DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___data0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void DragOrderObject::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderObject_OnEndDrag_m7DACA09F08AEC316CC83D49B0C3C7F9B911F4443 (DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (container.objectBeingDragged == this.gameObject) container.objectBeingDragged = null;
		DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * L_0 = __this->get_container_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D_inline(L_0, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// if (container.objectBeingDragged == this.gameObject) container.objectBeingDragged = null;
		DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * L_4 = __this->get_container_4();
		DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A_inline(L_4, (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL, /*hidden argument*/NULL);
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void DragOrderObject::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderObject_OnPointerEnter_m3229E30DB5FA15FDC6B4052D028F47E9249CAB8A (DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	{
		// GameObject objectBeingDragged = container.objectBeingDragged;
		DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * L_0 = __this->get_container_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D_inline(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (objectBeingDragged != null && objectBeingDragged != this.gameObject)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		// objectBeingDragged.transform.SetSiblingIndex(this.transform.GetSiblingIndex());
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_7, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_10;
		L_10 = Transform_GetSiblingIndex_mEF9DF6406920F8EBCFBC87C6D0630FE3E9E3C1EE(L_9, /*hidden argument*/NULL);
		Transform_SetSiblingIndex_mC69C3B37E6C731AA2A0B9BD787CF47AA5B8001FC(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0039:
	{
		// }
		return;
	}
}
// System.Void DragOrderObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragOrderObject__ctor_m4E6F3606E426A939F95A162E9CB6EBA57596CD8F (DragOrderObject_t4553DDA9206BB773A4BFC5520CFE42B0841992DF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropBlock::OnDrop(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropBlock_OnDrop_mF4D84654CB663F7464ADB5B07B80BCC0CE52FD58 (DropBlock_tE708349ABBE8FEDF540361F3D1F2B5943A797EED * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(DragBlock.command != null)
		Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * L_0 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_command_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		// bl.AddBlockToList(DragBlock.command.command);
		BlockList_tACE623DC5A93D96990FDCAB6A03784AF8D688D2F * L_2 = __this->get_bl_4();
		Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 * L_3 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_command_5();
		String_t* L_4 = L_3->get_command_4();
		BlockList_AddBlockToList_mB508FF730A896E8C6C390B3A3039D40DBC53DBE0(L_2, L_4, /*hidden argument*/NULL);
		// Destroy(DragBlock.ItemBeingDragged);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->get_ItemBeingDragged_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		// DragBlock.ItemBeingDragged = null;
		((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->set_ItemBeingDragged_4((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
		// DragBlock.command = null;
		((DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_StaticFields*)il2cpp_codegen_static_fields_for(DragBlock_t828162DBD3E8E77BCD049FD1AC25DCF5160DC095_il2cpp_TypeInfo_var))->set_command_5((Command_t3E8BA0D1672BAAD7F08A3DB49C5DA3FB41FECCA5 *)NULL);
	}

IL_0038:
	{
		// }
		return;
	}
}
// System.Void DropBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropBlock__ctor_m669A575F06274C13EA30AB67CFFF9730765E6BF1 (DropBlock_tE708349ABBE8FEDF540361F3D1F2B5943A797EED * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FloatHandler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatHandler_Start_mC6B874C8EA776DD0DF431847416A4C5DA15BF808 (FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void FloatHandler::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatHandler_Update_m265D23A1541B810DB46393768A836DB61C539441 (FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC * __this, const RuntimeMethod* method)
{
	{
		// ClampNumber(number);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = __this->get_number_4();
		FloatHandler_ClampNumber_m506D253C7AF6A1AA355009A2EA2602593278A24D(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FloatHandler::ClampNumber(UnityEngine.UI.InputField)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatHandler_ClampNumber_m506D253C7AF6A1AA355009A2EA2602593278A24D (FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC * __this, InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___numberToCheck0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if(!float.TryParse(numberToCheck.text, out n))
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = ___numberToCheck0;
		String_t* L_1;
		L_1 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = Single_TryParse_mC2E0086EAB164A81380FD03BDE671C574F52E373(L_1, (float*)(&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		// numberToCheck.text = "0";
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_3 = ___numberToCheck0;
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_3, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void FloatHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatHandler__ctor_m1DDABB8187BD45E5E7D26AAA3DBE45F6529398C6 (FloatHandler_t522B60629AC5ED86BAC63239C00B8B2BC5BF86FC * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ForwardBlock::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ForwardBlock_Awake_m255A948840163AC5131BC31683A15B4F2840E3E6 (ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_6(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_7(L_1);
		// }
		return;
	}
}
// System.Void ForwardBlock::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ForwardBlock_Command_mFC7093C7174209A215AB778E366F34393C0CF77B (ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player.RellenarComando("forward");
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_7();
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_0, _stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ForwardBlock::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ForwardBlock_BlockFunction_mD18B4E40F955426BE574FE83A2F56E1ADCDE32B4 (ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral296C4C50A89B5CBD4B5F1A9FF0C9505E18EFAFE4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag.AddToLoop("Avanzar();");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_6();
		ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8(L_0, _stringLiteral296C4C50A89B5CBD4B5F1A9FF0C9505E18EFAFE4, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void ForwardBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ForwardBlock__ctor_m917CC80D6AE80A26528ABD98CEB16ACF6F96EE8B (ForwardBlock_t9C2EEDB9A070C39929063E26E28EE4FAB16F6E69 * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GirarRueda::ChangeSpeed(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933 (GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * __this, int32_t ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE27198FCAB85C90DAADDF61FC3A44F1791D57C37);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF279E3C88485EAF053B6870B5F6FE836949059D6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFDE5F3FA2740B216D61997915E97AF21359817A9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log(input);
		int32_t L_0 = ___input0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		// if (input == 1)
		int32_t L_3 = ___input0;
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_002c;
		}
	}
	{
		// Debug.Log("Palante");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralE27198FCAB85C90DAADDF61FC3A44F1791D57C37, /*hidden argument*/NULL);
		// speed = baseValue*1;
		float L_4 = __this->get_baseValue_5();
		__this->set_speed_4(((float)il2cpp_codegen_multiply((float)L_4, (float)(1.0f))));
		// }
		return;
	}

IL_002c:
	{
		// else if(input == -1)
		int32_t L_5 = ___input0;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_004d;
		}
	}
	{
		// Debug.Log("Patr�s");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralF279E3C88485EAF053B6870B5F6FE836949059D6, /*hidden argument*/NULL);
		// speed = baseValue * -1;
		float L_6 = __this->get_baseValue_5();
		__this->set_speed_4(((float)il2cpp_codegen_multiply((float)L_6, (float)(-1.0f))));
		// }
		return;
	}

IL_004d:
	{
		// else if(input == 0)
		int32_t L_7 = ___input0;
		if (L_7)
		{
			goto IL_0065;
		}
	}
	{
		// Debug.Log("Quieto");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralFDE5F3FA2740B216D61997915E97AF21359817A9, /*hidden argument*/NULL);
		// speed = 0;
		__this->set_speed_4((0.0f));
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void GirarRueda::ChangeBaseSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GirarRueda_ChangeBaseSpeed_m0C947092E43F4688E114BE3F948648AB46ABCEF5 (GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * __this, float ___desiredSpeed0, const RuntimeMethod* method)
{
	{
		// baseValue = desiredSpeed*neverchanging;
		float L_0 = ___desiredSpeed0;
		float L_1 = __this->get_neverchanging_6();
		__this->set_baseValue_5(((float)il2cpp_codegen_multiply((float)L_0, (float)L_1)));
		// }
		return;
	}
}
// System.Void GirarRueda::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GirarRueda_Update_m9CCCEA05CCC7EE5B8292BEC8AFF2181AA55CBD1D (GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * __this, const RuntimeMethod* method)
{
	{
		// transform.Rotate(speed * Time.deltaTime, 0, 0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_speed_4();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E(L_0, ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), (0.0f), (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GirarRueda::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GirarRueda__ctor_m70C1661D53191D8DD0A03A08A79249E7546F96C3 (GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private float baseValue = 540;
		__this->set_baseValue_5((540.0f));
		// private float neverchanging = 540;
		__this->set_neverchanging_6((540.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuAppearScript::MenuAppear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuAppearScript_MenuAppear_mA84A1D2B30815FF859DBC6B72EE3895BCCF2C7E2 (MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8 * __this, const RuntimeMethod* method)
{
	{
		// isShowing = !isShowing;
		bool L_0 = __this->get_isShowing_7();
		__this->set_isShowing_7((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// SetImage(isShowing);
		bool L_1 = __this->get_isShowing_7();
		MenuAppearScript_SetImage_m3D6B513083873E48304D0CBB9CE2B5ADCB40F0E1(__this, L_1, /*hidden argument*/NULL);
		// menu.SetActive(isShowing);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_menu_6();
		bool L_3 = __this->get_isShowing_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuAppearScript::SetImage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuAppearScript_SetImage_m3D6B513083873E48304D0CBB9CE2B5ADCB40F0E1 (MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8 * __this, bool ___state0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(state)
		bool L_0 = ___state0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// GetComponent<Image>().sprite = first;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1;
		L_1 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(__this, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_2 = __this->get_first_4();
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0015:
	{
		// GetComponent<Image>().sprite = second;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_3;
		L_3 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(__this, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_4 = __this->get_second_5();
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuAppearScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuAppearScript_Update_mAE56EC50D83B4D609C8025EB1B8B3F80FA7AF497 (MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8 * __this, const RuntimeMethod* method)
{
	{
		// if (Input.GetKeyDown(KeyCode.Space))
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		// isShowing = !isShowing;
		bool L_1 = __this->get_isShowing_7();
		__this->set_isShowing_7((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		// menu.SetActive(isShowing);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_menu_6();
		bool L_3 = __this->get_isShowing_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void MenuAppearScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuAppearScript__ctor_m79276CBDFE0545CAC2ECB7CCEF1C3B6BF14AE5BF (MenuAppearScript_tE5FC743D82D293248FD312BFF5C14409852587B8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Movement::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1 (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Movement::StartMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement_StartMove_mBEB1DDB430F9908AE15CE198A6BC28C10A7147DC (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, const RuntimeMethod* method)
{
	{
		// if (startMove == false)
		bool L_0 = __this->get_startMove_10();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		// startMove = true;
		__this->set_startMove_10((bool)1);
		// stopped = false;
		__this->set_stopped_11((bool)0);
		// StartCoroutine(StartCoroutine(comandos));
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_1 = __this->get_comandos_7();
		RuntimeObject* L_2;
		L_2 = Movement_StartCoroutine_mDEB32D8D5168F232623CE5BCB97492E6DD77EDC4(__this, L_1, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void Movement::RellenarComando(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement_RellenarComando_m91988887F5B24BF61D2940F38220E42CD5596ACD (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, String_t* ___comando0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// comandos.Add(comando);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_comandos_7();
		String_t* L_1 = ___comando0;
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_1, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Movement::RellenarComando()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement_RellenarComando_mF515958A2160CF4D95865F31039CD11CD272CC99 (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// comandos.Add(commandText.text);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_comandos_7();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1 = __this->get_commandText_9();
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_2, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Movement::ResetPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement_ResetPlayer_m341D4D356FF0994B3E74FD27BF99AE1146531A6B (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// comandos = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_0, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_comandos_7(L_0);
		// rb.MovePosition(startPos.position);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_1 = __this->get_rb_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_startPos_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7(L_1, L_3, /*hidden argument*/NULL);
		// rb.velocity = new Vector3(0, 0, 0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = __this->get_rb_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_5), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_4, L_5, /*hidden argument*/NULL);
		// stopped = true;
		__this->set_stopped_11((bool)1);
		// startMove = false;
		__this->set_startMove_10((bool)0);
		// StopAllCoroutines();
		MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator Movement::Wait(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Movement_Wait_m7E219A93C438A3519BB4F2943AD942FB8BD051CB (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, int32_t ___seconds0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * L_0 = (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 *)il2cpp_codegen_object_new(U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01_il2cpp_TypeInfo_var);
		U3CWaitU3Ed__18__ctor_mBFF89EB8921F8EB82D33BA0EF8228082DA395F2F(L_0, 0, /*hidden argument*/NULL);
		U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * L_1 = L_0;
		int32_t L_2 = ___seconds0;
		L_1->set_seconds_2(L_2);
		return L_1;
	}
}
// System.Collections.IEnumerator Movement::StartCoroutine(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Movement_StartCoroutine_mDEB32D8D5168F232623CE5BCB97492E6DD77EDC4 (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * L_0 = (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 *)il2cpp_codegen_object_new(U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653_il2cpp_TypeInfo_var);
		U3CStartCoroutineU3Ed__19__ctor_m9BCECFBF92C11A847A748005D398FAA7B8922A8F(L_0, 0, /*hidden argument*/NULL);
		U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * L_2 = L_1;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3 = ___comandos0;
		L_2->set_comandos_3(L_3);
		return L_2;
	}
}
// System.Void Movement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float turnSpeed = 180f;
		__this->set_turnSpeed_5((180.0f));
		// public float moveSpeed = 6f;
		__this->set_moveSpeed_6((6.0f));
		// public List<string> comandos = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_0, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_comandos_7(L_0);
		// public char separation = ' ';
		__this->set_separation_12(((int32_t)32));
		// public float distance = 10f;
		__this->set_distance_13((10.0f));
		// public float waitSeconds = 5f;
		__this->set_waitSeconds_14((5.0f));
		// public Vector3 targetOffset = Vector3.forward * 10f;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_1, (10.0f), /*hidden argument*/NULL);
		__this->set_targetOffset_15(L_2);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Movement2::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_Start_m12F783FA1DF8188103E14AAEC7F1873C81F2DC1A (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Movement2::CheckMovementBools()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_CheckMovementBools_m4A5E33DE213E35A668DF4F1E7BFCDC1D161966DC (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	{
		// if(advance)
		bool L_0 = __this->get_advance_9();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// RuedaDerecha.ChangeSpeed(1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_1 = __this->get_RuedaDerecha_4();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_1, 1, /*hidden argument*/NULL);
		// RuedaIzquierda.ChangeSpeed(1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_2 = __this->get_RuedaIzquierda_5();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_2, 1, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// if(back)
		bool L_3 = __this->get_back_10();
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		// RuedaDerecha.ChangeSpeed(-1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_4 = __this->get_RuedaDerecha_4();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_4, (-1), /*hidden argument*/NULL);
		// RuedaIzquierda.ChangeSpeed(-1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_5 = __this->get_RuedaIzquierda_5();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_5, (-1), /*hidden argument*/NULL);
	}

IL_0040:
	{
		// if(turnLeft)
		bool L_6 = __this->get_turnLeft_11();
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		// RuedaDerecha.ChangeSpeed(1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_7 = __this->get_RuedaDerecha_4();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_7, 1, /*hidden argument*/NULL);
		// RuedaIzquierda.ChangeSpeed(-1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_8 = __this->get_RuedaIzquierda_5();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_8, (-1), /*hidden argument*/NULL);
	}

IL_0060:
	{
		// if(turnRight)
		bool L_9 = __this->get_turnRight_12();
		if (!L_9)
		{
			goto IL_0080;
		}
	}
	{
		// RuedaDerecha.ChangeSpeed(1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_10 = __this->get_RuedaDerecha_4();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_10, 1, /*hidden argument*/NULL);
		// RuedaIzquierda.ChangeSpeed(-1);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_11 = __this->get_RuedaIzquierda_5();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_11, (-1), /*hidden argument*/NULL);
	}

IL_0080:
	{
		// if(stop)
		bool L_12 = __this->get_stop_13();
		if (!L_12)
		{
			goto IL_00a0;
		}
	}
	{
		// RuedaDerecha.ChangeSpeed(0);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_13 = __this->get_RuedaDerecha_4();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_13, 0, /*hidden argument*/NULL);
		// RuedaIzquierda.ChangeSpeed(0);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_14 = __this->get_RuedaIzquierda_5();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_14, 0, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		// }
		return;
	}
}
// System.Void Movement2::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_Update_m0E01C423FC5685C606D1F8FB29209F8CF72DFAD7 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log(moveSpeed);
		float L_0 = __this->get_moveSpeed_18();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		// CheckMovementBools();
		Movement2_CheckMovementBools_m4A5E33DE213E35A668DF4F1E7BFCDC1D161966DC(__this, /*hidden argument*/NULL);
		// if(advance)
		bool L_3 = __this->get_advance_9();
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		// rb.velocity = transform.forward * moveSpeed;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = __this->get_rb_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_moveSpeed_18();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_6, L_7, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_4, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// if(back)
		bool L_9 = __this->get_back_10();
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		// rb.velocity = transform.forward * -moveSpeed;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_10 = __this->get_rb_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_11, /*hidden argument*/NULL);
		float L_13 = __this->get_moveSpeed_18();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_12, ((-L_13)), /*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_10, L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		// if(turnLeft)
		bool L_15 = __this->get_turnLeft_11();
		if (!L_15)
		{
			goto IL_00a3;
		}
	}
	{
		// rb.angularVelocity = new Vector3(0, (turnSpeed * Mathf.PI *-1)/ 180, 0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_16 = __this->get_rb_6();
		float L_17 = __this->get_turnSpeed_16();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_18), (0.0f), ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_17, (float)(3.14159274f))), (float)(-1.0f)))/(float)(180.0f))), (0.0f), /*hidden argument*/NULL);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_16, L_18, /*hidden argument*/NULL);
	}

IL_00a3:
	{
		// if(turnRight)
		bool L_19 = __this->get_turnRight_12();
		if (!L_19)
		{
			goto IL_00d7;
		}
	}
	{
		// rb.angularVelocity = new Vector3(0, (turnSpeed * Mathf.PI) / 180, 0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_20 = __this->get_rb_6();
		float L_21 = __this->get_turnSpeed_16();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_22), (0.0f), ((float)((float)((float)il2cpp_codegen_multiply((float)L_21, (float)(3.14159274f)))/(float)(180.0f))), (0.0f), /*hidden argument*/NULL);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_20, L_22, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator Movement2::StartCoroutine(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Movement2_StartCoroutine_m833568432212E05FD60EB6B743476EB3450A3CA3 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___comandos0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * L_0 = (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 *)il2cpp_codegen_object_new(U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360_il2cpp_TypeInfo_var);
		U3CStartCoroutineU3Ed__21__ctor_m81430BEC68689F4A9093E91D3E7685D5832AF243(L_0, 0, /*hidden argument*/NULL);
		U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * L_1 = L_0;
		L_1->set_U3CU3E4__this_3(__this);
		U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * L_2 = L_1;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3 = ___comandos0;
		L_2->set_comandos_2(L_3);
		return L_2;
	}
}
// System.Void Movement2::StartMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_StartMove_m289D60BE07D916B24EA3E20488556F4DE1A1E9B1 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	{
		// if (startMove == false)
		bool L_0 = __this->get_startMove_14();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		// startMove = true;
		__this->set_startMove_14((bool)1);
		// StartCoroutine(StartCoroutine(comandos));
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_1 = __this->get_comandos_15();
		RuntimeObject* L_2;
		L_2 = Movement2_StartCoroutine_m833568432212E05FD60EB6B743476EB3450A3CA3(__this, L_1, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// }
		return;
	}
}
// System.Void Movement2::RellenarComando(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, String_t* ___comando0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// comandos.Add(comando);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_comandos_15();
		String_t* L_1 = ___comando0;
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_1, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Movement2::RellenarComando()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_RellenarComando_m2185CD4FFDE06C5B691638A100B5A39F21D34143 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// comandos.Add(commandText.text);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_comandos_15();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1 = __this->get_commandText_8();
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_0, L_2, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Movement2::VaciarLista()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_VaciarLista_m257E3481A363F42DBC27D7614E376D4DA5D8BD2D (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// comandos = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_0, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_comandos_15(L_0);
		// }
		return;
	}
}
// System.Void Movement2::ResetPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2_ResetPlayer_mC5AA3620F9717631A06588A233B369D3E54B0F1F (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	{
		// advance = false;
		__this->set_advance_9((bool)0);
		// back = false;
		__this->set_back_10((bool)0);
		// turnLeft = false;
		__this->set_turnLeft_11((bool)0);
		// turnRight = false;
		__this->set_turnRight_12((bool)0);
		// rb.velocity = new Vector3(0, 0, 0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0 = __this->get_rb_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_0, L_1, /*hidden argument*/NULL);
		// rb.angularVelocity = new Vector3(0, 0, 0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_2 = __this->get_rb_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_2, L_3, /*hidden argument*/NULL);
		// rb.MovePosition(startPos.position);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = __this->get_rb_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_startPos_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7(L_4, L_6, /*hidden argument*/NULL);
		// transform.rotation = startPos.transform.rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8 = __this->get_startPos_7();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_8, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_10;
		L_10 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_9, /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_7, L_10, /*hidden argument*/NULL);
		// moveSpeed = baseMoveSpeed;
		float L_11 = __this->get_baseMoveSpeed_19();
		__this->set_moveSpeed_18(L_11);
		// RuedaDerecha.ChangeSpeed(0);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_12 = __this->get_RuedaDerecha_4();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_12, 0, /*hidden argument*/NULL);
		// RuedaIzquierda.ChangeSpeed(0);
		GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_13 = __this->get_RuedaIzquierda_5();
		GirarRueda_ChangeSpeed_m388D4E7FE83AEAE2182A550FE5AD5E38DE96F933(L_13, 0, /*hidden argument*/NULL);
		// startMove = false;
		__this->set_startMove_14((bool)0);
		// StopAllCoroutines();
		MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Movement2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Movement2__ctor_mC9B4E3CAFD71A1BC4EF6CAD384948C5E8C1B1D43 (Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<string> comandos = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_0, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_comandos_15(L_0);
		// public float turnSpeed = 160f;
		__this->set_turnSpeed_16((160.0f));
		// private float baseTurnSpeed = 160f;
		__this->set_baseTurnSpeed_17((160.0f));
		// public float moveSpeed = 6f;
		__this->set_moveSpeed_18((6.0f));
		// private float baseMoveSpeed = 6f;
		__this->set_baseMoveSpeed_19((6.0f));
		// public float waitTime = 0.5f;
		__this->set_waitTime_20((0.5f));
		// public char separation = ' ';
		__this->set_separation_21(((int32_t)32));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayButton::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayButton_Awake_mF5410C4C0FA5B54741C27CA899A337D14E54550A (PlayButton_tDDD3318D639982CFBD35C9DBF9E564031E331E74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player = FindObjectOfType<Movement2>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0;
		L_0 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_4(L_0);
		// }
		return;
	}
}
// System.Void PlayButton::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayButton_Play_mB2459054EC2AD057C53B3C3435A557F65EB9B4C2 (PlayButton_tDDD3318D639982CFBD35C9DBF9E564031E331E74 * __this, const RuntimeMethod* method)
{
	{
		// player.StartMove();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_4();
		Movement2_StartMove_m289D60BE07D916B24EA3E20488556F4DE1A1E9B1(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayButton__ctor_m8DD493422840621101705045AA587CF823674391 (PlayButton_tDDD3318D639982CFBD35C9DBF9E564031E331E74 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ResetButton::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetButton_Awake_m2D334496A60A83C42F635D33DB44D6379FF58446 (ResetButton_tB0E49990488D04A82400DBE855AE500A97FEF862 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player = FindObjectOfType<Movement2>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0;
		L_0 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_4(L_0);
		// }
		return;
	}
}
// System.Void ResetButton::ResetFunc()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetButton_ResetFunc_mA401C3C6F815BDFB34D1FBD182AD19AF6ABFF8F6 (ResetButton_tB0E49990488D04A82400DBE855AE500A97FEF862 * __this, const RuntimeMethod* method)
{
	{
		// player.ResetPlayer();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_4();
		Movement2_ResetPlayer_mC5AA3620F9717631A06588A233B369D3E54B0F1F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ResetButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetButton__ctor_m5FD6F16682DCC2AC71EC79637BB2C5CB4DF06162 (ResetButton_tB0E49990488D04A82400DBE855AE500A97FEF862 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RobotBlock::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RobotBlock_Awake_mD121CA1BFF00249D75AA1D6D9B6F50E156E9DA38 (RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_6(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_9(L_1);
		// }
		return;
	}
}
// System.Void RobotBlock::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RobotBlock_Command_mA895708E4DA10F54AC44B41D71401CF2D1906824 (RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral30E3EA07EC73E95C9F85B6230DEC6F3F0373CA42);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	String_t* V_2 = NULL;
	{
		// float re = float.Parse(RightEngine.text);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = __this->get_RightEngine_7();
		String_t* L_1;
		L_1 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_0, /*hidden argument*/NULL);
		float L_2;
		L_2 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_1, /*hidden argument*/NULL);
		// float le = float.Parse(LeftEngine.text);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_3 = __this->get_LeftEngine_8();
		String_t* L_4;
		L_4 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_3, /*hidden argument*/NULL);
		float L_5;
		L_5 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// float result = (float)(((re + le) / 2.0) / 255.0);
		float L_6 = V_0;
		V_1 = ((float)((float)((double)((double)((double)((double)((double)((double)((float)il2cpp_codegen_add((float)L_2, (float)L_6))))/(double)(2.0)))/(double)(255.0)))));
		// string text = result.ToString();
		String_t* L_7;
		L_7 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		// player.RellenarComando("moveSpeed" + player.separation + text);
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_8 = __this->get_player_9();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_9 = __this->get_player_9();
		Il2CppChar* L_10 = L_9->get_address_of_separation_21();
		String_t* L_11;
		L_11 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)L_10, /*hidden argument*/NULL);
		String_t* L_12 = V_2;
		String_t* L_13;
		L_13 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral30E3EA07EC73E95C9F85B6230DEC6F3F0373CA42, L_11, L_12, /*hidden argument*/NULL);
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_8, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RobotBlock::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RobotBlock_BlockFunction_m9C0573E4F824F0B50FD31284C255B0E1B50A6847 (RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral05904614F13CDD99847CD524AF0AEEB5B54097C6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral097457485506FFE2120920CAFF6C9188A3633C28);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral119AA6FCB5FB898E8C39C3C9DCD4E9EC5617EDD0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral186DE2ABC4189D1039B1EED81CC3519A6CF44C43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A41CFC59BAB02C3D184BC2537B0806F27F3A8DA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3EF05E27751242DAFA98456D71984290F4A7D9B7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5A09B4D67FCE0A752E1575AA20BB3969E11DF7D0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral61ED9CF67D51BD4F77B0D309402DEF714A30410F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral64FB9A957A6173E2C507D6355FB5425135847B8F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71A91E1E2BA8A0E7A8B00B8A207C3A6271C0315C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7951DD056E1125FB16B9FBDD80EF24D3ACDF25DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA14B87E468A7D39A25050FEDFC56E98BCD7EC451);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA74EBB3519D8DB41082F53E2ED4D92CDB6F8E95);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB306896C52449864784FAC3C721A8B665DE5D0FC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB906955E0189BE72565D0EB9908073AD1D6FC350);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC5137AA17B77508EDDC5D36A6DC60C21D60C7249);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC56A9D740D29F4C4F6B6057136491B75A782F6B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8BCEAABDEC4BC12DC399591257F062D4C9A0971);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF9F733B0E6A31938665A7D6E7FE78179490638A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (RightEngine.text == "")
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = __this->get_RightEngine_7();
		String_t* L_1;
		L_1 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		// RightEngine.text = "0";
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_3 = __this->get_RightEngine_7();
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_3, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
	}

IL_0027:
	{
		// if (LeftEngine.text == "")
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_4 = __this->get_LeftEngine_8();
		String_t* L_5;
		L_5 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_4, /*hidden argument*/NULL);
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004e;
		}
	}
	{
		// LeftEngine.text = "0";
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_7 = __this->get_LeftEngine_8();
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_7, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// ag.AddToDefinitions("int MotorD=" + RightEngine.text);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_8 = __this->get_ag_6();
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_9 = __this->get_RightEngine_7();
		String_t* L_10;
		L_10 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_9, /*hidden argument*/NULL);
		String_t* L_11;
		L_11 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralC56A9D740D29F4C4F6B6057136491B75A782F6B6, L_10, /*hidden argument*/NULL);
		ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C(L_8, L_11, /*hidden argument*/NULL);
		// ag.AddToDefinitions("int MotorI=" + LeftEngine.text);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_12 = __this->get_ag_6();
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_13 = __this->get_LeftEngine_8();
		String_t* L_14;
		L_14 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_13, /*hidden argument*/NULL);
		String_t* L_15;
		L_15 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral097457485506FFE2120920CAFF6C9188A3633C28, L_14, /*hidden argument*/NULL);
		ArduinoGenerator_AddToDefinitions_mF9C13B749491253D938541477192E51A89C5087C(L_12, L_15, /*hidden argument*/NULL);
		// ag.AddFunctionDefinition("void", "Avanzar", "digitalWrite(4, HIGH);\ndigitalWrite(5, LOW);\ndigitalWrite(7, LOW);\ndigitalWrite(8, HIGH);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_16 = __this->get_ag_6();
		ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849(L_16, _stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9, _stringLiteral186DE2ABC4189D1039B1EED81CC3519A6CF44C43, _stringLiteralAA74EBB3519D8DB41082F53E2ED4D92CDB6F8E95, /*hidden argument*/NULL);
		// ag.AddFunctionDefinition("void", "Retroceder", "digitalWrite(4, LOW);\ndigitalWrite(5, HIGH);\ndigitalWrite(7, HIGH);\ndigitalWrite(8, LOW);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_17 = __this->get_ag_6();
		ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849(L_17, _stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9, _stringLiteralA14B87E468A7D39A25050FEDFC56E98BCD7EC451, _stringLiteral71A91E1E2BA8A0E7A8B00B8A207C3A6271C0315C, /*hidden argument*/NULL);
		// ag.AddFunctionDefinition("void", "Izquierda", "digitalWrite(4, LOW);\ndigitalWrite(5, HIGH);\ndigitalWrite(7, LOW);\ndigitalWrite(8, HIGH);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_18 = __this->get_ag_6();
		ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849(L_18, _stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9, _stringLiteral5A09B4D67FCE0A752E1575AA20BB3969E11DF7D0, _stringLiteral7951DD056E1125FB16B9FBDD80EF24D3ACDF25DC, /*hidden argument*/NULL);
		// ag.AddFunctionDefinition("void", "Derecha", "digitalWrite(4, HIGH);\ndigitalWrite(5, LOW);\ndigitalWrite(7, HIGH);\ndigitalWrite(8, LOW);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_19 = __this->get_ag_6();
		ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849(L_19, _stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9, _stringLiteral64FB9A957A6173E2C507D6355FB5425135847B8F, _stringLiteralB906955E0189BE72565D0EB9908073AD1D6FC350, /*hidden argument*/NULL);
		// ag.AddFunctionDefinition("void", "Detener", "digitalWrite(4, LOW);\ndigitalWrite(5, LOW);\ndigitalWrite(7, LOW);\ndigitalWrite(8, LOW);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_20 = __this->get_ag_6();
		ArduinoGenerator_AddFunctionDefinition_m0D90BE1E3B507FDE841D6848D622097883B3C849(L_20, _stringLiteral6ECB47BAE746457DBE91D0BA044A27960D4563D9, _stringLiteralB306896C52449864784FAC3C721A8B665DE5D0FC, _stringLiteralC8BCEAABDEC4BC12DC399591257F062D4C9A0971, /*hidden argument*/NULL);
		// ag.AddToSetup("pinMode(4, OUTPUT);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_21 = __this->get_ag_6();
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_21, _stringLiteral119AA6FCB5FB898E8C39C3C9DCD4E9EC5617EDD0, /*hidden argument*/NULL);
		// ag.AddToSetup("pinMode(5, OUTPUT);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_22 = __this->get_ag_6();
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_22, _stringLiteral1A41CFC59BAB02C3D184BC2537B0806F27F3A8DA, /*hidden argument*/NULL);
		// ag.AddToSetup("pinMode(7, OUTPUT);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_23 = __this->get_ag_6();
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_23, _stringLiteral05904614F13CDD99847CD524AF0AEEB5B54097C6, /*hidden argument*/NULL);
		// ag.AddToSetup("pinMode(8, OUTPUT);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_24 = __this->get_ag_6();
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_24, _stringLiteralC5137AA17B77508EDDC5D36A6DC60C21D60C7249, /*hidden argument*/NULL);
		// ag.AddToSetup("pinMode(3, OUTPUT);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_25 = __this->get_ag_6();
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_25, _stringLiteral3EF05E27751242DAFA98456D71984290F4A7D9B7, /*hidden argument*/NULL);
		// ag.AddToSetup("pinMode(6, OUTPUT);");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_26 = __this->get_ag_6();
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_26, _stringLiteralF9F733B0E6A31938665A7D6E7FE78179490638A9, /*hidden argument*/NULL);
		// ag.AddToSetup("analogWrite(" + 6 + "," + LeftEngine.text + ");");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_27 = __this->get_ag_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		ArrayElementTypeCheck (L_29, _stringLiteral61ED9CF67D51BD4F77B0D309402DEF714A30410F);
		(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral61ED9CF67D51BD4F77B0D309402DEF714A30410F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_30 = L_29;
		V_0 = 6;
		String_t* L_31;
		L_31 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_31);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_32 = L_30;
		ArrayElementTypeCheck (L_32, _stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB);
		(L_32)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_33 = L_32;
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_34 = __this->get_LeftEngine_8();
		String_t* L_35;
		L_35 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_34, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_35);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_36 = L_33;
		ArrayElementTypeCheck (L_36, _stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780);
		(L_36)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780);
		String_t* L_37;
		L_37 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_36, /*hidden argument*/NULL);
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_27, L_37, /*hidden argument*/NULL);
		// ag.AddToSetup("analogWrite(" + 6 + "," + RightEngine.text + ");");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_38 = __this->get_ag_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_39 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_40 = L_39;
		ArrayElementTypeCheck (L_40, _stringLiteral61ED9CF67D51BD4F77B0D309402DEF714A30410F);
		(L_40)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral61ED9CF67D51BD4F77B0D309402DEF714A30410F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_41 = L_40;
		V_0 = 6;
		String_t* L_42;
		L_42 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_41, L_42);
		(L_41)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_42);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_43 = L_41;
		ArrayElementTypeCheck (L_43, _stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB);
		(L_43)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_44 = L_43;
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_45 = __this->get_RightEngine_7();
		String_t* L_46;
		L_46 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_45, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_44, L_46);
		(L_44)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_46);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_47 = L_44;
		ArrayElementTypeCheck (L_47, _stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780);
		(L_47)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780);
		String_t* L_48;
		L_48 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_47, /*hidden argument*/NULL);
		ArduinoGenerator_AddToSetup_m8EC4BDD45F6EBF51FAE460D62F13E8F50953D2D8(L_38, L_48, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void RobotBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RobotBlock__ctor_m801C1BE4F983EF8300062906889C2CF51B81E960 (RobotBlock_tF9F750737926AFB4B43A3F21411BCBC71967FD2D * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StopBlock::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StopBlock_Awake_m2CB9222D913E35E3E3CF5F4B0F6AB42D0BBE4747 (StopBlock_t714D002381C475273632C74B513BC93BC959CE2B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_6(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_7(L_1);
		// }
		return;
	}
}
// System.Void StopBlock::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StopBlock_Command_mCA6304A721B125BDCD3D0BA6FEDD1467DB420176 (StopBlock_t714D002381C475273632C74B513BC93BC959CE2B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player.RellenarComando("stop");
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_7();
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_0, _stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void StopBlock::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StopBlock_BlockFunction_mDE69EAD6C6BC4074EAB33932FE44F6A63DA79EA1 (StopBlock_t714D002381C475273632C74B513BC93BC959CE2B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral40731BD58AF33D59E341C39E01AED51A758BB7D2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag.AddToLoop("Detener();");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_6();
		ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8(L_0, _stringLiteral40731BD58AF33D59E341C39E01AED51A758BB7D2, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void StopBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StopBlock__ctor_m5986634D6A3740FF756C0BDF404CB7B0A451D005 (StopBlock_t714D002381C475273632C74B513BC93BC959CE2B * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnLeftBlock::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnLeftBlock_Awake_mAB53CDCD1100230E369C34FCB74BF8C67905DB21 (TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_6(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_7(L_1);
		// }
		return;
	}
}
// System.Void TurnLeftBlock::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnLeftBlock_Command_mF542404F51173279C09AB5A92ADD251C50E6AEC8 (TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3C2AD8496590C39FDE4C8E6283F034228A22495);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player.RellenarComando("turnLeft");
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_7();
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_0, _stringLiteralA3C2AD8496590C39FDE4C8E6283F034228A22495, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TurnLeftBlock::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnLeftBlock_BlockFunction_m84AF055FE76C840152090E0FBEFCCE4C61A79F60 (TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral03E19BE510F62532D17030698FB484EE1A313C7B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag.AddToLoop("Izquierda();");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_6();
		ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8(L_0, _stringLiteral03E19BE510F62532D17030698FB484EE1A313C7B, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void TurnLeftBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnLeftBlock__ctor_m030BA77CFBC0237496CC4AA6C5B382592664C515 (TurnLeftBlock_t5F5105EB833DFE70F5DA74EEC902E890C3454B51 * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnRightBlock::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnRightBlock_Awake_m132FCBB93ABFF06881AC7BC3736F0F325C5C379D (TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_6(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_7(L_1);
		// }
		return;
	}
}
// System.Void TurnRightBlock::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnRightBlock_Command_mB96F557241D00E7633695E74BD4E64BD735EE5B3 (TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3E05657E3059A26857EC5E6B6D4F04751C2068C6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// player.RellenarComando("turnRight");
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_0 = __this->get_player_7();
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_0, _stringLiteral3E05657E3059A26857EC5E6B6D4F04751C2068C6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TurnRightBlock::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnRightBlock_BlockFunction_mC1EC1302DFC2E405E949BAC4F04ED2C4C725A7EE (TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral267CF425CCE42E050EFC2C100440735E81A83D3E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag.AddToLoop("Derecha();");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_6();
		ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8(L_0, _stringLiteral267CF425CCE42E050EFC2C100440735E81A83D3E, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void TurnRightBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnRightBlock__ctor_m617637B41D0C72E9B00A15E8181B404B804D3025 (TurnRightBlock_t82379F8A1B7A39A4D954A052EEECE1D1E853E089 * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WaitButton::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitButton_Awake_m9120740B9F2F92AC9C4FD2E0054D29BF92B1FE9A (WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ag = FindObjectOfType<ArduinoGenerator>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0;
		L_0 = Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016(/*hidden argument*/Object_FindObjectOfType_TisArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0_m8737F35A0E79E0F5CC4AF959A24F541982732016_RuntimeMethod_var);
		__this->set_ag_7(L_0);
		// player = FindObjectOfType<Movement2>();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1;
		L_1 = Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15(/*hidden argument*/Object_FindObjectOfType_TisMovement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103_mCE89E453C2119D956AF3E1FA1CBAF36DB8BD3B15_RuntimeMethod_var);
		__this->set_player_8(L_1);
		// }
		return;
	}
}
// System.Void WaitButton::Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitButton_Command_m02AA59C63C0E60F96D4C0B5D69B5D9D4106F095C (WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (input.text == "")
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = __this->get_input_6();
		String_t* L_1;
		L_1 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0042;
		}
	}
	{
		// player.RellenarComando("wait" + player.separation + "0");
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_3 = __this->get_player_8();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_4 = __this->get_player_8();
		Il2CppChar* L_5 = L_4->get_address_of_separation_21();
		String_t* L_6;
		L_6 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)L_5, /*hidden argument*/NULL);
		String_t* L_7;
		L_7 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3, L_6, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_3, L_7, /*hidden argument*/NULL);
		return;
	}

IL_0042:
	{
		// player.RellenarComando("wait" + player.separation + input.text);
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_8 = __this->get_player_8();
		Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_9 = __this->get_player_8();
		Il2CppChar* L_10 = L_9->get_address_of_separation_21();
		String_t* L_11;
		L_11 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)L_10, /*hidden argument*/NULL);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_12 = __this->get_input_6();
		String_t* L_13;
		L_13 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_12, /*hidden argument*/NULL);
		String_t* L_14;
		L_14 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3, L_11, L_13, /*hidden argument*/NULL);
		Movement2_RellenarComando_m59D0637002C4496580F52D1FE88DB4ACFB4FE0FD(L_8, L_14, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Int32 WaitButton::CheckValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WaitButton_CheckValue_m7CCA307D27B82090E14654D4564924126C2EEC3A (WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (input.text == "")
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_0 = __this->get_input_6();
		String_t* L_1;
		L_1 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// return 0;
		return 0;
	}

IL_0019:
	{
		// return (int)(float.Parse(text)*1000f);
		String_t* L_3 = ___text0;
		float L_4;
		L_4 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_3, /*hidden argument*/NULL);
		return il2cpp_codegen_cast_double_to_int<int32_t>(((float)il2cpp_codegen_multiply((float)L_4, (float)(1000.0f))));
	}
}
// System.Void WaitButton::BlockFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitButton_BlockFunction_m8BCCB86E5A8F1A6DC92D3632EF3631A4415F37BE (WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC68705613E7D13EDE578EAD6DC3A12471F48A831);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// ag.AddToLoop("delay("+CheckValue(input.text)+");");
		ArduinoGenerator_tD92F0483DFC84B0317AD4DA3163C6F180D428BE0 * L_0 = __this->get_ag_7();
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_1 = __this->get_input_6();
		String_t* L_2;
		L_2 = InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline(L_1, /*hidden argument*/NULL);
		int32_t L_3;
		L_3 = WaitButton_CheckValue_m7CCA307D27B82090E14654D4564924126C2EEC3A(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4;
		L_4 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_5;
		L_5 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteralC68705613E7D13EDE578EAD6DC3A12471F48A831, L_4, _stringLiteral4C5B8ACB8190E84E27D67B689F7A739E72872780, /*hidden argument*/NULL);
		ArduinoGenerator_AddToLoop_m66B36475F3437766ADEEE412FB4ECAA37D42F3D8(L_0, L_5, /*hidden argument*/NULL);
		// Command();
		VirtActionInvoker0::Invoke(4 /* System.Void Block::Command() */, __this);
		// }
		return;
	}
}
// System.Void WaitButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitButton__ctor_m10CE988D4A5ED4BFDBA9250F7A4788BD96A0E437 (WaitButton_t33A15E53A06A6BE1522373283C9B7BA98A7333A3 * __this, const RuntimeMethod* method)
{
	{
		Block__ctor_m2AD58504FC354294167EDD7C1B386C108FF1BCAE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Movement/<StartCoroutine>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19__ctor_m9BCECFBF92C11A847A748005D398FAA7B8922A8F (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Movement/<StartCoroutine>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19_System_IDisposable_Dispose_mB37ED72A8D68B4562B4D700B39D943ADDD7F9249 (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1))) <= ((uint32_t)3))))
		{
			goto IL_001c;
		}
	}

IL_0012:
	{
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1C, FINALLY_0015);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0015;
	}

FINALLY_0015:
	{ // begin finally (depth: 1)
		U3CStartCoroutineU3Ed__19_U3CU3Em__Finally1_mB92A9D87A4F1B0276B8064AEB27FA56377C7DD20(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(21)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(21)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1C, IL_001c)
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean Movement/<StartCoroutine>d__19::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartCoroutineU3Ed__19_MoveNext_m15ABADF3AF20EAC7A466EDFDFBACEF9AF4786572 (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3A3C5272E76787A96B4445991AF0A820F0D2D4EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEECEAAC359EDB2E91D0F46F3CA9A65F47527BC5C);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * V_2 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_3 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_4 = NULL;
	float V_5 = 0.0f;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_6;
	memset((&V_6), 0, sizeof(V_6));
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 6> __leave_targets;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_1 = __this->get_U3CU3E4__this_2();
			V_2 = L_1;
			int32_t L_2 = V_1;
			switch (L_2)
			{
				case 0:
				{
					goto IL_002f;
				}
				case 1:
				{
					goto IL_012e;
				}
				case 2:
				{
					goto IL_02a4;
				}
				case 3:
				{
					goto IL_037d;
				}
				case 4:
				{
					goto IL_03d7;
				}
			}
		}

IL_0028:
		{
			V_0 = (bool)0;
			goto IL_0413;
		}

IL_002f:
		{
			__this->set_U3CU3E1__state_0((-1));
			// stopped = false;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_3 = V_2;
			L_3->set_stopped_11((bool)0);
			// foreach(var item in comandos)
			List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_4 = __this->get_comandos_3();
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_5;
			L_5 = List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF(L_4, /*hidden argument*/List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
			__this->set_U3CU3E7__wrap1_4(L_5);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_03e6;
		}

IL_005b:
		{
			// foreach(var item in comandos)
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_6 = __this->get_address_of_U3CU3E7__wrap1_4();
			String_t* L_7;
			L_7 = Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)L_6, /*hidden argument*/Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
			__this->set_U3CitemU3E5__3_5(L_7);
			// if(item.StartsWith("move"))
			String_t* L_8 = __this->get_U3CitemU3E5__3_5();
			bool L_9;
			L_9 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_8, _stringLiteralEECEAAC359EDB2E91D0F46F3CA9A65F47527BC5C, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0186;
			}
		}

IL_0081:
		{
			// string[] values = item.Split(separation);
			String_t* L_10 = __this->get_U3CitemU3E5__3_5();
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_11 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_12 = L_11;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_13 = V_2;
			Il2CppChar L_14 = L_13->get_separation_12();
			(L_12)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_14);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15;
			L_15 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_10, L_12, /*hidden argument*/NULL);
			V_3 = L_15;
			// distance = (float)int.Parse(values[1]);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_16 = V_2;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = V_3;
			int32_t L_18 = 1;
			String_t* L_19 = (L_17)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_18));
			int32_t L_20;
			L_20 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_19, /*hidden argument*/NULL);
			L_16->set_distance_13(((float)((float)L_20)));
			// targetOffset = transform.forward * distance;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_21 = V_2;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_22 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
			L_23 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_22, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
			L_24 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_23, /*hidden argument*/NULL);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_25 = V_2;
			float L_26 = L_25->get_distance_13();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
			L_27 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_24, L_26, /*hidden argument*/NULL);
			L_21->set_targetOffset_15(L_27);
			// waitSeconds = distance / moveSpeed;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_28 = V_2;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_29 = V_2;
			float L_30 = L_29->get_distance_13();
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_31 = V_2;
			float L_32 = L_31->get_moveSpeed_6();
			L_28->set_waitSeconds_14(((float)((float)L_30/(float)L_32)));
			// Vector3 targetPosition = transform.position + targetOffset;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_33 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34;
			L_34 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_33, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
			L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_36 = V_2;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = L_36->get_targetOffset_15();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
			L_38 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_35, L_37, /*hidden argument*/NULL);
			__this->set_U3CtargetPositionU3E5__4_6(L_38);
			goto IL_0136;
		}

IL_00f8:
		{
			// rb.velocity = transform.forward*moveSpeed;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_39 = V_2;
			Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_40 = L_39->get_rb_4();
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_41 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_42;
			L_42 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_41, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
			L_43 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_42, /*hidden argument*/NULL);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_44 = V_2;
			float L_45 = L_44->get_moveSpeed_6();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46;
			L_46 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_43, L_45, /*hidden argument*/NULL);
			Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_40, L_46, /*hidden argument*/NULL);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0413;
		}

IL_012e:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0136:
		{
			// while (!stopped&&Vector3.Distance(transform.position,targetPosition)>0.2f)
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_47 = V_2;
			bool L_48 = L_47->get_stopped_11();
			if (L_48)
			{
				goto IL_015b;
			}
		}

IL_013e:
		{
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_49 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_50;
			L_50 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_49, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
			L_51 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_50, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = __this->get_U3CtargetPositionU3E5__4_6();
			float L_53;
			L_53 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_51, L_52, /*hidden argument*/NULL);
			if ((((float)L_53) > ((float)(0.200000003f))))
			{
				goto IL_00f8;
			}
		}

IL_015b:
		{
			// rb.velocity = new Vector3(0,0,0);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_54 = V_2;
			Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_55 = L_54->get_rb_4();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_56;
			memset((&L_56), 0, sizeof(L_56));
			Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_56), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
			Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_55, L_56, /*hidden argument*/NULL);
			// }
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_57 = __this->get_address_of_U3CtargetPositionU3E5__4_6();
			il2cpp_codegen_initobj(L_57, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		}

IL_0186:
		{
			// if(item.StartsWith("rotate"))
			String_t* L_58 = __this->get_U3CitemU3E5__3_5();
			bool L_59;
			L_59 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_58, _stringLiteral3A3C5272E76787A96B4445991AF0A820F0D2D4EB, /*hidden argument*/NULL);
			if (!L_59)
			{
				goto IL_0306;
			}
		}

IL_019b:
		{
			// string[] values = item.Split(separation);
			String_t* L_60 = __this->get_U3CitemU3E5__3_5();
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_61 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_62 = L_61;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_63 = V_2;
			Il2CppChar L_64 = L_63->get_separation_12();
			(L_62)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_64);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_65;
			L_65 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_60, L_62, /*hidden argument*/NULL);
			V_4 = L_65;
			// int direction = int.Parse(values[1]);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_66 = V_4;
			int32_t L_67 = 1;
			String_t* L_68 = (L_66)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_67));
			int32_t L_69;
			L_69 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_68, /*hidden argument*/NULL);
			__this->set_U3CdirectionU3E5__5_7(L_69);
			// float angles = (float)int.Parse(values[2]);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_70 = V_4;
			int32_t L_71 = 2;
			String_t* L_72 = (L_70)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_71));
			int32_t L_73;
			L_73 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_72, /*hidden argument*/NULL);
			V_5 = ((float)((float)L_73));
			// Vector3 targetRotationVec = transform.rotation.eulerAngles+new Vector3(0,angles*direction,0);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_74 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_75;
			L_75 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_74, /*hidden argument*/NULL);
			Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_76;
			L_76 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_75, /*hidden argument*/NULL);
			V_6 = L_76;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_77;
			L_77 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_6), /*hidden argument*/NULL);
			float L_78 = V_5;
			int32_t L_79 = __this->get_U3CdirectionU3E5__5_7();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_80;
			memset((&L_80), 0, sizeof(L_80));
			Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_80), (0.0f), ((float)il2cpp_codegen_multiply((float)L_78, (float)((float)((float)L_79)))), (0.0f), /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_81;
			L_81 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_77, L_80, /*hidden argument*/NULL);
			__this->set_U3CtargetPositionU3E5__4_6(L_81);
			// Vector3 transformRotationVec = transform.rotation.eulerAngles;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_82 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_83;
			L_83 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_82, /*hidden argument*/NULL);
			Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_84;
			L_84 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_83, /*hidden argument*/NULL);
			V_6 = L_84;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_85;
			L_85 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_6), /*hidden argument*/NULL);
			__this->set_U3CtransformRotationVecU3E5__6_8(L_85);
			// Debug.Log(Vector3.Distance(targetRotationVec, transformRotationVec));
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_86 = __this->get_U3CtargetPositionU3E5__4_6();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_87 = __this->get_U3CtransformRotationVecU3E5__6_8();
			float L_88;
			L_88 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_86, L_87, /*hidden argument*/NULL);
			float L_89 = L_88;
			RuntimeObject * L_90 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_89);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
			Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_90, /*hidden argument*/NULL);
			goto IL_02ac;
		}

IL_0241:
		{
			// rb.angularVelocity = new Vector3(0, turnSpeed*Mathf.PI/180*direction, 0);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_91 = V_2;
			Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_92 = L_91->get_rb_4();
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_93 = V_2;
			float L_94 = L_93->get_turnSpeed_5();
			int32_t L_95 = __this->get_U3CdirectionU3E5__5_7();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96;
			memset((&L_96), 0, sizeof(L_96));
			Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_96), (0.0f), ((float)il2cpp_codegen_multiply((float)((float)((float)((float)il2cpp_codegen_multiply((float)L_94, (float)(3.14159274f)))/(float)(180.0f))), (float)((float)((float)L_95)))), (0.0f), /*hidden argument*/NULL);
			Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_92, L_96, /*hidden argument*/NULL);
			// transformRotationVec = transform.rotation.eulerAngles;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_97 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_98;
			L_98 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_97, /*hidden argument*/NULL);
			Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_99;
			L_99 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_98, /*hidden argument*/NULL);
			V_6 = L_99;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_100;
			L_100 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_6), /*hidden argument*/NULL);
			__this->set_U3CtransformRotationVecU3E5__6_8(L_100);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_0413;
		}

IL_02a4:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_02ac:
		{
			// while (!stopped&&Vector3.Distance(targetRotationVec,transformRotationVec)>2f)
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_101 = V_2;
			bool L_102 = L_101->get_stopped_11();
			if (L_102)
			{
				goto IL_02cf;
			}
		}

IL_02b4:
		{
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_103 = __this->get_U3CtargetPositionU3E5__4_6();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_104 = __this->get_U3CtransformRotationVecU3E5__6_8();
			float L_105;
			L_105 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_103, L_104, /*hidden argument*/NULL);
			if ((((float)L_105) > ((float)(2.0f))))
			{
				goto IL_0241;
			}
		}

IL_02cf:
		{
			// rb.angularVelocity = new Vector3(0, 0, 0);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_106 = V_2;
			Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_107 = L_106->get_rb_4();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_108;
			memset((&L_108), 0, sizeof(L_108));
			Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_108), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
			Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_107, L_108, /*hidden argument*/NULL);
			// }
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_109 = __this->get_address_of_U3CtargetPositionU3E5__4_6();
			il2cpp_codegen_initobj(L_109, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_110 = __this->get_address_of_U3CtransformRotationVecU3E5__6_8();
			il2cpp_codegen_initobj(L_110, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		}

IL_0306:
		{
			// if(item.StartsWith("wait"))
			String_t* L_111 = __this->get_U3CitemU3E5__3_5();
			bool L_112;
			L_112 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_111, _stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3, /*hidden argument*/NULL);
			if (!L_112)
			{
				goto IL_0392;
			}
		}

IL_0318:
		{
			// string[] values = item.Split(separation);
			String_t* L_113 = __this->get_U3CitemU3E5__3_5();
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_114 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_115 = L_114;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_116 = V_2;
			Il2CppChar L_117 = L_116->get_separation_12();
			(L_115)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_117);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_118;
			L_118 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_113, L_115, /*hidden argument*/NULL);
			V_7 = L_118;
			// float duration = (float)int.Parse(values[1]);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_119 = V_7;
			int32_t L_120 = 1;
			String_t* L_121 = (L_119)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_120));
			int32_t L_122;
			L_122 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_121, /*hidden argument*/NULL);
			__this->set_U3CdurationU3E5__7_9(((float)((float)L_122)));
			goto IL_0385;
		}

IL_0346:
		{
			// duration -= Time.deltaTime;
			float L_123 = __this->get_U3CdurationU3E5__7_9();
			float L_124;
			L_124 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
			__this->set_U3CdurationU3E5__7_9(((float)il2cpp_codegen_subtract((float)L_123, (float)L_124)));
			// Debug.Log(duration);
			float L_125 = __this->get_U3CdurationU3E5__7_9();
			float L_126 = L_125;
			RuntimeObject * L_127 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_126);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
			Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_127, /*hidden argument*/NULL);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(3);
			V_0 = (bool)1;
			goto IL_0413;
		}

IL_037d:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0385:
		{
			// while(duration>=0.0f)
			float L_128 = __this->get_U3CdurationU3E5__7_9();
			if ((((float)L_128) >= ((float)(0.0f))))
			{
				goto IL_0346;
			}
		}

IL_0392:
		{
			// if(item.StartsWith("forward"))
			String_t* L_129 = __this->get_U3CitemU3E5__3_5();
			bool L_130;
			L_130 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_129, _stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65, /*hidden argument*/NULL);
			if (!L_130)
			{
				goto IL_03df;
			}
		}

IL_03a4:
		{
			// rb.velocity = transform.forward * moveSpeed;
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_131 = V_2;
			Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_132 = L_131->get_rb_4();
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_133 = V_2;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_134;
			L_134 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_133, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_135;
			L_135 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_134, /*hidden argument*/NULL);
			Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C * L_136 = V_2;
			float L_137 = L_136->get_moveSpeed_6();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_138;
			L_138 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_135, L_137, /*hidden argument*/NULL);
			Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_132, L_138, /*hidden argument*/NULL);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(4);
			V_0 = (bool)1;
			goto IL_0413;
		}

IL_03d7:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_03df:
		{
			// }
			__this->set_U3CitemU3E5__3_5((String_t*)NULL);
		}

IL_03e6:
		{
			// foreach(var item in comandos)
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_139 = __this->get_address_of_U3CU3E7__wrap1_4();
			bool L_140;
			L_140 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)L_139, /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_140)
			{
				goto IL_005b;
			}
		}

IL_03f6:
		{
			U3CStartCoroutineU3Ed__19_U3CU3Em__Finally1_mB92A9D87A4F1B0276B8064AEB27FA56377C7DD20(__this, /*hidden argument*/NULL);
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_141 = __this->get_address_of_U3CU3E7__wrap1_4();
			il2cpp_codegen_initobj(L_141, sizeof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B ));
			// }
			V_0 = (bool)0;
			goto IL_0413;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_040c;
	}

FAULT_040c:
	{ // begin fault (depth: 1)
		U3CStartCoroutineU3Ed__19_System_IDisposable_Dispose_mB37ED72A8D68B4562B4D700B39D943ADDD7F9249(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(1036)
	} // end fault
	IL2CPP_CLEANUP(1036)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0413:
	{
		bool L_142 = V_0;
		return L_142;
	}
}
// System.Void Movement/<StartCoroutine>d__19::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19_U3CU3Em__Finally1_mB92A9D87A4F1B0276B8064AEB27FA56377C7DD20 (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_0 = __this->get_address_of_U3CU3E7__wrap1_4();
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)L_0, /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		return;
	}
}
// System.Object Movement/<StartCoroutine>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartCoroutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07691173CCEF5431341BBCF26C89A5CB841EE9EE (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Movement/<StartCoroutine>d__19::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_mC90489290B60555B1313D687BA31CF48C2D7626F (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_mC90489290B60555B1313D687BA31CF48C2D7626F_RuntimeMethod_var)));
	}
}
// System.Object Movement/<StartCoroutine>d__19::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartCoroutineU3Ed__19_System_Collections_IEnumerator_get_Current_m06EB29559E733DCA5830140A17F9264BC13249CB (U3CStartCoroutineU3Ed__19_t939CBA3186CFA388C2B9A8479351361761466653 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Movement/<Wait>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__18__ctor_mBFF89EB8921F8EB82D33BA0EF8228082DA395F2F (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Movement/<Wait>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__18_System_IDisposable_Dispose_m68B5C16847F6AB85196610F29CCBCE59A80F1049 (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Movement/<Wait>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CWaitU3Ed__18_MoveNext_m837F69776DDD410C89BCFF20AAF34AC93FE9896C (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0058;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float duration = (float)seconds;
		int32_t L_3 = __this->get_seconds_2();
		__this->set_U3CdurationU3E5__2_3(((float)((float)L_3)));
		goto IL_005f;
	}

IL_0026:
	{
		// duration -= Time.deltaTime;
		float L_4 = __this->get_U3CdurationU3E5__2_3();
		float L_5;
		L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CdurationU3E5__2_3(((float)il2cpp_codegen_subtract((float)L_4, (float)L_5)));
		// Debug.Log(duration);
		float L_6 = __this->get_U3CdurationU3E5__2_3();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_8, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0058:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_005f:
	{
		// while (duration >= 0.0f)
		float L_9 = __this->get_U3CdurationU3E5__2_3();
		if ((((float)L_9) >= ((float)(0.0f))))
		{
			goto IL_0026;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Movement/<Wait>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B6B679437293F259D9ED2A9D10816ADFFF268A (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Movement/<Wait>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_mFF16C512E6FD39347FFCA92676A6C55D2FE6A8B5 (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_mFF16C512E6FD39347FFCA92676A6C55D2FE6A8B5_RuntimeMethod_var)));
	}
}
// System.Object Movement/<Wait>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitU3Ed__18_System_Collections_IEnumerator_get_Current_mD244D0D827F1F8DB4334C341107854C934A3F5F4 (U3CWaitU3Ed__18_tAE28BF8F7F514FF96A5DFA2493C99C12BB027D01 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Movement2/<StartCoroutine>d__21::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21__ctor_m81430BEC68689F4A9093E91D3E7685D5832AF243 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Movement2/<StartCoroutine>d__21::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21_System_IDisposable_Dispose_mA53B56E97C0611E61A5A03CD4F29F23BC70817D6 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1))) <= ((uint32_t)5))))
		{
			goto IL_001c;
		}
	}

IL_0012:
	{
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1C, FINALLY_0015);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0015;
	}

FINALLY_0015:
	{ // begin finally (depth: 1)
		U3CStartCoroutineU3Ed__21_U3CU3Em__Finally1_m9E93B526D9B40F122720C047C1BF8980B27ECDC2(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(21)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(21)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1C, IL_001c)
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean Movement2/<StartCoroutine>d__21::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartCoroutineU3Ed__21_MoveNext_mCA96293BA9F9724AB205FEEF8FFD2FC71677752A (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral30E3EA07EC73E95C9F85B6230DEC6F3F0373CA42);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral381617D1A1C0C848CBE085A3C3BF523A03E9659F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3E05657E3059A26857EC5E6B6D4F04751C2068C6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3C2AD8496590C39FDE4C8E6283F034228A22495);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * V_2 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_3 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 8> __leave_targets;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_1 = __this->get_U3CU3E4__this_3();
			V_2 = L_1;
			int32_t L_2 = V_1;
			switch (L_2)
			{
				case 0:
				{
					goto IL_0037;
				}
				case 1:
				{
					goto IL_00b7;
				}
				case 2:
				{
					goto IL_0109;
				}
				case 3:
				{
					goto IL_015b;
				}
				case 4:
				{
					goto IL_01ad;
				}
				case 5:
				{
					goto IL_01ff;
				}
				case 6:
				{
					goto IL_026b;
				}
			}
		}

IL_0030:
		{
			V_0 = (bool)0;
			goto IL_033d;
		}

IL_0037:
		{
			__this->set_U3CU3E1__state_0((-1));
			// foreach (var item in comandos)
			List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3 = __this->get_comandos_2();
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_4;
			L_4 = List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF(L_3, /*hidden argument*/List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
			__this->set_U3CU3E7__wrap1_4(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_0309;
		}

IL_005c:
		{
			// foreach (var item in comandos)
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_5 = __this->get_address_of_U3CU3E7__wrap1_4();
			String_t* L_6;
			L_6 = Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)L_5, /*hidden argument*/Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
			__this->set_U3CitemU3E5__3_5(L_6);
			// if (item.StartsWith("forward"))
			String_t* L_7 = __this->get_U3CitemU3E5__3_5();
			bool L_8;
			L_8 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_7, _stringLiteralC523255E90EB47A1EB569A0D1D9BE0675B746B65, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_00bf;
			}
		}

IL_007f:
		{
			// back = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_9 = V_2;
			L_9->set_back_10((bool)0);
			// turnLeft = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_10 = V_2;
			L_10->set_turnLeft_11((bool)0);
			// turnRight = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_11 = V_2;
			L_11->set_turnRight_12((bool)0);
			// stop = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_12 = V_2;
			L_12->set_stop_13((bool)0);
			// advance = true;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_13 = V_2;
			L_13->set_advance_9((bool)1);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_033d;
		}

IL_00b7:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_00bf:
		{
			// if (item.StartsWith("back"))
			String_t* L_14 = __this->get_U3CitemU3E5__3_5();
			bool L_15;
			L_15 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_14, _stringLiteral381617D1A1C0C848CBE085A3C3BF523A03E9659F, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_0111;
			}
		}

IL_00d1:
		{
			// advance = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_16 = V_2;
			L_16->set_advance_9((bool)0);
			// turnLeft = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_17 = V_2;
			L_17->set_turnLeft_11((bool)0);
			// turnRight = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_18 = V_2;
			L_18->set_turnRight_12((bool)0);
			// stop = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_19 = V_2;
			L_19->set_stop_13((bool)0);
			// back = true;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_20 = V_2;
			L_20->set_back_10((bool)1);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_033d;
		}

IL_0109:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0111:
		{
			// if (item.StartsWith("turnLeft"))
			String_t* L_21 = __this->get_U3CitemU3E5__3_5();
			bool L_22;
			L_22 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_21, _stringLiteralA3C2AD8496590C39FDE4C8E6283F034228A22495, /*hidden argument*/NULL);
			if (!L_22)
			{
				goto IL_0163;
			}
		}

IL_0123:
		{
			// advance = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_23 = V_2;
			L_23->set_advance_9((bool)0);
			// back = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_24 = V_2;
			L_24->set_back_10((bool)0);
			// turnRight = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_25 = V_2;
			L_25->set_turnRight_12((bool)0);
			// stop = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_26 = V_2;
			L_26->set_stop_13((bool)0);
			// turnLeft = true;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_27 = V_2;
			L_27->set_turnLeft_11((bool)1);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(3);
			V_0 = (bool)1;
			goto IL_033d;
		}

IL_015b:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0163:
		{
			// if (item.StartsWith("turnRight"))
			String_t* L_28 = __this->get_U3CitemU3E5__3_5();
			bool L_29;
			L_29 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_28, _stringLiteral3E05657E3059A26857EC5E6B6D4F04751C2068C6, /*hidden argument*/NULL);
			if (!L_29)
			{
				goto IL_01b5;
			}
		}

IL_0175:
		{
			// advance = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_30 = V_2;
			L_30->set_advance_9((bool)0);
			// back = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_31 = V_2;
			L_31->set_back_10((bool)0);
			// turnLeft = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_32 = V_2;
			L_32->set_turnLeft_11((bool)0);
			// stop = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_33 = V_2;
			L_33->set_stop_13((bool)0);
			// turnRight = true;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_34 = V_2;
			L_34->set_turnRight_12((bool)1);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(4);
			V_0 = (bool)1;
			goto IL_033d;
		}

IL_01ad:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_01b5:
		{
			// if (item.StartsWith("stop"))
			String_t* L_35 = __this->get_U3CitemU3E5__3_5();
			bool L_36;
			L_36 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_35, _stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7, /*hidden argument*/NULL);
			if (!L_36)
			{
				goto IL_0207;
			}
		}

IL_01c7:
		{
			// advance = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_37 = V_2;
			L_37->set_advance_9((bool)0);
			// back = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_38 = V_2;
			L_38->set_back_10((bool)0);
			// turnLeft = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_39 = V_2;
			L_39->set_turnLeft_11((bool)0);
			// turnRight = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_40 = V_2;
			L_40->set_turnRight_12((bool)0);
			// stop = true;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_41 = V_2;
			L_41->set_stop_13((bool)1);
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(5);
			V_0 = (bool)1;
			goto IL_033d;
		}

IL_01ff:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0207:
		{
			// if (item.StartsWith("wait"))
			String_t* L_42 = __this->get_U3CitemU3E5__3_5();
			bool L_43;
			L_43 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_42, _stringLiteral9A4E9819A62B37CDEFDE754FE870C3E5B3F3B7C3, /*hidden argument*/NULL);
			if (!L_43)
			{
				goto IL_0280;
			}
		}

IL_0219:
		{
			// string[] values = item.Split(separation);
			String_t* L_44 = __this->get_U3CitemU3E5__3_5();
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_45 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_46 = L_45;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_47 = V_2;
			Il2CppChar L_48 = L_47->get_separation_21();
			(L_46)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_48);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_49;
			L_49 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_44, L_46, /*hidden argument*/NULL);
			V_3 = L_49;
			// float duration = float.Parse(values[1]);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_50 = V_3;
			int32_t L_51 = 1;
			String_t* L_52 = (L_50)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_51));
			float L_53;
			L_53 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_52, /*hidden argument*/NULL);
			__this->set_U3CdurationU3E5__4_6(L_53);
			goto IL_0273;
		}

IL_0244:
		{
			// duration -= Time.deltaTime;
			float L_54 = __this->get_U3CdurationU3E5__4_6();
			float L_55;
			L_55 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
			__this->set_U3CdurationU3E5__4_6(((float)il2cpp_codegen_subtract((float)L_54, (float)L_55)));
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(6);
			V_0 = (bool)1;
			goto IL_033d;
		}

IL_026b:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0273:
		{
			// while (duration >= 0.0f)
			float L_56 = __this->get_U3CdurationU3E5__4_6();
			if ((((float)L_56) >= ((float)(0.0f))))
			{
				goto IL_0244;
			}
		}

IL_0280:
		{
			// if(item.StartsWith("moveSpeed"))
			String_t* L_57 = __this->get_U3CitemU3E5__3_5();
			bool L_58;
			L_58 = String_StartsWith_mDE2FF98CAFFD13F88EDEB6C40158DDF840BFCF12(L_57, _stringLiteral30E3EA07EC73E95C9F85B6230DEC6F3F0373CA42, /*hidden argument*/NULL);
			if (!L_58)
			{
				goto IL_0302;
			}
		}

IL_0292:
		{
			// string[] values = item.Split(separation);
			String_t* L_59 = __this->get_U3CitemU3E5__3_5();
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_60 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
			CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_61 = L_60;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_62 = V_2;
			Il2CppChar L_63 = L_62->get_separation_21();
			(L_61)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_63);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_64;
			L_64 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_59, L_61, /*hidden argument*/NULL);
			V_4 = L_64;
			// moveSpeed = float.Parse(values[1])*baseMoveSpeed;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_65 = V_2;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_66 = V_4;
			int32_t L_67 = 1;
			String_t* L_68 = (L_66)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_67));
			float L_69;
			L_69 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_68, /*hidden argument*/NULL);
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_70 = V_2;
			float L_71 = L_70->get_baseMoveSpeed_19();
			L_65->set_moveSpeed_18(((float)il2cpp_codegen_multiply((float)L_69, (float)L_71)));
			// turnSpeed = float.Parse(values[1]) * baseTurnSpeed;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_72 = V_2;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_73 = V_4;
			int32_t L_74 = 1;
			String_t* L_75 = (L_73)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_74));
			float L_76;
			L_76 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_75, /*hidden argument*/NULL);
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_77 = V_2;
			float L_78 = L_77->get_baseTurnSpeed_17();
			L_72->set_turnSpeed_16(((float)il2cpp_codegen_multiply((float)L_76, (float)L_78)));
			// RuedaDerecha.ChangeBaseSpeed(float.Parse(values[1]));
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_79 = V_2;
			GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_80 = L_79->get_RuedaDerecha_4();
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_81 = V_4;
			int32_t L_82 = 1;
			String_t* L_83 = (L_81)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_82));
			float L_84;
			L_84 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_83, /*hidden argument*/NULL);
			GirarRueda_ChangeBaseSpeed_m0C947092E43F4688E114BE3F948648AB46ABCEF5(L_80, L_84, /*hidden argument*/NULL);
			// RuedaIzquierda.ChangeBaseSpeed(float.Parse(values[1]));
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_85 = V_2;
			GirarRueda_t8D683CF6543F9E5B95EFA5CB7EC4380E24E71E3B * L_86 = L_85->get_RuedaIzquierda_5();
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_87 = V_4;
			int32_t L_88 = 1;
			String_t* L_89 = (L_87)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_88));
			float L_90;
			L_90 = Single_Parse_mA1B20E6E0AAD67F60707D81E82667D2D4B274D6F(L_89, /*hidden argument*/NULL);
			GirarRueda_ChangeBaseSpeed_m0C947092E43F4688E114BE3F948648AB46ABCEF5(L_86, L_90, /*hidden argument*/NULL);
		}

IL_0302:
		{
			// }
			__this->set_U3CitemU3E5__3_5((String_t*)NULL);
		}

IL_0309:
		{
			// foreach (var item in comandos)
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_91 = __this->get_address_of_U3CU3E7__wrap1_4();
			bool L_92;
			L_92 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)L_91, /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_92)
			{
				goto IL_005c;
			}
		}

IL_0319:
		{
			U3CStartCoroutineU3Ed__21_U3CU3Em__Finally1_m9E93B526D9B40F122720C047C1BF8980B27ECDC2(__this, /*hidden argument*/NULL);
			Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_93 = __this->get_address_of_U3CU3E7__wrap1_4();
			il2cpp_codegen_initobj(L_93, sizeof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B ));
			// startMove = false;
			Movement2_t355FFF4A6E658B3D33D2AB6A1572C5B42F59E103 * L_94 = V_2;
			L_94->set_startMove_14((bool)0);
			// }
			V_0 = (bool)0;
			goto IL_033d;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_0336;
	}

FAULT_0336:
	{ // begin fault (depth: 1)
		U3CStartCoroutineU3Ed__21_System_IDisposable_Dispose_mA53B56E97C0611E61A5A03CD4F29F23BC70817D6(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(822)
	} // end fault
	IL2CPP_CLEANUP(822)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_033d:
	{
		bool L_95 = V_0;
		return L_95;
	}
}
// System.Void Movement2/<StartCoroutine>d__21::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21_U3CU3Em__Finally1_m9E93B526D9B40F122720C047C1BF8980B27ECDC2 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * L_0 = __this->get_address_of_U3CU3E7__wrap1_4();
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)L_0, /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		return;
	}
}
// System.Object Movement2/<StartCoroutine>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7332937FCE0266371F4E8FFC90A79B92C8C4B26 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Movement2/<StartCoroutine>d__21::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5F6A852BE1A4B60DD15B80646C5D9B06BFEF2607 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5F6A852BE1A4B60DD15B80646C5D9B06BFEF2607_RuntimeMethod_var)));
	}
}
// System.Object Movement2/<StartCoroutine>d__21::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA3AFAF6DEDE798A50A95DB5391C076FFA3FFA921 (U3CStartCoroutineU3Ed__21_tA1F8D71D1C750FD4F6387E26DF30046110573360 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_OnClick; }
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_0 = __this->get_m_OnClick_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InputField_get_text_m15D0C784A4A104390610325B02216FC2A6F1077C_inline (InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * __this, const RuntimeMethod* method)
{
	{
		// return m_Text;
		String_t* L_0 = __this->get_m_Text_38();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointerEventData_set_position_m65960EBCA54317C91CEFFC4893466F87FB168BBF_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 position { get; set; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___value0;
		__this->set_U3CpositionU3Ek__BackingField_13(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * RaycastResult_get_gameObject_mABA10AC828B2E6603A6C088A4CCD40932F6AF5FF_inline (RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * __this, const RuntimeMethod* method)
{
	{
		// get { return m_GameObject; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_m_GameObject_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 position { get; set; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_U3CpositionU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DragOrderContainer_set_objectBeingDragged_m24674BDE4A2C831362CB4CA9D08C006A54377C3A_inline (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___value0, const RuntimeMethod* method)
{
	{
		// public GameObject objectBeingDragged { get; set; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___value0;
		__this->set_U3CobjectBeingDraggedU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * DragOrderContainer_get_objectBeingDragged_mB7896F41BBCBCA83F394442A68BDE0B3780A915D_inline (DragOrderContainer_tCABE36E97E6D4C90807C6BFF1ABAD5C9CCEB5F3C * __this, const RuntimeMethod* method)
{
	{
		// public GameObject objectBeingDragged { get; set; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_U3CobjectBeingDraggedU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  List_1_get_Item_m8CAD57C6E350E1DA232EAAC07A84ED0C10D29CDB_gshared_inline (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D* L_2 = (RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((RaycastResultU5BU5D_t55B9DF597EFA3BE063604C0950E370D850283B9D*)L_2, (int32_t)L_3);
		return (RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m38EC27A53451661964C4F33683313E1FFF3A060D_gshared_inline (List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
